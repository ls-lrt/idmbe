<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="finished"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="/tmp/"/>
		<constant value="1"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="Process"/>
		<constant value="simplepdl"/>
		<constant value="J.allInstances():J"/>
		<constant value="2"/>
		<constant value="J.finishedtests():J"/>
		<constant value="name"/>
		<constant value="J.+(J):J"/>
		<constant value=".ltl"/>
		<constant value="J.writeTo(J):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="3:27-3:34"/>
		<constant value="4:3-4:20"/>
		<constant value="4:3-4:35"/>
		<constant value="5:21-5:25"/>
		<constant value="5:21-5:41"/>
		<constant value="5:50-5:60"/>
		<constant value="5:63-5:67"/>
		<constant value="5:63-5:72"/>
		<constant value="5:50-5:72"/>
		<constant value="5:75-5:81"/>
		<constant value="5:50-5:81"/>
		<constant value="5:21-5:82"/>
		<constant value="4:3-5:83"/>
		<constant value="3:2-5:83"/>
		<constant value="spdl"/>
		<constant value="repertoire"/>
		<constant value="self"/>
		<constant value="concatenateStrings"/>
		<constant value="J"/>
		<constant value="3"/>
		<constant value=""/>
		<constant value="4"/>
		<constant value="5"/>
		<constant value="12:36-12:38"/>
		<constant value="12:2-12:9"/>
		<constant value="12:41-12:44"/>
		<constant value="12:47-12:53"/>
		<constant value="12:41-12:53"/>
		<constant value="12:56-12:57"/>
		<constant value="12:41-12:57"/>
		<constant value="12:60-12:65"/>
		<constant value="12:41-12:65"/>
		<constant value="12:2-12:66"/>
		<constant value="s"/>
		<constant value="acc"/>
		<constant value="strings"/>
		<constant value="before"/>
		<constant value="after"/>
		<constant value="finishedtests"/>
		<constant value="Msimplepdl!Process;"/>
		<constant value="op finished = ("/>
		<constant value="0"/>
		<constant value="processElements"/>
		<constant value="J.nameElement():J"/>
		<constant value="J.concatenateStrings(JJJ):J"/>
		<constant value="0x0) \/ System_proc_finished;&#10;&#10;finished =&gt; [] finished;&#10;[] -finished;&#10;"/>
		<constant value="_finished .. 0x0"/>
		<constant value="_finished"/>
		<constant value="J.regexReplaceAll(JJ):J"/>
		<constant value="17:2-17:19"/>
		<constant value="18:34-18:38"/>
		<constant value="18:34-18:54"/>
		<constant value="18:69-18:71"/>
		<constant value="18:69-18:85"/>
		<constant value="18:34-18:86"/>
		<constant value="20:4-20:14"/>
		<constant value="20:34-20:40"/>
		<constant value="20:42-20:44"/>
		<constant value="20:46-20:48"/>
		<constant value="20:4-20:49"/>
		<constant value="18:3-20:49"/>
		<constant value="17:2-21:3"/>
		<constant value="21:5-21:82"/>
		<constant value="17:2-21:82"/>
		<constant value="22:24-22:42"/>
		<constant value="22:43-22:54"/>
		<constant value="16:32-22:55"/>
		<constant value="wd"/>
		<constant value="wdName"/>
		<constant value="nameElement"/>
		<constant value="Msimplepdl!WorkDefinition;"/>
		<constant value=" /\ "/>
		<constant value="26:30-26:34"/>
		<constant value="26:30-26:39"/>
		<constant value="26:42-26:53"/>
		<constant value="26:30-26:53"/>
		<constant value="26:56-26:63"/>
		<constant value="26:30-26:63"/>
		<constant value="Msimplepdl!WorkSequence;"/>
		<constant value="30:30-30:32"/>
		<constant value="Msimplepdl!Ressource;"/>
		<constant value="34:30-34:32"/>
		<constant value="Msimplepdl!RessourceLink;"/>
		<constant value="38:30-38:32"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<push arg="3"/>
			<store arg="4"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<push arg="7"/>
			<push arg="8"/>
			<findme/>
			<call arg="9"/>
			<iterate/>
			<store arg="10"/>
			<load arg="10"/>
			<call arg="11"/>
			<load arg="4"/>
			<load arg="10"/>
			<get arg="12"/>
			<call arg="13"/>
			<push arg="14"/>
			<call arg="13"/>
			<call arg="15"/>
			<call arg="16"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="17" begin="0" end="0"/>
			<lne id="18" begin="5" end="7"/>
			<lne id="19" begin="5" end="8"/>
			<lne id="20" begin="11" end="11"/>
			<lne id="21" begin="11" end="12"/>
			<lne id="22" begin="13" end="13"/>
			<lne id="23" begin="14" end="14"/>
			<lne id="24" begin="14" end="15"/>
			<lne id="25" begin="13" end="16"/>
			<lne id="26" begin="17" end="17"/>
			<lne id="27" begin="13" end="18"/>
			<lne id="28" begin="11" end="19"/>
			<lne id="29" begin="2" end="21"/>
			<lne id="30" begin="0" end="21"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="31" begin="10" end="20"/>
			<lve slot="1" name="32" begin="1" end="21"/>
			<lve slot="0" name="33" begin="0" end="21"/>
		</localvariabletable>
	</operation>
	<operation name="34">
		<context type="2"/>
		<parameters>
			<parameter name="4" type="35"/>
			<parameter name="10" type="35"/>
			<parameter name="36" type="35"/>
		</parameters>
		<code>
			<push arg="37"/>
			<store arg="38"/>
			<load arg="4"/>
			<iterate/>
			<store arg="39"/>
			<load arg="38"/>
			<load arg="10"/>
			<call arg="13"/>
			<load arg="39"/>
			<call arg="13"/>
			<load arg="36"/>
			<call arg="13"/>
			<store arg="38"/>
			<enditerate/>
			<load arg="38"/>
		</code>
		<linenumbertable>
			<lne id="40" begin="0" end="0"/>
			<lne id="41" begin="2" end="2"/>
			<lne id="42" begin="5" end="5"/>
			<lne id="43" begin="6" end="6"/>
			<lne id="44" begin="5" end="7"/>
			<lne id="45" begin="8" end="8"/>
			<lne id="46" begin="5" end="9"/>
			<lne id="47" begin="10" end="10"/>
			<lne id="48" begin="5" end="11"/>
			<lne id="49" begin="0" end="14"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="50" begin="4" end="12"/>
			<lve slot="4" name="51" begin="1" end="14"/>
			<lve slot="0" name="33" begin="0" end="14"/>
			<lve slot="1" name="52" begin="0" end="14"/>
			<lve slot="2" name="53" begin="0" end="14"/>
			<lve slot="3" name="54" begin="0" end="14"/>
		</localvariabletable>
	</operation>
	<operation name="55">
		<context type="56"/>
		<parameters>
		</parameters>
		<code>
			<push arg="57"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<load arg="58"/>
			<get arg="59"/>
			<iterate/>
			<store arg="4"/>
			<load arg="4"/>
			<call arg="60"/>
			<call arg="16"/>
			<enditerate/>
			<store arg="4"/>
			<getasm/>
			<load arg="4"/>
			<push arg="37"/>
			<push arg="37"/>
			<call arg="61"/>
			<call arg="13"/>
			<push arg="62"/>
			<call arg="13"/>
			<push arg="63"/>
			<push arg="64"/>
			<call arg="65"/>
		</code>
		<linenumbertable>
			<lne id="66" begin="0" end="0"/>
			<lne id="67" begin="4" end="4"/>
			<lne id="68" begin="4" end="5"/>
			<lne id="69" begin="8" end="8"/>
			<lne id="70" begin="8" end="9"/>
			<lne id="71" begin="1" end="11"/>
			<lne id="72" begin="13" end="13"/>
			<lne id="73" begin="14" end="14"/>
			<lne id="74" begin="15" end="15"/>
			<lne id="75" begin="16" end="16"/>
			<lne id="76" begin="13" end="17"/>
			<lne id="77" begin="1" end="17"/>
			<lne id="78" begin="0" end="18"/>
			<lne id="79" begin="19" end="19"/>
			<lne id="80" begin="0" end="20"/>
			<lne id="81" begin="21" end="21"/>
			<lne id="82" begin="22" end="22"/>
			<lne id="83" begin="0" end="23"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="84" begin="7" end="10"/>
			<lve slot="1" name="85" begin="12" end="17"/>
			<lve slot="0" name="33" begin="0" end="23"/>
		</localvariabletable>
	</operation>
	<operation name="86">
		<context type="87"/>
		<parameters>
		</parameters>
		<code>
			<load arg="58"/>
			<get arg="12"/>
			<push arg="64"/>
			<call arg="13"/>
			<push arg="88"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="89" begin="0" end="0"/>
			<lne id="90" begin="0" end="1"/>
			<lne id="91" begin="2" end="2"/>
			<lne id="92" begin="0" end="3"/>
			<lne id="93" begin="4" end="4"/>
			<lne id="94" begin="0" end="5"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="86">
		<context type="95"/>
		<parameters>
		</parameters>
		<code>
			<push arg="37"/>
		</code>
		<linenumbertable>
			<lne id="96" begin="0" end="0"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="0"/>
		</localvariabletable>
	</operation>
	<operation name="86">
		<context type="97"/>
		<parameters>
		</parameters>
		<code>
			<push arg="37"/>
		</code>
		<linenumbertable>
			<lne id="98" begin="0" end="0"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="0"/>
		</localvariabletable>
	</operation>
	<operation name="86">
		<context type="99"/>
		<parameters>
		</parameters>
		<code>
			<push arg="37"/>
		</code>
		<linenumbertable>
			<lne id="100" begin="0" end="0"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="0"/>
		</localvariabletable>
	</operation>
</asm>
