package fr.xtext.improvedsimplepdl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import fr.xtext.improvedsimplepdl.services.ImprovedSimplePdlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalImprovedSimplePdlParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'S2S'", "'F2S'", "'S2F'", "'F2F'", "'process'", "'{'", "'}'", "'wd'", "'['", "','", "']'", "'ws'", "'res'", "'use'"
    };
    public static final int RULE_ID=4;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalImprovedSimplePdlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalImprovedSimplePdlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalImprovedSimplePdlParser.tokenNames; }
    public String getGrammarFileName() { return "../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g"; }


     
     	private ImprovedSimplePdlGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ImprovedSimplePdlGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleProcess"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:60:1: entryRuleProcess : ruleProcess EOF ;
    public final void entryRuleProcess() throws RecognitionException {
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:61:1: ( ruleProcess EOF )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:62:1: ruleProcess EOF
            {
             before(grammarAccess.getProcessRule()); 
            pushFollow(FOLLOW_ruleProcess_in_entryRuleProcess61);
            ruleProcess();

            state._fsp--;

             after(grammarAccess.getProcessRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProcess68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProcess"


    // $ANTLR start "ruleProcess"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:69:1: ruleProcess : ( ( rule__Process__Group__0 ) ) ;
    public final void ruleProcess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:73:2: ( ( ( rule__Process__Group__0 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:74:1: ( ( rule__Process__Group__0 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:74:1: ( ( rule__Process__Group__0 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:75:1: ( rule__Process__Group__0 )
            {
             before(grammarAccess.getProcessAccess().getGroup()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:76:1: ( rule__Process__Group__0 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:76:2: rule__Process__Group__0
            {
            pushFollow(FOLLOW_rule__Process__Group__0_in_ruleProcess94);
            rule__Process__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProcessAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProcess"


    // $ANTLR start "entryRuleProcessElement"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:88:1: entryRuleProcessElement : ruleProcessElement EOF ;
    public final void entryRuleProcessElement() throws RecognitionException {
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:89:1: ( ruleProcessElement EOF )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:90:1: ruleProcessElement EOF
            {
             before(grammarAccess.getProcessElementRule()); 
            pushFollow(FOLLOW_ruleProcessElement_in_entryRuleProcessElement121);
            ruleProcessElement();

            state._fsp--;

             after(grammarAccess.getProcessElementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProcessElement128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProcessElement"


    // $ANTLR start "ruleProcessElement"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:97:1: ruleProcessElement : ( ( rule__ProcessElement__Alternatives ) ) ;
    public final void ruleProcessElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:101:2: ( ( ( rule__ProcessElement__Alternatives ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:102:1: ( ( rule__ProcessElement__Alternatives ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:102:1: ( ( rule__ProcessElement__Alternatives ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:103:1: ( rule__ProcessElement__Alternatives )
            {
             before(grammarAccess.getProcessElementAccess().getAlternatives()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:104:1: ( rule__ProcessElement__Alternatives )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:104:2: rule__ProcessElement__Alternatives
            {
            pushFollow(FOLLOW_rule__ProcessElement__Alternatives_in_ruleProcessElement154);
            rule__ProcessElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getProcessElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProcessElement"


    // $ANTLR start "entryRuleWorkDef"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:116:1: entryRuleWorkDef : ruleWorkDef EOF ;
    public final void entryRuleWorkDef() throws RecognitionException {
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:117:1: ( ruleWorkDef EOF )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:118:1: ruleWorkDef EOF
            {
             before(grammarAccess.getWorkDefRule()); 
            pushFollow(FOLLOW_ruleWorkDef_in_entryRuleWorkDef181);
            ruleWorkDef();

            state._fsp--;

             after(grammarAccess.getWorkDefRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWorkDef188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWorkDef"


    // $ANTLR start "ruleWorkDef"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:125:1: ruleWorkDef : ( ( rule__WorkDef__Group__0 ) ) ;
    public final void ruleWorkDef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:129:2: ( ( ( rule__WorkDef__Group__0 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:130:1: ( ( rule__WorkDef__Group__0 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:130:1: ( ( rule__WorkDef__Group__0 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:131:1: ( rule__WorkDef__Group__0 )
            {
             before(grammarAccess.getWorkDefAccess().getGroup()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:132:1: ( rule__WorkDef__Group__0 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:132:2: rule__WorkDef__Group__0
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__0_in_ruleWorkDef214);
            rule__WorkDef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWorkDef"


    // $ANTLR start "entryRuleWorkSeq"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:144:1: entryRuleWorkSeq : ruleWorkSeq EOF ;
    public final void entryRuleWorkSeq() throws RecognitionException {
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:145:1: ( ruleWorkSeq EOF )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:146:1: ruleWorkSeq EOF
            {
             before(grammarAccess.getWorkSeqRule()); 
            pushFollow(FOLLOW_ruleWorkSeq_in_entryRuleWorkSeq241);
            ruleWorkSeq();

            state._fsp--;

             after(grammarAccess.getWorkSeqRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWorkSeq248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWorkSeq"


    // $ANTLR start "ruleWorkSeq"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:153:1: ruleWorkSeq : ( ( rule__WorkSeq__Group__0 ) ) ;
    public final void ruleWorkSeq() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:157:2: ( ( ( rule__WorkSeq__Group__0 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:158:1: ( ( rule__WorkSeq__Group__0 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:158:1: ( ( rule__WorkSeq__Group__0 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:159:1: ( rule__WorkSeq__Group__0 )
            {
             before(grammarAccess.getWorkSeqAccess().getGroup()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:160:1: ( rule__WorkSeq__Group__0 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:160:2: rule__WorkSeq__Group__0
            {
            pushFollow(FOLLOW_rule__WorkSeq__Group__0_in_ruleWorkSeq274);
            rule__WorkSeq__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWorkSeqAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWorkSeq"


    // $ANTLR start "entryRuleRessource"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:174:1: entryRuleRessource : ruleRessource EOF ;
    public final void entryRuleRessource() throws RecognitionException {
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:175:1: ( ruleRessource EOF )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:176:1: ruleRessource EOF
            {
             before(grammarAccess.getRessourceRule()); 
            pushFollow(FOLLOW_ruleRessource_in_entryRuleRessource303);
            ruleRessource();

            state._fsp--;

             after(grammarAccess.getRessourceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRessource310); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRessource"


    // $ANTLR start "ruleRessource"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:183:1: ruleRessource : ( ( rule__Ressource__Group__0 ) ) ;
    public final void ruleRessource() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:187:2: ( ( ( rule__Ressource__Group__0 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:188:1: ( ( rule__Ressource__Group__0 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:188:1: ( ( rule__Ressource__Group__0 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:189:1: ( rule__Ressource__Group__0 )
            {
             before(grammarAccess.getRessourceAccess().getGroup()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:190:1: ( rule__Ressource__Group__0 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:190:2: rule__Ressource__Group__0
            {
            pushFollow(FOLLOW_rule__Ressource__Group__0_in_ruleRessource336);
            rule__Ressource__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRessourceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRessource"


    // $ANTLR start "entryRuleRessourceUsage"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:202:1: entryRuleRessourceUsage : ruleRessourceUsage EOF ;
    public final void entryRuleRessourceUsage() throws RecognitionException {
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:203:1: ( ruleRessourceUsage EOF )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:204:1: ruleRessourceUsage EOF
            {
             before(grammarAccess.getRessourceUsageRule()); 
            pushFollow(FOLLOW_ruleRessourceUsage_in_entryRuleRessourceUsage363);
            ruleRessourceUsage();

            state._fsp--;

             after(grammarAccess.getRessourceUsageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRessourceUsage370); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRessourceUsage"


    // $ANTLR start "ruleRessourceUsage"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:211:1: ruleRessourceUsage : ( ( rule__RessourceUsage__Group__0 ) ) ;
    public final void ruleRessourceUsage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:215:2: ( ( ( rule__RessourceUsage__Group__0 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:216:1: ( ( rule__RessourceUsage__Group__0 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:216:1: ( ( rule__RessourceUsage__Group__0 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:217:1: ( rule__RessourceUsage__Group__0 )
            {
             before(grammarAccess.getRessourceUsageAccess().getGroup()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:218:1: ( rule__RessourceUsage__Group__0 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:218:2: rule__RessourceUsage__Group__0
            {
            pushFollow(FOLLOW_rule__RessourceUsage__Group__0_in_ruleRessourceUsage396);
            rule__RessourceUsage__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRessourceUsageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRessourceUsage"


    // $ANTLR start "ruleWorkSequenceType"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:231:1: ruleWorkSequenceType : ( ( rule__WorkSequenceType__Alternatives ) ) ;
    public final void ruleWorkSequenceType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:235:1: ( ( ( rule__WorkSequenceType__Alternatives ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:236:1: ( ( rule__WorkSequenceType__Alternatives ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:236:1: ( ( rule__WorkSequenceType__Alternatives ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:237:1: ( rule__WorkSequenceType__Alternatives )
            {
             before(grammarAccess.getWorkSequenceTypeAccess().getAlternatives()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:238:1: ( rule__WorkSequenceType__Alternatives )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:238:2: rule__WorkSequenceType__Alternatives
            {
            pushFollow(FOLLOW_rule__WorkSequenceType__Alternatives_in_ruleWorkSequenceType433);
            rule__WorkSequenceType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getWorkSequenceTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWorkSequenceType"


    // $ANTLR start "rule__ProcessElement__Alternatives"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:249:1: rule__ProcessElement__Alternatives : ( ( ruleWorkDef ) | ( ruleWorkSeq ) | ( ruleRessource ) | ( ruleRessourceUsage ) );
    public final void rule__ProcessElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:253:1: ( ( ruleWorkDef ) | ( ruleWorkSeq ) | ( ruleRessource ) | ( ruleRessourceUsage ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt1=1;
                }
                break;
            case 22:
                {
                alt1=2;
                }
                break;
            case 23:
                {
                alt1=3;
                }
                break;
            case 24:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:254:1: ( ruleWorkDef )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:254:1: ( ruleWorkDef )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:255:1: ruleWorkDef
                    {
                     before(grammarAccess.getProcessElementAccess().getWorkDefParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleWorkDef_in_rule__ProcessElement__Alternatives468);
                    ruleWorkDef();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getWorkDefParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:260:6: ( ruleWorkSeq )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:260:6: ( ruleWorkSeq )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:261:1: ruleWorkSeq
                    {
                     before(grammarAccess.getProcessElementAccess().getWorkSeqParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleWorkSeq_in_rule__ProcessElement__Alternatives485);
                    ruleWorkSeq();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getWorkSeqParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:266:6: ( ruleRessource )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:266:6: ( ruleRessource )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:267:1: ruleRessource
                    {
                     before(grammarAccess.getProcessElementAccess().getRessourceParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleRessource_in_rule__ProcessElement__Alternatives502);
                    ruleRessource();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getRessourceParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:272:6: ( ruleRessourceUsage )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:272:6: ( ruleRessourceUsage )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:273:1: ruleRessourceUsage
                    {
                     before(grammarAccess.getProcessElementAccess().getRessourceUsageParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleRessourceUsage_in_rule__ProcessElement__Alternatives519);
                    ruleRessourceUsage();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getRessourceUsageParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcessElement__Alternatives"


    // $ANTLR start "rule__WorkSequenceType__Alternatives"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:283:1: rule__WorkSequenceType__Alternatives : ( ( ( 'S2S' ) ) | ( ( 'F2S' ) ) | ( ( 'S2F' ) ) | ( ( 'F2F' ) ) );
    public final void rule__WorkSequenceType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:287:1: ( ( ( 'S2S' ) ) | ( ( 'F2S' ) ) | ( ( 'S2F' ) ) | ( ( 'F2F' ) ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:288:1: ( ( 'S2S' ) )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:288:1: ( ( 'S2S' ) )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:289:1: ( 'S2S' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getStartToStartEnumLiteralDeclaration_0()); 
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:290:1: ( 'S2S' )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:290:3: 'S2S'
                    {
                    match(input,11,FOLLOW_11_in_rule__WorkSequenceType__Alternatives552); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getStartToStartEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:295:6: ( ( 'F2S' ) )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:295:6: ( ( 'F2S' ) )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:296:1: ( 'F2S' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getFinishToStartEnumLiteralDeclaration_1()); 
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:297:1: ( 'F2S' )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:297:3: 'F2S'
                    {
                    match(input,12,FOLLOW_12_in_rule__WorkSequenceType__Alternatives573); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getFinishToStartEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:302:6: ( ( 'S2F' ) )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:302:6: ( ( 'S2F' ) )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:303:1: ( 'S2F' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getStartToFinishEnumLiteralDeclaration_2()); 
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:304:1: ( 'S2F' )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:304:3: 'S2F'
                    {
                    match(input,13,FOLLOW_13_in_rule__WorkSequenceType__Alternatives594); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getStartToFinishEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:309:6: ( ( 'F2F' ) )
                    {
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:309:6: ( ( 'F2F' ) )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:310:1: ( 'F2F' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getFinishToFinishEnumLiteralDeclaration_3()); 
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:311:1: ( 'F2F' )
                    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:311:3: 'F2F'
                    {
                    match(input,14,FOLLOW_14_in_rule__WorkSequenceType__Alternatives615); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getFinishToFinishEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequenceType__Alternatives"


    // $ANTLR start "rule__Process__Group__0"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:323:1: rule__Process__Group__0 : rule__Process__Group__0__Impl rule__Process__Group__1 ;
    public final void rule__Process__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:327:1: ( rule__Process__Group__0__Impl rule__Process__Group__1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:328:2: rule__Process__Group__0__Impl rule__Process__Group__1
            {
            pushFollow(FOLLOW_rule__Process__Group__0__Impl_in_rule__Process__Group__0648);
            rule__Process__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Process__Group__1_in_rule__Process__Group__0651);
            rule__Process__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__0"


    // $ANTLR start "rule__Process__Group__0__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:335:1: rule__Process__Group__0__Impl : ( 'process' ) ;
    public final void rule__Process__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:339:1: ( ( 'process' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:340:1: ( 'process' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:340:1: ( 'process' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:341:1: 'process'
            {
             before(grammarAccess.getProcessAccess().getProcessKeyword_0()); 
            match(input,15,FOLLOW_15_in_rule__Process__Group__0__Impl679); 
             after(grammarAccess.getProcessAccess().getProcessKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__0__Impl"


    // $ANTLR start "rule__Process__Group__1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:354:1: rule__Process__Group__1 : rule__Process__Group__1__Impl rule__Process__Group__2 ;
    public final void rule__Process__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:358:1: ( rule__Process__Group__1__Impl rule__Process__Group__2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:359:2: rule__Process__Group__1__Impl rule__Process__Group__2
            {
            pushFollow(FOLLOW_rule__Process__Group__1__Impl_in_rule__Process__Group__1710);
            rule__Process__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Process__Group__2_in_rule__Process__Group__1713);
            rule__Process__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__1"


    // $ANTLR start "rule__Process__Group__1__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:366:1: rule__Process__Group__1__Impl : ( ( rule__Process__NameAssignment_1 ) ) ;
    public final void rule__Process__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:370:1: ( ( ( rule__Process__NameAssignment_1 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:371:1: ( ( rule__Process__NameAssignment_1 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:371:1: ( ( rule__Process__NameAssignment_1 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:372:1: ( rule__Process__NameAssignment_1 )
            {
             before(grammarAccess.getProcessAccess().getNameAssignment_1()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:373:1: ( rule__Process__NameAssignment_1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:373:2: rule__Process__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Process__NameAssignment_1_in_rule__Process__Group__1__Impl740);
            rule__Process__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getProcessAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__1__Impl"


    // $ANTLR start "rule__Process__Group__2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:383:1: rule__Process__Group__2 : rule__Process__Group__2__Impl rule__Process__Group__3 ;
    public final void rule__Process__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:387:1: ( rule__Process__Group__2__Impl rule__Process__Group__3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:388:2: rule__Process__Group__2__Impl rule__Process__Group__3
            {
            pushFollow(FOLLOW_rule__Process__Group__2__Impl_in_rule__Process__Group__2770);
            rule__Process__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Process__Group__3_in_rule__Process__Group__2773);
            rule__Process__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__2"


    // $ANTLR start "rule__Process__Group__2__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:395:1: rule__Process__Group__2__Impl : ( '{' ) ;
    public final void rule__Process__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:399:1: ( ( '{' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:400:1: ( '{' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:400:1: ( '{' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:401:1: '{'
            {
             before(grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,16,FOLLOW_16_in_rule__Process__Group__2__Impl801); 
             after(grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__2__Impl"


    // $ANTLR start "rule__Process__Group__3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:414:1: rule__Process__Group__3 : rule__Process__Group__3__Impl rule__Process__Group__4 ;
    public final void rule__Process__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:418:1: ( rule__Process__Group__3__Impl rule__Process__Group__4 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:419:2: rule__Process__Group__3__Impl rule__Process__Group__4
            {
            pushFollow(FOLLOW_rule__Process__Group__3__Impl_in_rule__Process__Group__3832);
            rule__Process__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Process__Group__4_in_rule__Process__Group__3835);
            rule__Process__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__3"


    // $ANTLR start "rule__Process__Group__3__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:426:1: rule__Process__Group__3__Impl : ( ( rule__Process__ProcessElementsAssignment_3 )* ) ;
    public final void rule__Process__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:430:1: ( ( ( rule__Process__ProcessElementsAssignment_3 )* ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:431:1: ( ( rule__Process__ProcessElementsAssignment_3 )* )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:431:1: ( ( rule__Process__ProcessElementsAssignment_3 )* )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:432:1: ( rule__Process__ProcessElementsAssignment_3 )*
            {
             before(grammarAccess.getProcessAccess().getProcessElementsAssignment_3()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:433:1: ( rule__Process__ProcessElementsAssignment_3 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==18||(LA3_0>=22 && LA3_0<=24)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:433:2: rule__Process__ProcessElementsAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Process__ProcessElementsAssignment_3_in_rule__Process__Group__3__Impl862);
            	    rule__Process__ProcessElementsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getProcessAccess().getProcessElementsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__3__Impl"


    // $ANTLR start "rule__Process__Group__4"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:443:1: rule__Process__Group__4 : rule__Process__Group__4__Impl ;
    public final void rule__Process__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:447:1: ( rule__Process__Group__4__Impl )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:448:2: rule__Process__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Process__Group__4__Impl_in_rule__Process__Group__4893);
            rule__Process__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__4"


    // $ANTLR start "rule__Process__Group__4__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:454:1: rule__Process__Group__4__Impl : ( '}' ) ;
    public final void rule__Process__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:458:1: ( ( '}' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:459:1: ( '}' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:459:1: ( '}' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:460:1: '}'
            {
             before(grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_4()); 
            match(input,17,FOLLOW_17_in_rule__Process__Group__4__Impl921); 
             after(grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__4__Impl"


    // $ANTLR start "rule__WorkDef__Group__0"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:483:1: rule__WorkDef__Group__0 : rule__WorkDef__Group__0__Impl rule__WorkDef__Group__1 ;
    public final void rule__WorkDef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:487:1: ( rule__WorkDef__Group__0__Impl rule__WorkDef__Group__1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:488:2: rule__WorkDef__Group__0__Impl rule__WorkDef__Group__1
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__0__Impl_in_rule__WorkDef__Group__0962);
            rule__WorkDef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkDef__Group__1_in_rule__WorkDef__Group__0965);
            rule__WorkDef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__0"


    // $ANTLR start "rule__WorkDef__Group__0__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:495:1: rule__WorkDef__Group__0__Impl : ( 'wd' ) ;
    public final void rule__WorkDef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:499:1: ( ( 'wd' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:500:1: ( 'wd' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:500:1: ( 'wd' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:501:1: 'wd'
            {
             before(grammarAccess.getWorkDefAccess().getWdKeyword_0()); 
            match(input,18,FOLLOW_18_in_rule__WorkDef__Group__0__Impl993); 
             after(grammarAccess.getWorkDefAccess().getWdKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__0__Impl"


    // $ANTLR start "rule__WorkDef__Group__1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:514:1: rule__WorkDef__Group__1 : rule__WorkDef__Group__1__Impl rule__WorkDef__Group__2 ;
    public final void rule__WorkDef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:518:1: ( rule__WorkDef__Group__1__Impl rule__WorkDef__Group__2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:519:2: rule__WorkDef__Group__1__Impl rule__WorkDef__Group__2
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__1__Impl_in_rule__WorkDef__Group__11024);
            rule__WorkDef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkDef__Group__2_in_rule__WorkDef__Group__11027);
            rule__WorkDef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__1"


    // $ANTLR start "rule__WorkDef__Group__1__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:526:1: rule__WorkDef__Group__1__Impl : ( ( rule__WorkDef__NameAssignment_1 ) ) ;
    public final void rule__WorkDef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:530:1: ( ( ( rule__WorkDef__NameAssignment_1 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:531:1: ( ( rule__WorkDef__NameAssignment_1 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:531:1: ( ( rule__WorkDef__NameAssignment_1 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:532:1: ( rule__WorkDef__NameAssignment_1 )
            {
             before(grammarAccess.getWorkDefAccess().getNameAssignment_1()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:533:1: ( rule__WorkDef__NameAssignment_1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:533:2: rule__WorkDef__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__WorkDef__NameAssignment_1_in_rule__WorkDef__Group__1__Impl1054);
            rule__WorkDef__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__1__Impl"


    // $ANTLR start "rule__WorkDef__Group__2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:543:1: rule__WorkDef__Group__2 : rule__WorkDef__Group__2__Impl rule__WorkDef__Group__3 ;
    public final void rule__WorkDef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:547:1: ( rule__WorkDef__Group__2__Impl rule__WorkDef__Group__3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:548:2: rule__WorkDef__Group__2__Impl rule__WorkDef__Group__3
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__2__Impl_in_rule__WorkDef__Group__21084);
            rule__WorkDef__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkDef__Group__3_in_rule__WorkDef__Group__21087);
            rule__WorkDef__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__2"


    // $ANTLR start "rule__WorkDef__Group__2__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:555:1: rule__WorkDef__Group__2__Impl : ( '[' ) ;
    public final void rule__WorkDef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:559:1: ( ( '[' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:560:1: ( '[' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:560:1: ( '[' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:561:1: '['
            {
             before(grammarAccess.getWorkDefAccess().getLeftSquareBracketKeyword_2()); 
            match(input,19,FOLLOW_19_in_rule__WorkDef__Group__2__Impl1115); 
             after(grammarAccess.getWorkDefAccess().getLeftSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__2__Impl"


    // $ANTLR start "rule__WorkDef__Group__3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:574:1: rule__WorkDef__Group__3 : rule__WorkDef__Group__3__Impl rule__WorkDef__Group__4 ;
    public final void rule__WorkDef__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:578:1: ( rule__WorkDef__Group__3__Impl rule__WorkDef__Group__4 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:579:2: rule__WorkDef__Group__3__Impl rule__WorkDef__Group__4
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__3__Impl_in_rule__WorkDef__Group__31146);
            rule__WorkDef__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkDef__Group__4_in_rule__WorkDef__Group__31149);
            rule__WorkDef__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__3"


    // $ANTLR start "rule__WorkDef__Group__3__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:586:1: rule__WorkDef__Group__3__Impl : ( ( rule__WorkDef__Min_timeAssignment_3 ) ) ;
    public final void rule__WorkDef__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:590:1: ( ( ( rule__WorkDef__Min_timeAssignment_3 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:591:1: ( ( rule__WorkDef__Min_timeAssignment_3 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:591:1: ( ( rule__WorkDef__Min_timeAssignment_3 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:592:1: ( rule__WorkDef__Min_timeAssignment_3 )
            {
             before(grammarAccess.getWorkDefAccess().getMin_timeAssignment_3()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:593:1: ( rule__WorkDef__Min_timeAssignment_3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:593:2: rule__WorkDef__Min_timeAssignment_3
            {
            pushFollow(FOLLOW_rule__WorkDef__Min_timeAssignment_3_in_rule__WorkDef__Group__3__Impl1176);
            rule__WorkDef__Min_timeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefAccess().getMin_timeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__3__Impl"


    // $ANTLR start "rule__WorkDef__Group__4"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:603:1: rule__WorkDef__Group__4 : rule__WorkDef__Group__4__Impl rule__WorkDef__Group__5 ;
    public final void rule__WorkDef__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:607:1: ( rule__WorkDef__Group__4__Impl rule__WorkDef__Group__5 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:608:2: rule__WorkDef__Group__4__Impl rule__WorkDef__Group__5
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__4__Impl_in_rule__WorkDef__Group__41206);
            rule__WorkDef__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkDef__Group__5_in_rule__WorkDef__Group__41209);
            rule__WorkDef__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__4"


    // $ANTLR start "rule__WorkDef__Group__4__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:615:1: rule__WorkDef__Group__4__Impl : ( ',' ) ;
    public final void rule__WorkDef__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:619:1: ( ( ',' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:620:1: ( ',' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:620:1: ( ',' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:621:1: ','
            {
             before(grammarAccess.getWorkDefAccess().getCommaKeyword_4()); 
            match(input,20,FOLLOW_20_in_rule__WorkDef__Group__4__Impl1237); 
             after(grammarAccess.getWorkDefAccess().getCommaKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__4__Impl"


    // $ANTLR start "rule__WorkDef__Group__5"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:634:1: rule__WorkDef__Group__5 : rule__WorkDef__Group__5__Impl rule__WorkDef__Group__6 ;
    public final void rule__WorkDef__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:638:1: ( rule__WorkDef__Group__5__Impl rule__WorkDef__Group__6 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:639:2: rule__WorkDef__Group__5__Impl rule__WorkDef__Group__6
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__5__Impl_in_rule__WorkDef__Group__51268);
            rule__WorkDef__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkDef__Group__6_in_rule__WorkDef__Group__51271);
            rule__WorkDef__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__5"


    // $ANTLR start "rule__WorkDef__Group__5__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:646:1: rule__WorkDef__Group__5__Impl : ( ( rule__WorkDef__Max_timeAssignment_5 ) ) ;
    public final void rule__WorkDef__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:650:1: ( ( ( rule__WorkDef__Max_timeAssignment_5 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:651:1: ( ( rule__WorkDef__Max_timeAssignment_5 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:651:1: ( ( rule__WorkDef__Max_timeAssignment_5 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:652:1: ( rule__WorkDef__Max_timeAssignment_5 )
            {
             before(grammarAccess.getWorkDefAccess().getMax_timeAssignment_5()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:653:1: ( rule__WorkDef__Max_timeAssignment_5 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:653:2: rule__WorkDef__Max_timeAssignment_5
            {
            pushFollow(FOLLOW_rule__WorkDef__Max_timeAssignment_5_in_rule__WorkDef__Group__5__Impl1298);
            rule__WorkDef__Max_timeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefAccess().getMax_timeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__5__Impl"


    // $ANTLR start "rule__WorkDef__Group__6"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:663:1: rule__WorkDef__Group__6 : rule__WorkDef__Group__6__Impl ;
    public final void rule__WorkDef__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:667:1: ( rule__WorkDef__Group__6__Impl )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:668:2: rule__WorkDef__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__WorkDef__Group__6__Impl_in_rule__WorkDef__Group__61328);
            rule__WorkDef__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__6"


    // $ANTLR start "rule__WorkDef__Group__6__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:674:1: rule__WorkDef__Group__6__Impl : ( ']' ) ;
    public final void rule__WorkDef__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:678:1: ( ( ']' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:679:1: ( ']' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:679:1: ( ']' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:680:1: ']'
            {
             before(grammarAccess.getWorkDefAccess().getRightSquareBracketKeyword_6()); 
            match(input,21,FOLLOW_21_in_rule__WorkDef__Group__6__Impl1356); 
             after(grammarAccess.getWorkDefAccess().getRightSquareBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Group__6__Impl"


    // $ANTLR start "rule__WorkSeq__Group__0"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:707:1: rule__WorkSeq__Group__0 : rule__WorkSeq__Group__0__Impl rule__WorkSeq__Group__1 ;
    public final void rule__WorkSeq__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:711:1: ( rule__WorkSeq__Group__0__Impl rule__WorkSeq__Group__1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:712:2: rule__WorkSeq__Group__0__Impl rule__WorkSeq__Group__1
            {
            pushFollow(FOLLOW_rule__WorkSeq__Group__0__Impl_in_rule__WorkSeq__Group__01401);
            rule__WorkSeq__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkSeq__Group__1_in_rule__WorkSeq__Group__01404);
            rule__WorkSeq__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__0"


    // $ANTLR start "rule__WorkSeq__Group__0__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:719:1: rule__WorkSeq__Group__0__Impl : ( 'ws' ) ;
    public final void rule__WorkSeq__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:723:1: ( ( 'ws' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:724:1: ( 'ws' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:724:1: ( 'ws' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:725:1: 'ws'
            {
             before(grammarAccess.getWorkSeqAccess().getWsKeyword_0()); 
            match(input,22,FOLLOW_22_in_rule__WorkSeq__Group__0__Impl1432); 
             after(grammarAccess.getWorkSeqAccess().getWsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__0__Impl"


    // $ANTLR start "rule__WorkSeq__Group__1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:738:1: rule__WorkSeq__Group__1 : rule__WorkSeq__Group__1__Impl rule__WorkSeq__Group__2 ;
    public final void rule__WorkSeq__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:742:1: ( rule__WorkSeq__Group__1__Impl rule__WorkSeq__Group__2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:743:2: rule__WorkSeq__Group__1__Impl rule__WorkSeq__Group__2
            {
            pushFollow(FOLLOW_rule__WorkSeq__Group__1__Impl_in_rule__WorkSeq__Group__11463);
            rule__WorkSeq__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkSeq__Group__2_in_rule__WorkSeq__Group__11466);
            rule__WorkSeq__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__1"


    // $ANTLR start "rule__WorkSeq__Group__1__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:750:1: rule__WorkSeq__Group__1__Impl : ( ( rule__WorkSeq__PredecessorAssignment_1 ) ) ;
    public final void rule__WorkSeq__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:754:1: ( ( ( rule__WorkSeq__PredecessorAssignment_1 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:755:1: ( ( rule__WorkSeq__PredecessorAssignment_1 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:755:1: ( ( rule__WorkSeq__PredecessorAssignment_1 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:756:1: ( rule__WorkSeq__PredecessorAssignment_1 )
            {
             before(grammarAccess.getWorkSeqAccess().getPredecessorAssignment_1()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:757:1: ( rule__WorkSeq__PredecessorAssignment_1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:757:2: rule__WorkSeq__PredecessorAssignment_1
            {
            pushFollow(FOLLOW_rule__WorkSeq__PredecessorAssignment_1_in_rule__WorkSeq__Group__1__Impl1493);
            rule__WorkSeq__PredecessorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWorkSeqAccess().getPredecessorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__1__Impl"


    // $ANTLR start "rule__WorkSeq__Group__2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:767:1: rule__WorkSeq__Group__2 : rule__WorkSeq__Group__2__Impl rule__WorkSeq__Group__3 ;
    public final void rule__WorkSeq__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:771:1: ( rule__WorkSeq__Group__2__Impl rule__WorkSeq__Group__3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:772:2: rule__WorkSeq__Group__2__Impl rule__WorkSeq__Group__3
            {
            pushFollow(FOLLOW_rule__WorkSeq__Group__2__Impl_in_rule__WorkSeq__Group__21523);
            rule__WorkSeq__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WorkSeq__Group__3_in_rule__WorkSeq__Group__21526);
            rule__WorkSeq__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__2"


    // $ANTLR start "rule__WorkSeq__Group__2__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:779:1: rule__WorkSeq__Group__2__Impl : ( ( rule__WorkSeq__LinkTypeAssignment_2 ) ) ;
    public final void rule__WorkSeq__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:783:1: ( ( ( rule__WorkSeq__LinkTypeAssignment_2 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:784:1: ( ( rule__WorkSeq__LinkTypeAssignment_2 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:784:1: ( ( rule__WorkSeq__LinkTypeAssignment_2 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:785:1: ( rule__WorkSeq__LinkTypeAssignment_2 )
            {
             before(grammarAccess.getWorkSeqAccess().getLinkTypeAssignment_2()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:786:1: ( rule__WorkSeq__LinkTypeAssignment_2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:786:2: rule__WorkSeq__LinkTypeAssignment_2
            {
            pushFollow(FOLLOW_rule__WorkSeq__LinkTypeAssignment_2_in_rule__WorkSeq__Group__2__Impl1553);
            rule__WorkSeq__LinkTypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getWorkSeqAccess().getLinkTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__2__Impl"


    // $ANTLR start "rule__WorkSeq__Group__3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:796:1: rule__WorkSeq__Group__3 : rule__WorkSeq__Group__3__Impl ;
    public final void rule__WorkSeq__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:800:1: ( rule__WorkSeq__Group__3__Impl )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:801:2: rule__WorkSeq__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__WorkSeq__Group__3__Impl_in_rule__WorkSeq__Group__31583);
            rule__WorkSeq__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__3"


    // $ANTLR start "rule__WorkSeq__Group__3__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:807:1: rule__WorkSeq__Group__3__Impl : ( ( rule__WorkSeq__SuccessorAssignment_3 ) ) ;
    public final void rule__WorkSeq__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:811:1: ( ( ( rule__WorkSeq__SuccessorAssignment_3 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:812:1: ( ( rule__WorkSeq__SuccessorAssignment_3 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:812:1: ( ( rule__WorkSeq__SuccessorAssignment_3 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:813:1: ( rule__WorkSeq__SuccessorAssignment_3 )
            {
             before(grammarAccess.getWorkSeqAccess().getSuccessorAssignment_3()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:814:1: ( rule__WorkSeq__SuccessorAssignment_3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:814:2: rule__WorkSeq__SuccessorAssignment_3
            {
            pushFollow(FOLLOW_rule__WorkSeq__SuccessorAssignment_3_in_rule__WorkSeq__Group__3__Impl1610);
            rule__WorkSeq__SuccessorAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getWorkSeqAccess().getSuccessorAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__Group__3__Impl"


    // $ANTLR start "rule__Ressource__Group__0"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:833:1: rule__Ressource__Group__0 : rule__Ressource__Group__0__Impl rule__Ressource__Group__1 ;
    public final void rule__Ressource__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:837:1: ( rule__Ressource__Group__0__Impl rule__Ressource__Group__1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:838:2: rule__Ressource__Group__0__Impl rule__Ressource__Group__1
            {
            pushFollow(FOLLOW_rule__Ressource__Group__0__Impl_in_rule__Ressource__Group__01649);
            rule__Ressource__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Ressource__Group__1_in_rule__Ressource__Group__01652);
            rule__Ressource__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__Group__0"


    // $ANTLR start "rule__Ressource__Group__0__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:845:1: rule__Ressource__Group__0__Impl : ( 'res' ) ;
    public final void rule__Ressource__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:849:1: ( ( 'res' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:850:1: ( 'res' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:850:1: ( 'res' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:851:1: 'res'
            {
             before(grammarAccess.getRessourceAccess().getResKeyword_0()); 
            match(input,23,FOLLOW_23_in_rule__Ressource__Group__0__Impl1680); 
             after(grammarAccess.getRessourceAccess().getResKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__Group__0__Impl"


    // $ANTLR start "rule__Ressource__Group__1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:864:1: rule__Ressource__Group__1 : rule__Ressource__Group__1__Impl rule__Ressource__Group__2 ;
    public final void rule__Ressource__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:868:1: ( rule__Ressource__Group__1__Impl rule__Ressource__Group__2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:869:2: rule__Ressource__Group__1__Impl rule__Ressource__Group__2
            {
            pushFollow(FOLLOW_rule__Ressource__Group__1__Impl_in_rule__Ressource__Group__11711);
            rule__Ressource__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Ressource__Group__2_in_rule__Ressource__Group__11714);
            rule__Ressource__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__Group__1"


    // $ANTLR start "rule__Ressource__Group__1__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:876:1: rule__Ressource__Group__1__Impl : ( ( rule__Ressource__NameAssignment_1 ) ) ;
    public final void rule__Ressource__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:880:1: ( ( ( rule__Ressource__NameAssignment_1 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:881:1: ( ( rule__Ressource__NameAssignment_1 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:881:1: ( ( rule__Ressource__NameAssignment_1 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:882:1: ( rule__Ressource__NameAssignment_1 )
            {
             before(grammarAccess.getRessourceAccess().getNameAssignment_1()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:883:1: ( rule__Ressource__NameAssignment_1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:883:2: rule__Ressource__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Ressource__NameAssignment_1_in_rule__Ressource__Group__1__Impl1741);
            rule__Ressource__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRessourceAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__Group__1__Impl"


    // $ANTLR start "rule__Ressource__Group__2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:893:1: rule__Ressource__Group__2 : rule__Ressource__Group__2__Impl ;
    public final void rule__Ressource__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:897:1: ( rule__Ressource__Group__2__Impl )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:898:2: rule__Ressource__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Ressource__Group__2__Impl_in_rule__Ressource__Group__21771);
            rule__Ressource__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__Group__2"


    // $ANTLR start "rule__Ressource__Group__2__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:904:1: rule__Ressource__Group__2__Impl : ( ( rule__Ressource__QuantityAssignment_2 ) ) ;
    public final void rule__Ressource__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:908:1: ( ( ( rule__Ressource__QuantityAssignment_2 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:909:1: ( ( rule__Ressource__QuantityAssignment_2 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:909:1: ( ( rule__Ressource__QuantityAssignment_2 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:910:1: ( rule__Ressource__QuantityAssignment_2 )
            {
             before(grammarAccess.getRessourceAccess().getQuantityAssignment_2()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:911:1: ( rule__Ressource__QuantityAssignment_2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:911:2: rule__Ressource__QuantityAssignment_2
            {
            pushFollow(FOLLOW_rule__Ressource__QuantityAssignment_2_in_rule__Ressource__Group__2__Impl1798);
            rule__Ressource__QuantityAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRessourceAccess().getQuantityAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__Group__2__Impl"


    // $ANTLR start "rule__RessourceUsage__Group__0"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:927:1: rule__RessourceUsage__Group__0 : rule__RessourceUsage__Group__0__Impl rule__RessourceUsage__Group__1 ;
    public final void rule__RessourceUsage__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:931:1: ( rule__RessourceUsage__Group__0__Impl rule__RessourceUsage__Group__1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:932:2: rule__RessourceUsage__Group__0__Impl rule__RessourceUsage__Group__1
            {
            pushFollow(FOLLOW_rule__RessourceUsage__Group__0__Impl_in_rule__RessourceUsage__Group__01834);
            rule__RessourceUsage__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RessourceUsage__Group__1_in_rule__RessourceUsage__Group__01837);
            rule__RessourceUsage__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__0"


    // $ANTLR start "rule__RessourceUsage__Group__0__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:939:1: rule__RessourceUsage__Group__0__Impl : ( 'use' ) ;
    public final void rule__RessourceUsage__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:943:1: ( ( 'use' ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:944:1: ( 'use' )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:944:1: ( 'use' )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:945:1: 'use'
            {
             before(grammarAccess.getRessourceUsageAccess().getUseKeyword_0()); 
            match(input,24,FOLLOW_24_in_rule__RessourceUsage__Group__0__Impl1865); 
             after(grammarAccess.getRessourceUsageAccess().getUseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__0__Impl"


    // $ANTLR start "rule__RessourceUsage__Group__1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:958:1: rule__RessourceUsage__Group__1 : rule__RessourceUsage__Group__1__Impl rule__RessourceUsage__Group__2 ;
    public final void rule__RessourceUsage__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:962:1: ( rule__RessourceUsage__Group__1__Impl rule__RessourceUsage__Group__2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:963:2: rule__RessourceUsage__Group__1__Impl rule__RessourceUsage__Group__2
            {
            pushFollow(FOLLOW_rule__RessourceUsage__Group__1__Impl_in_rule__RessourceUsage__Group__11896);
            rule__RessourceUsage__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RessourceUsage__Group__2_in_rule__RessourceUsage__Group__11899);
            rule__RessourceUsage__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__1"


    // $ANTLR start "rule__RessourceUsage__Group__1__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:970:1: rule__RessourceUsage__Group__1__Impl : ( ( rule__RessourceUsage__WorkDefAssignment_1 ) ) ;
    public final void rule__RessourceUsage__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:974:1: ( ( ( rule__RessourceUsage__WorkDefAssignment_1 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:975:1: ( ( rule__RessourceUsage__WorkDefAssignment_1 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:975:1: ( ( rule__RessourceUsage__WorkDefAssignment_1 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:976:1: ( rule__RessourceUsage__WorkDefAssignment_1 )
            {
             before(grammarAccess.getRessourceUsageAccess().getWorkDefAssignment_1()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:977:1: ( rule__RessourceUsage__WorkDefAssignment_1 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:977:2: rule__RessourceUsage__WorkDefAssignment_1
            {
            pushFollow(FOLLOW_rule__RessourceUsage__WorkDefAssignment_1_in_rule__RessourceUsage__Group__1__Impl1926);
            rule__RessourceUsage__WorkDefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRessourceUsageAccess().getWorkDefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__1__Impl"


    // $ANTLR start "rule__RessourceUsage__Group__2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:987:1: rule__RessourceUsage__Group__2 : rule__RessourceUsage__Group__2__Impl rule__RessourceUsage__Group__3 ;
    public final void rule__RessourceUsage__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:991:1: ( rule__RessourceUsage__Group__2__Impl rule__RessourceUsage__Group__3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:992:2: rule__RessourceUsage__Group__2__Impl rule__RessourceUsage__Group__3
            {
            pushFollow(FOLLOW_rule__RessourceUsage__Group__2__Impl_in_rule__RessourceUsage__Group__21956);
            rule__RessourceUsage__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RessourceUsage__Group__3_in_rule__RessourceUsage__Group__21959);
            rule__RessourceUsage__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__2"


    // $ANTLR start "rule__RessourceUsage__Group__2__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:999:1: rule__RessourceUsage__Group__2__Impl : ( ( rule__RessourceUsage__RessourceAssignment_2 ) ) ;
    public final void rule__RessourceUsage__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1003:1: ( ( ( rule__RessourceUsage__RessourceAssignment_2 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1004:1: ( ( rule__RessourceUsage__RessourceAssignment_2 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1004:1: ( ( rule__RessourceUsage__RessourceAssignment_2 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1005:1: ( rule__RessourceUsage__RessourceAssignment_2 )
            {
             before(grammarAccess.getRessourceUsageAccess().getRessourceAssignment_2()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1006:1: ( rule__RessourceUsage__RessourceAssignment_2 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1006:2: rule__RessourceUsage__RessourceAssignment_2
            {
            pushFollow(FOLLOW_rule__RessourceUsage__RessourceAssignment_2_in_rule__RessourceUsage__Group__2__Impl1986);
            rule__RessourceUsage__RessourceAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRessourceUsageAccess().getRessourceAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__2__Impl"


    // $ANTLR start "rule__RessourceUsage__Group__3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1016:1: rule__RessourceUsage__Group__3 : rule__RessourceUsage__Group__3__Impl ;
    public final void rule__RessourceUsage__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1020:1: ( rule__RessourceUsage__Group__3__Impl )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1021:2: rule__RessourceUsage__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__RessourceUsage__Group__3__Impl_in_rule__RessourceUsage__Group__32016);
            rule__RessourceUsage__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__3"


    // $ANTLR start "rule__RessourceUsage__Group__3__Impl"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1027:1: rule__RessourceUsage__Group__3__Impl : ( ( rule__RessourceUsage__WeightAssignment_3 ) ) ;
    public final void rule__RessourceUsage__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1031:1: ( ( ( rule__RessourceUsage__WeightAssignment_3 ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1032:1: ( ( rule__RessourceUsage__WeightAssignment_3 ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1032:1: ( ( rule__RessourceUsage__WeightAssignment_3 ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1033:1: ( rule__RessourceUsage__WeightAssignment_3 )
            {
             before(grammarAccess.getRessourceUsageAccess().getWeightAssignment_3()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1034:1: ( rule__RessourceUsage__WeightAssignment_3 )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1034:2: rule__RessourceUsage__WeightAssignment_3
            {
            pushFollow(FOLLOW_rule__RessourceUsage__WeightAssignment_3_in_rule__RessourceUsage__Group__3__Impl2043);
            rule__RessourceUsage__WeightAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRessourceUsageAccess().getWeightAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__Group__3__Impl"


    // $ANTLR start "rule__Process__NameAssignment_1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1053:1: rule__Process__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Process__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1057:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1058:1: ( RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1058:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1059:1: RULE_ID
            {
             before(grammarAccess.getProcessAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Process__NameAssignment_12086); 
             after(grammarAccess.getProcessAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__NameAssignment_1"


    // $ANTLR start "rule__Process__ProcessElementsAssignment_3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1068:1: rule__Process__ProcessElementsAssignment_3 : ( ruleProcessElement ) ;
    public final void rule__Process__ProcessElementsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1072:1: ( ( ruleProcessElement ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1073:1: ( ruleProcessElement )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1073:1: ( ruleProcessElement )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1074:1: ruleProcessElement
            {
             before(grammarAccess.getProcessAccess().getProcessElementsProcessElementParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleProcessElement_in_rule__Process__ProcessElementsAssignment_32117);
            ruleProcessElement();

            state._fsp--;

             after(grammarAccess.getProcessAccess().getProcessElementsProcessElementParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__ProcessElementsAssignment_3"


    // $ANTLR start "rule__WorkDef__NameAssignment_1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1083:1: rule__WorkDef__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__WorkDef__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1087:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1088:1: ( RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1088:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1089:1: RULE_ID
            {
             before(grammarAccess.getWorkDefAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__WorkDef__NameAssignment_12148); 
             after(grammarAccess.getWorkDefAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__NameAssignment_1"


    // $ANTLR start "rule__WorkDef__Min_timeAssignment_3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1098:1: rule__WorkDef__Min_timeAssignment_3 : ( RULE_INT ) ;
    public final void rule__WorkDef__Min_timeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1102:1: ( ( RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1103:1: ( RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1103:1: ( RULE_INT )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1104:1: RULE_INT
            {
             before(grammarAccess.getWorkDefAccess().getMin_timeINTTerminalRuleCall_3_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__WorkDef__Min_timeAssignment_32179); 
             after(grammarAccess.getWorkDefAccess().getMin_timeINTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Min_timeAssignment_3"


    // $ANTLR start "rule__WorkDef__Max_timeAssignment_5"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1113:1: rule__WorkDef__Max_timeAssignment_5 : ( RULE_INT ) ;
    public final void rule__WorkDef__Max_timeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1117:1: ( ( RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1118:1: ( RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1118:1: ( RULE_INT )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1119:1: RULE_INT
            {
             before(grammarAccess.getWorkDefAccess().getMax_timeINTTerminalRuleCall_5_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__WorkDef__Max_timeAssignment_52210); 
             after(grammarAccess.getWorkDefAccess().getMax_timeINTTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDef__Max_timeAssignment_5"


    // $ANTLR start "rule__WorkSeq__PredecessorAssignment_1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1128:1: rule__WorkSeq__PredecessorAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__WorkSeq__PredecessorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1132:1: ( ( ( RULE_ID ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1133:1: ( ( RULE_ID ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1133:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1134:1: ( RULE_ID )
            {
             before(grammarAccess.getWorkSeqAccess().getPredecessorWorkDefinitionCrossReference_1_0()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1135:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1136:1: RULE_ID
            {
             before(grammarAccess.getWorkSeqAccess().getPredecessorWorkDefinitionIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__WorkSeq__PredecessorAssignment_12245); 
             after(grammarAccess.getWorkSeqAccess().getPredecessorWorkDefinitionIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getWorkSeqAccess().getPredecessorWorkDefinitionCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__PredecessorAssignment_1"


    // $ANTLR start "rule__WorkSeq__LinkTypeAssignment_2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1147:1: rule__WorkSeq__LinkTypeAssignment_2 : ( ruleWorkSequenceType ) ;
    public final void rule__WorkSeq__LinkTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1151:1: ( ( ruleWorkSequenceType ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1152:1: ( ruleWorkSequenceType )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1152:1: ( ruleWorkSequenceType )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1153:1: ruleWorkSequenceType
            {
             before(grammarAccess.getWorkSeqAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleWorkSequenceType_in_rule__WorkSeq__LinkTypeAssignment_22280);
            ruleWorkSequenceType();

            state._fsp--;

             after(grammarAccess.getWorkSeqAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__LinkTypeAssignment_2"


    // $ANTLR start "rule__WorkSeq__SuccessorAssignment_3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1162:1: rule__WorkSeq__SuccessorAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__WorkSeq__SuccessorAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1166:1: ( ( ( RULE_ID ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1167:1: ( ( RULE_ID ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1167:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1168:1: ( RULE_ID )
            {
             before(grammarAccess.getWorkSeqAccess().getSuccessorWorkDefinitionCrossReference_3_0()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1169:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1170:1: RULE_ID
            {
             before(grammarAccess.getWorkSeqAccess().getSuccessorWorkDefinitionIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__WorkSeq__SuccessorAssignment_32315); 
             after(grammarAccess.getWorkSeqAccess().getSuccessorWorkDefinitionIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getWorkSeqAccess().getSuccessorWorkDefinitionCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSeq__SuccessorAssignment_3"


    // $ANTLR start "rule__Ressource__NameAssignment_1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1182:1: rule__Ressource__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Ressource__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1186:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1187:1: ( RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1187:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1188:1: RULE_ID
            {
             before(grammarAccess.getRessourceAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Ressource__NameAssignment_12351); 
             after(grammarAccess.getRessourceAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__NameAssignment_1"


    // $ANTLR start "rule__Ressource__QuantityAssignment_2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1197:1: rule__Ressource__QuantityAssignment_2 : ( RULE_INT ) ;
    public final void rule__Ressource__QuantityAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1201:1: ( ( RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1202:1: ( RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1202:1: ( RULE_INT )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1203:1: RULE_INT
            {
             before(grammarAccess.getRessourceAccess().getQuantityINTTerminalRuleCall_2_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Ressource__QuantityAssignment_22382); 
             after(grammarAccess.getRessourceAccess().getQuantityINTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ressource__QuantityAssignment_2"


    // $ANTLR start "rule__RessourceUsage__WorkDefAssignment_1"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1212:1: rule__RessourceUsage__WorkDefAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__RessourceUsage__WorkDefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1216:1: ( ( ( RULE_ID ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1217:1: ( ( RULE_ID ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1217:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1218:1: ( RULE_ID )
            {
             before(grammarAccess.getRessourceUsageAccess().getWorkDefWorkDefinitionCrossReference_1_0()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1219:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1220:1: RULE_ID
            {
             before(grammarAccess.getRessourceUsageAccess().getWorkDefWorkDefinitionIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RessourceUsage__WorkDefAssignment_12417); 
             after(grammarAccess.getRessourceUsageAccess().getWorkDefWorkDefinitionIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getRessourceUsageAccess().getWorkDefWorkDefinitionCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__WorkDefAssignment_1"


    // $ANTLR start "rule__RessourceUsage__RessourceAssignment_2"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1231:1: rule__RessourceUsage__RessourceAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__RessourceUsage__RessourceAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1235:1: ( ( ( RULE_ID ) ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1236:1: ( ( RULE_ID ) )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1236:1: ( ( RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1237:1: ( RULE_ID )
            {
             before(grammarAccess.getRessourceUsageAccess().getRessourceRessourceCrossReference_2_0()); 
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1238:1: ( RULE_ID )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1239:1: RULE_ID
            {
             before(grammarAccess.getRessourceUsageAccess().getRessourceRessourceIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RessourceUsage__RessourceAssignment_22456); 
             after(grammarAccess.getRessourceUsageAccess().getRessourceRessourceIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getRessourceUsageAccess().getRessourceRessourceCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__RessourceAssignment_2"


    // $ANTLR start "rule__RessourceUsage__WeightAssignment_3"
    // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1250:1: rule__RessourceUsage__WeightAssignment_3 : ( RULE_INT ) ;
    public final void rule__RessourceUsage__WeightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1254:1: ( ( RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1255:1: ( RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1255:1: ( RULE_INT )
            // ../fr.xtext.improvedsimplepdl.ui/src-gen/fr/xtext/improvedsimplepdl/ui/contentassist/antlr/internal/InternalImprovedSimplePdl.g:1256:1: RULE_INT
            {
             before(grammarAccess.getRessourceUsageAccess().getWeightINTTerminalRuleCall_3_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__RessourceUsage__WeightAssignment_32491); 
             after(grammarAccess.getRessourceUsageAccess().getWeightINTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RessourceUsage__WeightAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleProcess_in_entryRuleProcess61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProcess68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__Group__0_in_ruleProcess94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProcessElement_in_entryRuleProcessElement121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProcessElement128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProcessElement__Alternatives_in_ruleProcessElement154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkDef_in_entryRuleWorkDef181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWorkDef188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__0_in_ruleWorkDef214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkSeq_in_entryRuleWorkSeq241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWorkSeq248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__0_in_ruleWorkSeq274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessource_in_entryRuleRessource303 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRessource310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ressource__Group__0_in_ruleRessource336 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessourceUsage_in_entryRuleRessourceUsage363 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRessourceUsage370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__0_in_ruleRessourceUsage396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSequenceType__Alternatives_in_ruleWorkSequenceType433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkDef_in_rule__ProcessElement__Alternatives468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkSeq_in_rule__ProcessElement__Alternatives485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessource_in_rule__ProcessElement__Alternatives502 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessourceUsage_in_rule__ProcessElement__Alternatives519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__WorkSequenceType__Alternatives552 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__WorkSequenceType__Alternatives573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__WorkSequenceType__Alternatives594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__WorkSequenceType__Alternatives615 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__Group__0__Impl_in_rule__Process__Group__0648 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Process__Group__1_in_rule__Process__Group__0651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Process__Group__0__Impl679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__Group__1__Impl_in_rule__Process__Group__1710 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Process__Group__2_in_rule__Process__Group__1713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__NameAssignment_1_in_rule__Process__Group__1__Impl740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__Group__2__Impl_in_rule__Process__Group__2770 = new BitSet(new long[]{0x0000000001C60000L});
    public static final BitSet FOLLOW_rule__Process__Group__3_in_rule__Process__Group__2773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Process__Group__2__Impl801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__Group__3__Impl_in_rule__Process__Group__3832 = new BitSet(new long[]{0x0000000001C60000L});
    public static final BitSet FOLLOW_rule__Process__Group__4_in_rule__Process__Group__3835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Process__ProcessElementsAssignment_3_in_rule__Process__Group__3__Impl862 = new BitSet(new long[]{0x0000000001C40002L});
    public static final BitSet FOLLOW_rule__Process__Group__4__Impl_in_rule__Process__Group__4893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Process__Group__4__Impl921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__0__Impl_in_rule__WorkDef__Group__0962 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__1_in_rule__WorkDef__Group__0965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__WorkDef__Group__0__Impl993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__1__Impl_in_rule__WorkDef__Group__11024 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__2_in_rule__WorkDef__Group__11027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__NameAssignment_1_in_rule__WorkDef__Group__1__Impl1054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__2__Impl_in_rule__WorkDef__Group__21084 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__3_in_rule__WorkDef__Group__21087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__WorkDef__Group__2__Impl1115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__3__Impl_in_rule__WorkDef__Group__31146 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__4_in_rule__WorkDef__Group__31149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Min_timeAssignment_3_in_rule__WorkDef__Group__3__Impl1176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__4__Impl_in_rule__WorkDef__Group__41206 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__5_in_rule__WorkDef__Group__41209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__WorkDef__Group__4__Impl1237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__5__Impl_in_rule__WorkDef__Group__51268 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__6_in_rule__WorkDef__Group__51271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Max_timeAssignment_5_in_rule__WorkDef__Group__5__Impl1298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkDef__Group__6__Impl_in_rule__WorkDef__Group__61328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__WorkDef__Group__6__Impl1356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__0__Impl_in_rule__WorkSeq__Group__01401 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__1_in_rule__WorkSeq__Group__01404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__WorkSeq__Group__0__Impl1432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__1__Impl_in_rule__WorkSeq__Group__11463 = new BitSet(new long[]{0x0000000000007800L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__2_in_rule__WorkSeq__Group__11466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__PredecessorAssignment_1_in_rule__WorkSeq__Group__1__Impl1493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__2__Impl_in_rule__WorkSeq__Group__21523 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__3_in_rule__WorkSeq__Group__21526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__LinkTypeAssignment_2_in_rule__WorkSeq__Group__2__Impl1553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__Group__3__Impl_in_rule__WorkSeq__Group__31583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WorkSeq__SuccessorAssignment_3_in_rule__WorkSeq__Group__3__Impl1610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ressource__Group__0__Impl_in_rule__Ressource__Group__01649 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Ressource__Group__1_in_rule__Ressource__Group__01652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Ressource__Group__0__Impl1680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ressource__Group__1__Impl_in_rule__Ressource__Group__11711 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Ressource__Group__2_in_rule__Ressource__Group__11714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ressource__NameAssignment_1_in_rule__Ressource__Group__1__Impl1741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ressource__Group__2__Impl_in_rule__Ressource__Group__21771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ressource__QuantityAssignment_2_in_rule__Ressource__Group__2__Impl1798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__0__Impl_in_rule__RessourceUsage__Group__01834 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__1_in_rule__RessourceUsage__Group__01837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__RessourceUsage__Group__0__Impl1865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__1__Impl_in_rule__RessourceUsage__Group__11896 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__2_in_rule__RessourceUsage__Group__11899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__WorkDefAssignment_1_in_rule__RessourceUsage__Group__1__Impl1926 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__2__Impl_in_rule__RessourceUsage__Group__21956 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__3_in_rule__RessourceUsage__Group__21959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__RessourceAssignment_2_in_rule__RessourceUsage__Group__2__Impl1986 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__Group__3__Impl_in_rule__RessourceUsage__Group__32016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RessourceUsage__WeightAssignment_3_in_rule__RessourceUsage__Group__3__Impl2043 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Process__NameAssignment_12086 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProcessElement_in_rule__Process__ProcessElementsAssignment_32117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__WorkDef__NameAssignment_12148 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__WorkDef__Min_timeAssignment_32179 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__WorkDef__Max_timeAssignment_52210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__WorkSeq__PredecessorAssignment_12245 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkSequenceType_in_rule__WorkSeq__LinkTypeAssignment_22280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__WorkSeq__SuccessorAssignment_32315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Ressource__NameAssignment_12351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Ressource__QuantityAssignment_22382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RessourceUsage__WorkDefAssignment_12417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RessourceUsage__RessourceAssignment_22456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__RessourceUsage__WeightAssignment_32491 = new BitSet(new long[]{0x0000000000000002L});

}