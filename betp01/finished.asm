<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="finished"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="/tmp/"/>
		<constant value="1"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="PetriNet"/>
		<constant value="petrinet"/>
		<constant value="J.allInstances():J"/>
		<constant value="2"/>
		<constant value="J.finishedop():J"/>
		<constant value="name"/>
		<constant value="J.+(J):J"/>
		<constant value=".net"/>
		<constant value="J.writeTo(J):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="3:27-3:34"/>
		<constant value="4:3-4:20"/>
		<constant value="4:3-4:35"/>
		<constant value="5:19-5:21"/>
		<constant value="5:19-5:34"/>
		<constant value="5:43-5:53"/>
		<constant value="5:56-5:58"/>
		<constant value="5:56-5:63"/>
		<constant value="5:43-5:63"/>
		<constant value="5:66-5:72"/>
		<constant value="5:43-5:72"/>
		<constant value="5:19-5:73"/>
		<constant value="4:3-5:74"/>
		<constant value="3:2-5:74"/>
		<constant value="pn"/>
		<constant value="repertoire"/>
		<constant value="self"/>
		<constant value="concatenateStrings"/>
		<constant value="J"/>
		<constant value="3"/>
		<constant value=""/>
		<constant value="4"/>
		<constant value="5"/>
		<constant value="12:36-12:38"/>
		<constant value="12:2-12:9"/>
		<constant value="12:41-12:44"/>
		<constant value="12:47-12:53"/>
		<constant value="12:41-12:53"/>
		<constant value="12:56-12:57"/>
		<constant value="12:41-12:57"/>
		<constant value="12:60-12:65"/>
		<constant value="12:41-12:65"/>
		<constant value="12:2-12:66"/>
		<constant value="s"/>
		<constant value="acc"/>
		<constant value="strings"/>
		<constant value="before"/>
		<constant value="after"/>
		<constant value="isFinishedPlace"/>
		<constant value="Mpetrinet!Place;"/>
		<constant value="test"/>
		<constant value="17:2-17:8"/>
		<constant value="inv"/>
		<constant value="invariant&#10;"/>
		<constant value="22:2-22:15"/>
		<constant value="finishedop"/>
		<constant value="Mpetrinet!PetriNet;"/>
		<constant value="op finished ="/>
		<constant value="0"/>
		<constant value="nodes"/>
		<constant value="J.isFinishedPlace():J"/>
		<constant value="/\"/>
		<constant value="J.concatenateStrings(JJJ):J"/>
		<constant value="&#10;"/>
		<constant value="constant"/>
		<constant value="28:2-28:17"/>
		<constant value="29:40-29:44"/>
		<constant value="29:40-29:50"/>
		<constant value="29:64-29:65"/>
		<constant value="29:64-29:83"/>
		<constant value="29:40-29:84"/>
		<constant value="31:4-31:14"/>
		<constant value="31:34-31:46"/>
		<constant value="31:48-31:50"/>
		<constant value="31:52-31:57"/>
		<constant value="31:4-31:58"/>
		<constant value="29:3-31:58"/>
		<constant value="28:2-32:3"/>
		<constant value="32:5-32:9"/>
		<constant value="28:2-32:9"/>
		<constant value="32:12-32:22"/>
		<constant value="28:2-32:22"/>
		<constant value="p"/>
		<constant value="nodesStrings"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<push arg="3"/>
			<store arg="4"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<push arg="7"/>
			<push arg="8"/>
			<findme/>
			<call arg="9"/>
			<iterate/>
			<store arg="10"/>
			<load arg="10"/>
			<call arg="11"/>
			<load arg="4"/>
			<load arg="10"/>
			<get arg="12"/>
			<call arg="13"/>
			<push arg="14"/>
			<call arg="13"/>
			<call arg="15"/>
			<call arg="16"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="17" begin="0" end="0"/>
			<lne id="18" begin="5" end="7"/>
			<lne id="19" begin="5" end="8"/>
			<lne id="20" begin="11" end="11"/>
			<lne id="21" begin="11" end="12"/>
			<lne id="22" begin="13" end="13"/>
			<lne id="23" begin="14" end="14"/>
			<lne id="24" begin="14" end="15"/>
			<lne id="25" begin="13" end="16"/>
			<lne id="26" begin="17" end="17"/>
			<lne id="27" begin="13" end="18"/>
			<lne id="28" begin="11" end="19"/>
			<lne id="29" begin="2" end="21"/>
			<lne id="30" begin="0" end="21"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="31" begin="10" end="20"/>
			<lve slot="1" name="32" begin="1" end="21"/>
			<lve slot="0" name="33" begin="0" end="21"/>
		</localvariabletable>
	</operation>
	<operation name="34">
		<context type="2"/>
		<parameters>
			<parameter name="4" type="35"/>
			<parameter name="10" type="35"/>
			<parameter name="36" type="35"/>
		</parameters>
		<code>
			<push arg="37"/>
			<store arg="38"/>
			<load arg="4"/>
			<iterate/>
			<store arg="39"/>
			<load arg="38"/>
			<load arg="10"/>
			<call arg="13"/>
			<load arg="39"/>
			<call arg="13"/>
			<load arg="36"/>
			<call arg="13"/>
			<store arg="38"/>
			<enditerate/>
			<load arg="38"/>
		</code>
		<linenumbertable>
			<lne id="40" begin="0" end="0"/>
			<lne id="41" begin="2" end="2"/>
			<lne id="42" begin="5" end="5"/>
			<lne id="43" begin="6" end="6"/>
			<lne id="44" begin="5" end="7"/>
			<lne id="45" begin="8" end="8"/>
			<lne id="46" begin="5" end="9"/>
			<lne id="47" begin="10" end="10"/>
			<lne id="48" begin="5" end="11"/>
			<lne id="49" begin="0" end="14"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="50" begin="4" end="12"/>
			<lve slot="4" name="51" begin="1" end="14"/>
			<lve slot="0" name="33" begin="0" end="14"/>
			<lve slot="1" name="52" begin="0" end="14"/>
			<lve slot="2" name="53" begin="0" end="14"/>
			<lve slot="3" name="54" begin="0" end="14"/>
		</localvariabletable>
	</operation>
	<operation name="55">
		<context type="56"/>
		<parameters>
		</parameters>
		<code>
			<push arg="57"/>
		</code>
		<linenumbertable>
			<lne id="58" begin="0" end="0"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="0"/>
		</localvariabletable>
	</operation>
	<operation name="59">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<push arg="60"/>
		</code>
		<linenumbertable>
			<lne id="61" begin="0" end="0"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="0"/>
		</localvariabletable>
	</operation>
	<operation name="62">
		<context type="63"/>
		<parameters>
		</parameters>
		<code>
			<push arg="64"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<load arg="65"/>
			<get arg="66"/>
			<iterate/>
			<store arg="4"/>
			<load arg="4"/>
			<call arg="67"/>
			<call arg="16"/>
			<enditerate/>
			<store arg="4"/>
			<getasm/>
			<load arg="4"/>
			<push arg="37"/>
			<push arg="68"/>
			<call arg="69"/>
			<call arg="13"/>
			<push arg="70"/>
			<call arg="13"/>
			<push arg="71"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="72" begin="0" end="0"/>
			<lne id="73" begin="4" end="4"/>
			<lne id="74" begin="4" end="5"/>
			<lne id="75" begin="8" end="8"/>
			<lne id="76" begin="8" end="9"/>
			<lne id="77" begin="1" end="11"/>
			<lne id="78" begin="13" end="13"/>
			<lne id="79" begin="14" end="14"/>
			<lne id="80" begin="15" end="15"/>
			<lne id="81" begin="16" end="16"/>
			<lne id="82" begin="13" end="17"/>
			<lne id="83" begin="1" end="17"/>
			<lne id="84" begin="0" end="18"/>
			<lne id="85" begin="19" end="19"/>
			<lne id="86" begin="0" end="20"/>
			<lne id="87" begin="21" end="21"/>
			<lne id="88" begin="0" end="22"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="89" begin="7" end="10"/>
			<lve slot="1" name="90" begin="12" end="17"/>
			<lve slot="0" name="33" begin="0" end="22"/>
		</localvariabletable>
	</operation>
</asm>
