/*
* generated by Xtext
*/
package fr.xtext.improvedsimplepdl.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class ImprovedSimplePdlAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.tokens");
	}
}
