package betp02.serializer;

import betp02.services.PetrinetGrammarAccess;
import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public class PetrinetSyntacticSequencer extends AbstractSyntacticSequencer {

	protected PetrinetGrammarAccess grammarAccess;
	protected AbstractElementAlias match_Place_LeftSquareBracketKeyword_2_q;
	protected AbstractElementAlias match_Transition_LeftSquareBracketKeyword_2_q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (PetrinetGrammarAccess) access;
		match_Place_LeftSquareBracketKeyword_2_q = new TokenAlias(false, true, grammarAccess.getPlaceAccess().getLeftSquareBracketKeyword_2());
		match_Transition_LeftSquareBracketKeyword_2_q = new TokenAlias(false, true, grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_2());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_Place_LeftSquareBracketKeyword_2_q.equals(syntax))
				emit_Place_LeftSquareBracketKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Transition_LeftSquareBracketKeyword_2_q.equals(syntax))
				emit_Transition_LeftSquareBracketKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     '['?
	 */
	protected void emit_Place_LeftSquareBracketKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '['?
	 */
	protected void emit_Transition_LeftSquareBracketKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
