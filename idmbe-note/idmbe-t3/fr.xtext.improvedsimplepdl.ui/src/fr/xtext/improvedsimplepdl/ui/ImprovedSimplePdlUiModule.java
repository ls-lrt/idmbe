/*
 * generated by Xtext
 */
package fr.xtext.improvedsimplepdl.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the IDE.
 */
public class ImprovedSimplePdlUiModule extends fr.xtext.improvedsimplepdl.ui.AbstractImprovedSimplePdlUiModule {
	public ImprovedSimplePdlUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
