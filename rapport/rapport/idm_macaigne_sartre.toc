\select@language {french}
\contentsline {section}{\numberline {1}Prise en compte du temps et des ressources}{2}
\contentsline {section}{\numberline {2}Enrichissement de l'\IeC {\'e}diteur graphique}{2}
\contentsline {section}{\numberline {3}Syntaxe textuelle et Xtext}{3}
\contentsline {subsection}{\numberline {3.1}Enrichissement de la syntaxe}{3}
\contentsline {subsection}{\numberline {3.2}\IeC {\'E}diteur textuel avec xtext}{4}
\contentsline {section}{\numberline {4}Nouvelles contraintes OCL}{6}
\contentsline {section}{\numberline {5}Transformation SimplePdl vers PetriNet}{7}
\contentsline {subsection}{\numberline {5.1}Traduction des \IeC {\'e}l\IeC {\'e}ments}{7}
\contentsline {subsection}{\numberline {5.2}Ajout des observateurs}{8}
\contentsline {section}{\numberline {6}Simulation d'un r\IeC {\'e}seau auto-g\IeC {\'e}n\IeC {\'e}r\IeC {\'e}}{10}
\contentsline {subsection}{\numberline {6.1}Exemple d'ex\IeC {\'e}cution}{10}
\contentsline {section}{\numberline {7}PetriNet vers Tina}{12}
\contentsline {section}{\numberline {8}Nouvelles contraintes OCL sur PetriNet}{12}
\contentsline {section}{\numberline {9}Terminaison d'un processus}{13}
\contentsline {section}{\numberline {10}Validation de la transformation}{14}
\contentsline {subsection}{\numberline {10.1}Pr\IeC {\'e}servation des invariants}{14}
\contentsline {subsection}{\numberline {10.2}R\IeC {\'e}sultats}{14}
\contentsline {section}{\numberline {A}Gestion plus fine des ressources}{17}
\contentsline {subsection}{\numberline {A.1}Solution propos\IeC {\'e}e}{17}
\contentsline {subsection}{\numberline {A.2}Application sur un exemple}{18}
\contentsline {section}{\numberline {B}Ressources alternatives}{20}
\contentsline {subsection}{\numberline {B.1}T\IeC {\^a}che 1}{20}
\contentsline {subsection}{\numberline {B.2}T\IeC {\^a}che 5}{20}
