package fr.xtext.improvedsimplepdl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.xtext.improvedsimplepdl.services.ImprovedSimplePdlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalImprovedSimplePdlParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'process'", "'{'", "'}'", "'wd'", "'['", "','", "']'", "'ws'", "'res'", "'use'", "'S2S'", "'F2S'", "'S2F'", "'F2F'"
    };
    public static final int RULE_ID=4;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalImprovedSimplePdlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalImprovedSimplePdlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalImprovedSimplePdlParser.tokenNames; }
    public String getGrammarFileName() { return "../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g"; }



     	private ImprovedSimplePdlGrammarAccess grammarAccess;
     	
        public InternalImprovedSimplePdlParser(TokenStream input, ImprovedSimplePdlGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Process";	
       	}
       	
       	@Override
       	protected ImprovedSimplePdlGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleProcess"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:68:1: entryRuleProcess returns [EObject current=null] : iv_ruleProcess= ruleProcess EOF ;
    public final EObject entryRuleProcess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcess = null;


        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:69:2: (iv_ruleProcess= ruleProcess EOF )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:70:2: iv_ruleProcess= ruleProcess EOF
            {
             newCompositeNode(grammarAccess.getProcessRule()); 
            pushFollow(FOLLOW_ruleProcess_in_entryRuleProcess75);
            iv_ruleProcess=ruleProcess();

            state._fsp--;

             current =iv_ruleProcess; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProcess85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcess"


    // $ANTLR start "ruleProcess"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:77:1: ruleProcess returns [EObject current=null] : (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_processElements_3_0= ruleProcessElement ) )* otherlv_4= '}' ) ;
    public final EObject ruleProcess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_processElements_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:80:28: ( (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_processElements_3_0= ruleProcessElement ) )* otherlv_4= '}' ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:81:1: (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_processElements_3_0= ruleProcessElement ) )* otherlv_4= '}' )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:81:1: (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_processElements_3_0= ruleProcessElement ) )* otherlv_4= '}' )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:81:3: otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_processElements_3_0= ruleProcessElement ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleProcess122); 

                	newLeafNode(otherlv_0, grammarAccess.getProcessAccess().getProcessKeyword_0());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:85:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:86:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:86:1: (lv_name_1_0= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:87:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProcess139); 

            			newLeafNode(lv_name_1_0, grammarAccess.getProcessAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getProcessRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleProcess156); 

                	newLeafNode(otherlv_2, grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_2());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:107:1: ( (lv_processElements_3_0= ruleProcessElement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14||(LA1_0>=18 && LA1_0<=20)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:108:1: (lv_processElements_3_0= ruleProcessElement )
            	    {
            	    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:108:1: (lv_processElements_3_0= ruleProcessElement )
            	    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:109:3: lv_processElements_3_0= ruleProcessElement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getProcessAccess().getProcessElementsProcessElementParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleProcessElement_in_ruleProcess177);
            	    lv_processElements_3_0=ruleProcessElement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getProcessRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"processElements",
            	            		lv_processElements_3_0, 
            	            		"ProcessElement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleProcess190); 

                	newLeafNode(otherlv_4, grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcess"


    // $ANTLR start "entryRuleProcessElement"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:137:1: entryRuleProcessElement returns [EObject current=null] : iv_ruleProcessElement= ruleProcessElement EOF ;
    public final EObject entryRuleProcessElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcessElement = null;


        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:138:2: (iv_ruleProcessElement= ruleProcessElement EOF )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:139:2: iv_ruleProcessElement= ruleProcessElement EOF
            {
             newCompositeNode(grammarAccess.getProcessElementRule()); 
            pushFollow(FOLLOW_ruleProcessElement_in_entryRuleProcessElement226);
            iv_ruleProcessElement=ruleProcessElement();

            state._fsp--;

             current =iv_ruleProcessElement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProcessElement236); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcessElement"


    // $ANTLR start "ruleProcessElement"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:146:1: ruleProcessElement returns [EObject current=null] : (this_WorkDef_0= ruleWorkDef | this_WorkSeq_1= ruleWorkSeq | this_Ressource_2= ruleRessource | this_RessourceUsage_3= ruleRessourceUsage ) ;
    public final EObject ruleProcessElement() throws RecognitionException {
        EObject current = null;

        EObject this_WorkDef_0 = null;

        EObject this_WorkSeq_1 = null;

        EObject this_Ressource_2 = null;

        EObject this_RessourceUsage_3 = null;


         enterRule(); 
            
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:149:28: ( (this_WorkDef_0= ruleWorkDef | this_WorkSeq_1= ruleWorkSeq | this_Ressource_2= ruleRessource | this_RessourceUsage_3= ruleRessourceUsage ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:150:1: (this_WorkDef_0= ruleWorkDef | this_WorkSeq_1= ruleWorkSeq | this_Ressource_2= ruleRessource | this_RessourceUsage_3= ruleRessourceUsage )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:150:1: (this_WorkDef_0= ruleWorkDef | this_WorkSeq_1= ruleWorkSeq | this_Ressource_2= ruleRessource | this_RessourceUsage_3= ruleRessourceUsage )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 18:
                {
                alt2=2;
                }
                break;
            case 19:
                {
                alt2=3;
                }
                break;
            case 20:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:151:5: this_WorkDef_0= ruleWorkDef
                    {
                     
                            newCompositeNode(grammarAccess.getProcessElementAccess().getWorkDefParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleWorkDef_in_ruleProcessElement283);
                    this_WorkDef_0=ruleWorkDef();

                    state._fsp--;

                     
                            current = this_WorkDef_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:161:5: this_WorkSeq_1= ruleWorkSeq
                    {
                     
                            newCompositeNode(grammarAccess.getProcessElementAccess().getWorkSeqParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleWorkSeq_in_ruleProcessElement310);
                    this_WorkSeq_1=ruleWorkSeq();

                    state._fsp--;

                     
                            current = this_WorkSeq_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:171:5: this_Ressource_2= ruleRessource
                    {
                     
                            newCompositeNode(grammarAccess.getProcessElementAccess().getRessourceParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleRessource_in_ruleProcessElement337);
                    this_Ressource_2=ruleRessource();

                    state._fsp--;

                     
                            current = this_Ressource_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:181:5: this_RessourceUsage_3= ruleRessourceUsage
                    {
                     
                            newCompositeNode(grammarAccess.getProcessElementAccess().getRessourceUsageParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleRessourceUsage_in_ruleProcessElement364);
                    this_RessourceUsage_3=ruleRessourceUsage();

                    state._fsp--;

                     
                            current = this_RessourceUsage_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcessElement"


    // $ANTLR start "entryRuleWorkDef"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:197:1: entryRuleWorkDef returns [EObject current=null] : iv_ruleWorkDef= ruleWorkDef EOF ;
    public final EObject entryRuleWorkDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkDef = null;


        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:198:2: (iv_ruleWorkDef= ruleWorkDef EOF )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:199:2: iv_ruleWorkDef= ruleWorkDef EOF
            {
             newCompositeNode(grammarAccess.getWorkDefRule()); 
            pushFollow(FOLLOW_ruleWorkDef_in_entryRuleWorkDef399);
            iv_ruleWorkDef=ruleWorkDef();

            state._fsp--;

             current =iv_ruleWorkDef; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWorkDef409); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkDef"


    // $ANTLR start "ruleWorkDef"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:206:1: ruleWorkDef returns [EObject current=null] : (otherlv_0= 'wd' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']' ) ;
    public final EObject ruleWorkDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_min_time_3_0=null;
        Token otherlv_4=null;
        Token lv_max_time_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:209:28: ( (otherlv_0= 'wd' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']' ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:210:1: (otherlv_0= 'wd' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']' )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:210:1: (otherlv_0= 'wd' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']' )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:210:3: otherlv_0= 'wd' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleWorkDef446); 

                	newLeafNode(otherlv_0, grammarAccess.getWorkDefAccess().getWdKeyword_0());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:214:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:215:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:215:1: (lv_name_1_0= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:216:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleWorkDef463); 

            			newLeafNode(lv_name_1_0, grammarAccess.getWorkDefAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getWorkDefRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleWorkDef480); 

                	newLeafNode(otherlv_2, grammarAccess.getWorkDefAccess().getLeftSquareBracketKeyword_2());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:236:1: ( (lv_min_time_3_0= RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:237:1: (lv_min_time_3_0= RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:237:1: (lv_min_time_3_0= RULE_INT )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:238:3: lv_min_time_3_0= RULE_INT
            {
            lv_min_time_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleWorkDef497); 

            			newLeafNode(lv_min_time_3_0, grammarAccess.getWorkDefAccess().getMin_timeINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getWorkDefRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"min_time",
                    		lv_min_time_3_0, 
                    		"INT");
            	    

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleWorkDef514); 

                	newLeafNode(otherlv_4, grammarAccess.getWorkDefAccess().getCommaKeyword_4());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:258:1: ( (lv_max_time_5_0= RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:259:1: (lv_max_time_5_0= RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:259:1: (lv_max_time_5_0= RULE_INT )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:260:3: lv_max_time_5_0= RULE_INT
            {
            lv_max_time_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleWorkDef531); 

            			newLeafNode(lv_max_time_5_0, grammarAccess.getWorkDefAccess().getMax_timeINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getWorkDefRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"max_time",
                    		lv_max_time_5_0, 
                    		"INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleWorkDef548); 

                	newLeafNode(otherlv_6, grammarAccess.getWorkDefAccess().getRightSquareBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkDef"


    // $ANTLR start "entryRuleWorkSeq"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:288:1: entryRuleWorkSeq returns [EObject current=null] : iv_ruleWorkSeq= ruleWorkSeq EOF ;
    public final EObject entryRuleWorkSeq() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkSeq = null;


        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:289:2: (iv_ruleWorkSeq= ruleWorkSeq EOF )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:290:2: iv_ruleWorkSeq= ruleWorkSeq EOF
            {
             newCompositeNode(grammarAccess.getWorkSeqRule()); 
            pushFollow(FOLLOW_ruleWorkSeq_in_entryRuleWorkSeq584);
            iv_ruleWorkSeq=ruleWorkSeq();

            state._fsp--;

             current =iv_ruleWorkSeq; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWorkSeq594); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkSeq"


    // $ANTLR start "ruleWorkSeq"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:297:1: ruleWorkSeq returns [EObject current=null] : (otherlv_0= 'ws' ( (otherlv_1= RULE_ID ) ) ( (lv_linkType_2_0= ruleWorkSequenceType ) ) ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleWorkSeq() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_linkType_2_0 = null;


         enterRule(); 
            
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:300:28: ( (otherlv_0= 'ws' ( (otherlv_1= RULE_ID ) ) ( (lv_linkType_2_0= ruleWorkSequenceType ) ) ( (otherlv_3= RULE_ID ) ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:301:1: (otherlv_0= 'ws' ( (otherlv_1= RULE_ID ) ) ( (lv_linkType_2_0= ruleWorkSequenceType ) ) ( (otherlv_3= RULE_ID ) ) )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:301:1: (otherlv_0= 'ws' ( (otherlv_1= RULE_ID ) ) ( (lv_linkType_2_0= ruleWorkSequenceType ) ) ( (otherlv_3= RULE_ID ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:301:3: otherlv_0= 'ws' ( (otherlv_1= RULE_ID ) ) ( (lv_linkType_2_0= ruleWorkSequenceType ) ) ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleWorkSeq631); 

                	newLeafNode(otherlv_0, grammarAccess.getWorkSeqAccess().getWsKeyword_0());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:305:1: ( (otherlv_1= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:306:1: (otherlv_1= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:306:1: (otherlv_1= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:307:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getWorkSeqRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleWorkSeq651); 

            		newLeafNode(otherlv_1, grammarAccess.getWorkSeqAccess().getPredecessorWorkDefinitionCrossReference_1_0()); 
            	

            }


            }

            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:318:2: ( (lv_linkType_2_0= ruleWorkSequenceType ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:319:1: (lv_linkType_2_0= ruleWorkSequenceType )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:319:1: (lv_linkType_2_0= ruleWorkSequenceType )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:320:3: lv_linkType_2_0= ruleWorkSequenceType
            {
             
            	        newCompositeNode(grammarAccess.getWorkSeqAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleWorkSequenceType_in_ruleWorkSeq672);
            lv_linkType_2_0=ruleWorkSequenceType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWorkSeqRule());
            	        }
                   		set(
                   			current, 
                   			"linkType",
                    		lv_linkType_2_0, 
                    		"WorkSequenceType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:336:2: ( (otherlv_3= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:337:1: (otherlv_3= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:337:1: (otherlv_3= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:338:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getWorkSeqRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleWorkSeq692); 

            		newLeafNode(otherlv_3, grammarAccess.getWorkSeqAccess().getSuccessorWorkDefinitionCrossReference_3_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkSeq"


    // $ANTLR start "entryRuleRessource"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:359:1: entryRuleRessource returns [EObject current=null] : iv_ruleRessource= ruleRessource EOF ;
    public final EObject entryRuleRessource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRessource = null;


        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:360:2: (iv_ruleRessource= ruleRessource EOF )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:361:2: iv_ruleRessource= ruleRessource EOF
            {
             newCompositeNode(grammarAccess.getRessourceRule()); 
            pushFollow(FOLLOW_ruleRessource_in_entryRuleRessource730);
            iv_ruleRessource=ruleRessource();

            state._fsp--;

             current =iv_ruleRessource; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRessource740); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRessource"


    // $ANTLR start "ruleRessource"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:368:1: ruleRessource returns [EObject current=null] : (otherlv_0= 'res' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= RULE_INT ) ) ) ;
    public final EObject ruleRessource() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_quantity_2_0=null;

         enterRule(); 
            
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:371:28: ( (otherlv_0= 'res' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= RULE_INT ) ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:372:1: (otherlv_0= 'res' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= RULE_INT ) ) )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:372:1: (otherlv_0= 'res' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= RULE_INT ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:372:3: otherlv_0= 'res' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleRessource777); 

                	newLeafNode(otherlv_0, grammarAccess.getRessourceAccess().getResKeyword_0());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:376:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:377:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:377:1: (lv_name_1_0= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:378:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRessource794); 

            			newLeafNode(lv_name_1_0, grammarAccess.getRessourceAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRessourceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:394:2: ( (lv_quantity_2_0= RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:395:1: (lv_quantity_2_0= RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:395:1: (lv_quantity_2_0= RULE_INT )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:396:3: lv_quantity_2_0= RULE_INT
            {
            lv_quantity_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleRessource816); 

            			newLeafNode(lv_quantity_2_0, grammarAccess.getRessourceAccess().getQuantityINTTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRessourceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"quantity",
                    		lv_quantity_2_0, 
                    		"INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRessource"


    // $ANTLR start "entryRuleRessourceUsage"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:420:1: entryRuleRessourceUsage returns [EObject current=null] : iv_ruleRessourceUsage= ruleRessourceUsage EOF ;
    public final EObject entryRuleRessourceUsage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRessourceUsage = null;


        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:421:2: (iv_ruleRessourceUsage= ruleRessourceUsage EOF )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:422:2: iv_ruleRessourceUsage= ruleRessourceUsage EOF
            {
             newCompositeNode(grammarAccess.getRessourceUsageRule()); 
            pushFollow(FOLLOW_ruleRessourceUsage_in_entryRuleRessourceUsage857);
            iv_ruleRessourceUsage=ruleRessourceUsage();

            state._fsp--;

             current =iv_ruleRessourceUsage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRessourceUsage867); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRessourceUsage"


    // $ANTLR start "ruleRessourceUsage"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:429:1: ruleRessourceUsage returns [EObject current=null] : (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ( (lv_weight_3_0= RULE_INT ) ) ) ;
    public final EObject ruleRessourceUsage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_weight_3_0=null;

         enterRule(); 
            
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:432:28: ( (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ( (lv_weight_3_0= RULE_INT ) ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:433:1: (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ( (lv_weight_3_0= RULE_INT ) ) )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:433:1: (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ( (lv_weight_3_0= RULE_INT ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:433:3: otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ( (lv_weight_3_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleRessourceUsage904); 

                	newLeafNode(otherlv_0, grammarAccess.getRessourceUsageAccess().getUseKeyword_0());
                
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:437:1: ( (otherlv_1= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:438:1: (otherlv_1= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:438:1: (otherlv_1= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:439:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRessourceUsageRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRessourceUsage924); 

            		newLeafNode(otherlv_1, grammarAccess.getRessourceUsageAccess().getWorkDefWorkDefinitionCrossReference_1_0()); 
            	

            }


            }

            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:450:2: ( (otherlv_2= RULE_ID ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:451:1: (otherlv_2= RULE_ID )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:451:1: (otherlv_2= RULE_ID )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:452:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRessourceUsageRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRessourceUsage944); 

            		newLeafNode(otherlv_2, grammarAccess.getRessourceUsageAccess().getRessourceRessourceCrossReference_2_0()); 
            	

            }


            }

            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:463:2: ( (lv_weight_3_0= RULE_INT ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:464:1: (lv_weight_3_0= RULE_INT )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:464:1: (lv_weight_3_0= RULE_INT )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:465:3: lv_weight_3_0= RULE_INT
            {
            lv_weight_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleRessourceUsage961); 

            			newLeafNode(lv_weight_3_0, grammarAccess.getRessourceUsageAccess().getWeightINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRessourceUsageRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"weight",
                    		lv_weight_3_0, 
                    		"INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRessourceUsage"


    // $ANTLR start "ruleWorkSequenceType"
    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:489:1: ruleWorkSequenceType returns [Enumerator current=null] : ( (enumLiteral_0= 'S2S' ) | (enumLiteral_1= 'F2S' ) | (enumLiteral_2= 'S2F' ) | (enumLiteral_3= 'F2F' ) ) ;
    public final Enumerator ruleWorkSequenceType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:491:28: ( ( (enumLiteral_0= 'S2S' ) | (enumLiteral_1= 'F2S' ) | (enumLiteral_2= 'S2F' ) | (enumLiteral_3= 'F2F' ) ) )
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:492:1: ( (enumLiteral_0= 'S2S' ) | (enumLiteral_1= 'F2S' ) | (enumLiteral_2= 'S2F' ) | (enumLiteral_3= 'F2F' ) )
            {
            // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:492:1: ( (enumLiteral_0= 'S2S' ) | (enumLiteral_1= 'F2S' ) | (enumLiteral_2= 'S2F' ) | (enumLiteral_3= 'F2F' ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt3=1;
                }
                break;
            case 22:
                {
                alt3=2;
                }
                break;
            case 23:
                {
                alt3=3;
                }
                break;
            case 24:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:492:2: (enumLiteral_0= 'S2S' )
                    {
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:492:2: (enumLiteral_0= 'S2S' )
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:492:4: enumLiteral_0= 'S2S'
                    {
                    enumLiteral_0=(Token)match(input,21,FOLLOW_21_in_ruleWorkSequenceType1016); 

                            current = grammarAccess.getWorkSequenceTypeAccess().getStartToStartEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getWorkSequenceTypeAccess().getStartToStartEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:498:6: (enumLiteral_1= 'F2S' )
                    {
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:498:6: (enumLiteral_1= 'F2S' )
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:498:8: enumLiteral_1= 'F2S'
                    {
                    enumLiteral_1=(Token)match(input,22,FOLLOW_22_in_ruleWorkSequenceType1033); 

                            current = grammarAccess.getWorkSequenceTypeAccess().getFinishToStartEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getWorkSequenceTypeAccess().getFinishToStartEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:504:6: (enumLiteral_2= 'S2F' )
                    {
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:504:6: (enumLiteral_2= 'S2F' )
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:504:8: enumLiteral_2= 'S2F'
                    {
                    enumLiteral_2=(Token)match(input,23,FOLLOW_23_in_ruleWorkSequenceType1050); 

                            current = grammarAccess.getWorkSequenceTypeAccess().getStartToFinishEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getWorkSequenceTypeAccess().getStartToFinishEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:510:6: (enumLiteral_3= 'F2F' )
                    {
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:510:6: (enumLiteral_3= 'F2F' )
                    // ../fr.xtext.improvedsimplepdl/src-gen/fr/xtext/improvedsimplepdl/parser/antlr/internal/InternalImprovedSimplePdl.g:510:8: enumLiteral_3= 'F2F'
                    {
                    enumLiteral_3=(Token)match(input,24,FOLLOW_24_in_ruleWorkSequenceType1067); 

                            current = grammarAccess.getWorkSequenceTypeAccess().getFinishToFinishEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getWorkSequenceTypeAccess().getFinishToFinishEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkSequenceType"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleProcess_in_entryRuleProcess75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProcess85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleProcess122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProcess139 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProcess156 = new BitSet(new long[]{0x00000000001C6000L});
    public static final BitSet FOLLOW_ruleProcessElement_in_ruleProcess177 = new BitSet(new long[]{0x00000000001C6000L});
    public static final BitSet FOLLOW_13_in_ruleProcess190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProcessElement_in_entryRuleProcessElement226 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProcessElement236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkDef_in_ruleProcessElement283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkSeq_in_ruleProcessElement310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessource_in_ruleProcessElement337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessourceUsage_in_ruleProcessElement364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkDef_in_entryRuleWorkDef399 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWorkDef409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleWorkDef446 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleWorkDef463 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleWorkDef480 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleWorkDef497 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleWorkDef514 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleWorkDef531 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleWorkDef548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWorkSeq_in_entryRuleWorkSeq584 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWorkSeq594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleWorkSeq631 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleWorkSeq651 = new BitSet(new long[]{0x0000000001E00000L});
    public static final BitSet FOLLOW_ruleWorkSequenceType_in_ruleWorkSeq672 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleWorkSeq692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessource_in_entryRuleRessource730 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRessource740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleRessource777 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRessource794 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleRessource816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRessourceUsage_in_entryRuleRessourceUsage857 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRessourceUsage867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRessourceUsage904 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRessourceUsage924 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRessourceUsage944 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleRessourceUsage961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleWorkSequenceType1016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleWorkSequenceType1033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleWorkSequenceType1050 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleWorkSequenceType1067 = new BitSet(new long[]{0x0000000000000002L});

}