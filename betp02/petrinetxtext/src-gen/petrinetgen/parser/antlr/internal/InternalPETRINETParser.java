package petrinetgen.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import petrinetgen.services.PETRINETGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPETRINETParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'pn'", "'{'", "'}'", "'p'", "'['", "']'", "'t'", "','", "']\\n'", "'read_arc'", "'normal'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalPETRINETParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPETRINETParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPETRINETParser.tokenNames; }
    public String getGrammarFileName() { return "../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g"; }



     	private PETRINETGrammarAccess grammarAccess;
     	
        public InternalPETRINETParser(TokenStream input, PETRINETGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Petrinet";	
       	}
       	
       	@Override
       	protected PETRINETGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulePetrinet"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:68:1: entryRulePetrinet returns [EObject current=null] : iv_rulePetrinet= rulePetrinet EOF ;
    public final EObject entryRulePetrinet() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePetrinet = null;


        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:69:2: (iv_rulePetrinet= rulePetrinet EOF )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:70:2: iv_rulePetrinet= rulePetrinet EOF
            {
             newCompositeNode(grammarAccess.getPetrinetRule()); 
            pushFollow(FOLLOW_rulePetrinet_in_entryRulePetrinet75);
            iv_rulePetrinet=rulePetrinet();

            state._fsp--;

             current =iv_rulePetrinet; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePetrinet85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePetrinet"


    // $ANTLR start "rulePetrinet"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:77:1: rulePetrinet returns [EObject current=null] : (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )? otherlv_4= '}' ) ;
    public final EObject rulePetrinet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_nodes_3_0 = null;


         enterRule(); 
            
        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:80:28: ( (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )? otherlv_4= '}' ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:81:1: (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )? otherlv_4= '}' )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:81:1: (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )? otherlv_4= '}' )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:81:3: otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )? otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_rulePetrinet122); 

                	newLeafNode(otherlv_0, grammarAccess.getPetrinetAccess().getPnKeyword_0());
                
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:85:1: ( (lv_name_1_0= RULE_ID ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:86:1: (lv_name_1_0= RULE_ID )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:86:1: (lv_name_1_0= RULE_ID )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:87:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePetrinet139); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPetrinetAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPetrinetRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_rulePetrinet156); 

                	newLeafNode(otherlv_2, grammarAccess.getPetrinetAccess().getLeftCurlyBracketKeyword_2());
                
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:107:1: ( (lv_nodes_3_0= rulePnNode ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14||LA1_0==17) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:108:1: (lv_nodes_3_0= rulePnNode )
                    {
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:108:1: (lv_nodes_3_0= rulePnNode )
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:109:3: lv_nodes_3_0= rulePnNode
                    {
                     
                    	        newCompositeNode(grammarAccess.getPetrinetAccess().getNodesPnNodeParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_rulePnNode_in_rulePetrinet177);
                    lv_nodes_3_0=rulePnNode();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPetrinetRule());
                    	        }
                           		add(
                           			current, 
                           			"nodes",
                            		lv_nodes_3_0, 
                            		"PnNode");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_rulePetrinet190); 

                	newLeafNode(otherlv_4, grammarAccess.getPetrinetAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePetrinet"


    // $ANTLR start "entryRulePnNode"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:137:1: entryRulePnNode returns [EObject current=null] : iv_rulePnNode= rulePnNode EOF ;
    public final EObject entryRulePnNode() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePnNode = null;


        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:138:2: (iv_rulePnNode= rulePnNode EOF )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:139:2: iv_rulePnNode= rulePnNode EOF
            {
             newCompositeNode(grammarAccess.getPnNodeRule()); 
            pushFollow(FOLLOW_rulePnNode_in_entryRulePnNode226);
            iv_rulePnNode=rulePnNode();

            state._fsp--;

             current =iv_rulePnNode; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePnNode236); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePnNode"


    // $ANTLR start "rulePnNode"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:146:1: rulePnNode returns [EObject current=null] : (this_Transition_0= ruleTransition | this_Place_1= rulePlace ) ;
    public final EObject rulePnNode() throws RecognitionException {
        EObject current = null;

        EObject this_Transition_0 = null;

        EObject this_Place_1 = null;


         enterRule(); 
            
        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:149:28: ( (this_Transition_0= ruleTransition | this_Place_1= rulePlace ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:150:1: (this_Transition_0= ruleTransition | this_Place_1= rulePlace )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:150:1: (this_Transition_0= ruleTransition | this_Place_1= rulePlace )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==17) ) {
                alt2=1;
            }
            else if ( (LA2_0==14) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:151:5: this_Transition_0= ruleTransition
                    {
                     
                            newCompositeNode(grammarAccess.getPnNodeAccess().getTransitionParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleTransition_in_rulePnNode283);
                    this_Transition_0=ruleTransition();

                    state._fsp--;

                     
                            current = this_Transition_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:161:5: this_Place_1= rulePlace
                    {
                     
                            newCompositeNode(grammarAccess.getPnNodeAccess().getPlaceParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_rulePlace_in_rulePnNode310);
                    this_Place_1=rulePlace();

                    state._fsp--;

                     
                            current = this_Place_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePnNode"


    // $ANTLR start "entryRulePlace"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:179:1: entryRulePlace returns [EObject current=null] : iv_rulePlace= rulePlace EOF ;
    public final EObject entryRulePlace() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlace = null;


        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:180:2: (iv_rulePlace= rulePlace EOF )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:181:2: iv_rulePlace= rulePlace EOF
            {
             newCompositeNode(grammarAccess.getPlaceRule()); 
            pushFollow(FOLLOW_rulePlace_in_entryRulePlace347);
            iv_rulePlace=rulePlace();

            state._fsp--;

             current =iv_rulePlace; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePlace357); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:188:1: rulePlace returns [EObject current=null] : (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' ) ;
    public final EObject rulePlace() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_marking_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:191:28: ( (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:192:1: (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:192:1: (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:192:3: otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_rulePlace394); 

                	newLeafNode(otherlv_0, grammarAccess.getPlaceAccess().getPKeyword_0());
                
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:196:1: ( (lv_name_1_0= RULE_ID ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:197:1: (lv_name_1_0= RULE_ID )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:197:1: (lv_name_1_0= RULE_ID )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:198:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePlace411); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPlaceAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPlaceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:214:2: (otherlv_2= '[' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:214:4: otherlv_2= '['
                    {
                    otherlv_2=(Token)match(input,15,FOLLOW_15_in_rulePlace429); 

                        	newLeafNode(otherlv_2, grammarAccess.getPlaceAccess().getLeftSquareBracketKeyword_2());
                        

                    }
                    break;

            }

            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:218:3: ( (lv_marking_3_0= RULE_INT ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:219:1: (lv_marking_3_0= RULE_INT )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:219:1: (lv_marking_3_0= RULE_INT )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:220:3: lv_marking_3_0= RULE_INT
            {
            lv_marking_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_rulePlace448); 

            			newLeafNode(lv_marking_3_0, grammarAccess.getPlaceAccess().getMarkingINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPlaceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"marking",
                    		lv_marking_3_0, 
                    		"INT");
            	    

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_rulePlace465); 

                	newLeafNode(otherlv_4, grammarAccess.getPlaceAccess().getRightSquareBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "entryRuleTransition"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:248:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:249:2: (iv_ruleTransition= ruleTransition EOF )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:250:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_ruleTransition_in_entryRuleTransition501);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTransition511); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:257:1: ruleTransition returns [EObject current=null] : (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_min_time_3_0=null;
        Token otherlv_4=null;
        Token lv_max_time_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:260:28: ( (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:261:1: (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:261:1: (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:261:3: otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleTransition548); 

                	newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getTKeyword_0());
                
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:265:1: ( (lv_name_1_0= RULE_ID ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:266:1: (lv_name_1_0= RULE_ID )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:266:1: (lv_name_1_0= RULE_ID )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:267:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTransition565); 

            			newLeafNode(lv_name_1_0, grammarAccess.getTransitionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:283:2: (otherlv_2= '[' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:283:4: otherlv_2= '['
                    {
                    otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleTransition583); 

                        	newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_2());
                        

                    }
                    break;

            }

            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:287:3: ( (lv_min_time_3_0= RULE_INT ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:288:1: (lv_min_time_3_0= RULE_INT )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:288:1: (lv_min_time_3_0= RULE_INT )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:289:3: lv_min_time_3_0= RULE_INT
            {
            lv_min_time_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleTransition602); 

            			newLeafNode(lv_min_time_3_0, grammarAccess.getTransitionAccess().getMin_timeINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"min_time",
                    		lv_min_time_3_0, 
                    		"INT");
            	    

            }


            }

            otherlv_4=(Token)match(input,18,FOLLOW_18_in_ruleTransition619); 

                	newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getCommaKeyword_4());
                
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:309:1: ( (lv_max_time_5_0= RULE_INT ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:310:1: (lv_max_time_5_0= RULE_INT )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:310:1: (lv_max_time_5_0= RULE_INT )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:311:3: lv_max_time_5_0= RULE_INT
            {
            lv_max_time_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleTransition636); 

            			newLeafNode(lv_max_time_5_0, grammarAccess.getTransitionAccess().getMax_timeINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"max_time",
                    		lv_max_time_5_0, 
                    		"INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,19,FOLLOW_19_in_ruleTransition653); 

                	newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getRightSquareBracketLineFeedLfKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "ruleArcKind"
    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:339:1: ruleArcKind returns [Enumerator current=null] : ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) ) ;
    public final Enumerator ruleArcKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:341:28: ( ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) ) )
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:342:1: ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) )
            {
            // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:342:1: ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            else if ( (LA5_0==21) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:342:2: (enumLiteral_0= 'read_arc' )
                    {
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:342:2: (enumLiteral_0= 'read_arc' )
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:342:4: enumLiteral_0= 'read_arc'
                    {
                    enumLiteral_0=(Token)match(input,20,FOLLOW_20_in_ruleArcKind703); 

                            current = grammarAccess.getArcKindAccess().getRead_arcEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getArcKindAccess().getRead_arcEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:348:6: (enumLiteral_1= 'normal' )
                    {
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:348:6: (enumLiteral_1= 'normal' )
                    // ../petrinet/src-gen/petrinetgen/parser/antlr/internal/InternalPETRINET.g:348:8: enumLiteral_1= 'normal'
                    {
                    enumLiteral_1=(Token)match(input,21,FOLLOW_21_in_ruleArcKind720); 

                            current = grammarAccess.getArcKindAccess().getNormalEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getArcKindAccess().getNormalEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArcKind"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePetrinet_in_entryRulePetrinet75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePetrinet85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rulePetrinet122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePetrinet139 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_rulePetrinet156 = new BitSet(new long[]{0x0000000000026000L});
    public static final BitSet FOLLOW_rulePnNode_in_rulePetrinet177 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_rulePetrinet190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnNode_in_entryRulePnNode226 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePnNode236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_rulePnNode283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlace_in_rulePnNode310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlace_in_entryRulePlace347 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlace357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rulePlace394 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePlace411 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_15_in_rulePlace429 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_rulePlace448 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_rulePlace465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition501 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTransition511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleTransition548 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTransition565 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_15_in_ruleTransition583 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleTransition602 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleTransition619 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleTransition636 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleTransition653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleArcKind703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleArcKind720 = new BitSet(new long[]{0x0000000000000002L});

}