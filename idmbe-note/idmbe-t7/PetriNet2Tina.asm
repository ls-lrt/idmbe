<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="PetriNet2Tina"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="/tmp/"/>
		<constant value="1"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="PetriNet"/>
		<constant value="petrinet"/>
		<constant value="J.allInstances():J"/>
		<constant value="2"/>
		<constant value="J.toTina():J"/>
		<constant value="name"/>
		<constant value="J.+(J):J"/>
		<constant value=".net"/>
		<constant value="J.writeTo(J):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="3:27-3:34"/>
		<constant value="4:3-4:20"/>
		<constant value="4:3-4:35"/>
		<constant value="5:19-5:21"/>
		<constant value="5:19-5:30"/>
		<constant value="5:39-5:49"/>
		<constant value="5:52-5:54"/>
		<constant value="5:52-5:59"/>
		<constant value="5:39-5:59"/>
		<constant value="5:62-5:68"/>
		<constant value="5:39-5:68"/>
		<constant value="5:19-5:69"/>
		<constant value="4:3-5:70"/>
		<constant value="3:2-5:70"/>
		<constant value="pn"/>
		<constant value="repertoire"/>
		<constant value="self"/>
		<constant value="concatenateStrings"/>
		<constant value="J"/>
		<constant value="3"/>
		<constant value=""/>
		<constant value="4"/>
		<constant value="5"/>
		<constant value="12:36-12:38"/>
		<constant value="12:2-12:9"/>
		<constant value="12:41-12:44"/>
		<constant value="12:47-12:53"/>
		<constant value="12:41-12:53"/>
		<constant value="12:56-12:57"/>
		<constant value="12:41-12:57"/>
		<constant value="12:60-12:65"/>
		<constant value="12:41-12:65"/>
		<constant value="12:2-12:66"/>
		<constant value="s"/>
		<constant value="acc"/>
		<constant value="strings"/>
		<constant value="before"/>
		<constant value="after"/>
		<constant value="translatetoinf"/>
		<constant value="0"/>
		<constant value="J.-(J):J"/>
		<constant value="J.=(J):J"/>
		<constant value="11"/>
		<constant value="J.toString():J"/>
		<constant value="]"/>
		<constant value="12"/>
		<constant value="w["/>
		<constant value="17:7-17:14"/>
		<constant value="17:18-17:19"/>
		<constant value="17:17-17:19"/>
		<constant value="17:7-17:19"/>
		<constant value="21:3-21:10"/>
		<constant value="21:3-21:21"/>
		<constant value="21:24-21:27"/>
		<constant value="21:3-21:27"/>
		<constant value="19:3-19:7"/>
		<constant value="17:3-22:8"/>
		<constant value="maxtime"/>
		<constant value="toTina"/>
		<constant value="Mpetrinet!PetriNet;"/>
		<constant value="net "/>
		<constant value="&#10;"/>
		<constant value="nodes"/>
		<constant value="J.concatenateStrings(JJJ):J"/>
		<constant value="28:2-28:8"/>
		<constant value="28:12-28:16"/>
		<constant value="28:12-28:21"/>
		<constant value="28:2-28:21"/>
		<constant value="28:24-28:28"/>
		<constant value="28:2-28:28"/>
		<constant value="29:40-29:44"/>
		<constant value="29:40-29:50"/>
		<constant value="29:64-29:65"/>
		<constant value="29:64-29:74"/>
		<constant value="29:40-29:75"/>
		<constant value="31:4-31:14"/>
		<constant value="31:34-31:46"/>
		<constant value="31:48-31:50"/>
		<constant value="31:52-31:56"/>
		<constant value="31:4-31:57"/>
		<constant value="29:3-31:57"/>
		<constant value="28:2-32:3"/>
		<constant value="p"/>
		<constant value="nodesStrings"/>
		<constant value="Mpetrinet!Place;"/>
		<constant value="pl "/>
		<constant value="("/>
		<constant value="marking"/>
		<constant value=")"/>
		<constant value="39:2-39:7"/>
		<constant value="39:10-39:14"/>
		<constant value="39:10-39:19"/>
		<constant value="39:2-39:19"/>
		<constant value="39:22-39:25"/>
		<constant value="39:2-39:25"/>
		<constant value="39:28-39:32"/>
		<constant value="39:28-39:40"/>
		<constant value="39:28-39:51"/>
		<constant value="39:2-39:51"/>
		<constant value="39:54-39:57"/>
		<constant value="39:2-39:57"/>
		<constant value="Mpetrinet!Transition;"/>
		<constant value="incomings"/>
		<constant value="J.asTina(J):J"/>
		<constant value="outgoings"/>
		<constant value="tr "/>
		<constant value=" "/>
		<constant value="["/>
		<constant value="min_time"/>
		<constant value=","/>
		<constant value="max_time"/>
		<constant value="J.translatetoinf(J):J"/>
		<constant value=" -&gt; "/>
		<constant value="47:5-47:9"/>
		<constant value="47:5-47:19"/>
		<constant value="47:35-47:38"/>
		<constant value="47:46-47:50"/>
		<constant value="47:35-47:51"/>
		<constant value="47:5-47:52"/>
		<constant value="49:5-49:9"/>
		<constant value="49:5-49:19"/>
		<constant value="49:35-49:38"/>
		<constant value="49:46-49:51"/>
		<constant value="49:35-49:52"/>
		<constant value="49:5-49:53"/>
		<constant value="51:3-51:8"/>
		<constant value="51:12-51:16"/>
		<constant value="51:12-51:21"/>
		<constant value="51:3-51:21"/>
		<constant value="51:24-51:27"/>
		<constant value="51:3-51:27"/>
		<constant value="52:4-52:7"/>
		<constant value="51:3-52:7"/>
		<constant value="52:10-52:14"/>
		<constant value="52:10-52:23"/>
		<constant value="51:3-52:23"/>
		<constant value="52:26-52:29"/>
		<constant value="51:3-52:29"/>
		<constant value="52:32-52:42"/>
		<constant value="52:58-52:62"/>
		<constant value="52:58-52:71"/>
		<constant value="52:32-52:72"/>
		<constant value="51:3-52:72"/>
		<constant value="53:5-53:15"/>
		<constant value="53:35-53:47"/>
		<constant value="53:49-53:52"/>
		<constant value="53:54-53:56"/>
		<constant value="53:5-53:57"/>
		<constant value="51:3-53:57"/>
		<constant value="54:5-54:11"/>
		<constant value="51:3-54:11"/>
		<constant value="55:5-55:15"/>
		<constant value="55:35-55:48"/>
		<constant value="55:50-55:53"/>
		<constant value="55:55-55:57"/>
		<constant value="55:5-55:58"/>
		<constant value="51:3-55:58"/>
		<constant value="48:5-55:58"/>
		<constant value="46:2-55:58"/>
		<constant value="arc"/>
		<constant value="outNodesNames"/>
		<constant value="inNodesNames"/>
		<constant value="asTina"/>
		<constant value="Mpetrinet!Arc;"/>
		<constant value="8"/>
		<constant value="target"/>
		<constant value="source"/>
		<constant value="kind"/>
		<constant value="EnumLiteral"/>
		<constant value="normal"/>
		<constant value="23"/>
		<constant value="?"/>
		<constant value="24"/>
		<constant value="*"/>
		<constant value="weight"/>
		<constant value="63:5-63:13"/>
		<constant value="63:16-63:20"/>
		<constant value="63:5-63:20"/>
		<constant value="66:3-66:7"/>
		<constant value="66:3-66:14"/>
		<constant value="66:3-66:19"/>
		<constant value="64:3-64:7"/>
		<constant value="64:3-64:14"/>
		<constant value="64:3-64:19"/>
		<constant value="63:2-67:7"/>
		<constant value="69:5-69:9"/>
		<constant value="69:5-69:14"/>
		<constant value="69:17-69:24"/>
		<constant value="69:5-69:24"/>
		<constant value="72:3-72:6"/>
		<constant value="70:3-70:6"/>
		<constant value="69:2-73:7"/>
		<constant value="63:2-73:7"/>
		<constant value="74:4-74:8"/>
		<constant value="74:4-74:15"/>
		<constant value="74:4-74:26"/>
		<constant value="63:2-74:26"/>
		<constant value="isSource"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<push arg="3"/>
			<store arg="4"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<push arg="7"/>
			<push arg="8"/>
			<findme/>
			<call arg="9"/>
			<iterate/>
			<store arg="10"/>
			<load arg="10"/>
			<call arg="11"/>
			<load arg="4"/>
			<load arg="10"/>
			<get arg="12"/>
			<call arg="13"/>
			<push arg="14"/>
			<call arg="13"/>
			<call arg="15"/>
			<call arg="16"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="17" begin="0" end="0"/>
			<lne id="18" begin="5" end="7"/>
			<lne id="19" begin="5" end="8"/>
			<lne id="20" begin="11" end="11"/>
			<lne id="21" begin="11" end="12"/>
			<lne id="22" begin="13" end="13"/>
			<lne id="23" begin="14" end="14"/>
			<lne id="24" begin="14" end="15"/>
			<lne id="25" begin="13" end="16"/>
			<lne id="26" begin="17" end="17"/>
			<lne id="27" begin="13" end="18"/>
			<lne id="28" begin="11" end="19"/>
			<lne id="29" begin="2" end="21"/>
			<lne id="30" begin="0" end="21"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="31" begin="10" end="20"/>
			<lve slot="1" name="32" begin="1" end="21"/>
			<lve slot="0" name="33" begin="0" end="21"/>
		</localvariabletable>
	</operation>
	<operation name="34">
		<context type="2"/>
		<parameters>
			<parameter name="4" type="35"/>
			<parameter name="10" type="35"/>
			<parameter name="36" type="35"/>
		</parameters>
		<code>
			<push arg="37"/>
			<store arg="38"/>
			<load arg="4"/>
			<iterate/>
			<store arg="39"/>
			<load arg="38"/>
			<load arg="10"/>
			<call arg="13"/>
			<load arg="39"/>
			<call arg="13"/>
			<load arg="36"/>
			<call arg="13"/>
			<store arg="38"/>
			<enditerate/>
			<load arg="38"/>
		</code>
		<linenumbertable>
			<lne id="40" begin="0" end="0"/>
			<lne id="41" begin="2" end="2"/>
			<lne id="42" begin="5" end="5"/>
			<lne id="43" begin="6" end="6"/>
			<lne id="44" begin="5" end="7"/>
			<lne id="45" begin="8" end="8"/>
			<lne id="46" begin="5" end="9"/>
			<lne id="47" begin="10" end="10"/>
			<lne id="48" begin="5" end="11"/>
			<lne id="49" begin="0" end="14"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="50" begin="4" end="12"/>
			<lve slot="4" name="51" begin="1" end="14"/>
			<lve slot="0" name="33" begin="0" end="14"/>
			<lve slot="1" name="52" begin="0" end="14"/>
			<lve slot="2" name="53" begin="0" end="14"/>
			<lve slot="3" name="54" begin="0" end="14"/>
		</localvariabletable>
	</operation>
	<operation name="55">
		<context type="2"/>
		<parameters>
			<parameter name="4" type="35"/>
		</parameters>
		<code>
			<load arg="4"/>
			<pushi arg="56"/>
			<pushi arg="4"/>
			<call arg="57"/>
			<call arg="58"/>
			<if arg="59"/>
			<load arg="4"/>
			<call arg="60"/>
			<push arg="61"/>
			<call arg="13"/>
			<goto arg="62"/>
			<push arg="63"/>
		</code>
		<linenumbertable>
			<lne id="64" begin="0" end="0"/>
			<lne id="65" begin="2" end="2"/>
			<lne id="66" begin="1" end="3"/>
			<lne id="67" begin="0" end="4"/>
			<lne id="68" begin="6" end="6"/>
			<lne id="69" begin="6" end="7"/>
			<lne id="70" begin="8" end="8"/>
			<lne id="71" begin="6" end="9"/>
			<lne id="72" begin="11" end="11"/>
			<lne id="73" begin="0" end="11"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="11"/>
			<lve slot="1" name="74" begin="0" end="11"/>
		</localvariabletable>
	</operation>
	<operation name="75">
		<context type="76"/>
		<parameters>
		</parameters>
		<code>
			<push arg="77"/>
			<load arg="56"/>
			<get arg="12"/>
			<call arg="13"/>
			<push arg="78"/>
			<call arg="13"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<load arg="56"/>
			<get arg="79"/>
			<iterate/>
			<store arg="4"/>
			<load arg="4"/>
			<call arg="11"/>
			<call arg="16"/>
			<enditerate/>
			<store arg="4"/>
			<getasm/>
			<load arg="4"/>
			<push arg="37"/>
			<push arg="78"/>
			<call arg="80"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="81" begin="0" end="0"/>
			<lne id="82" begin="1" end="1"/>
			<lne id="83" begin="1" end="2"/>
			<lne id="84" begin="0" end="3"/>
			<lne id="85" begin="4" end="4"/>
			<lne id="86" begin="0" end="5"/>
			<lne id="87" begin="9" end="9"/>
			<lne id="88" begin="9" end="10"/>
			<lne id="89" begin="13" end="13"/>
			<lne id="90" begin="13" end="14"/>
			<lne id="91" begin="6" end="16"/>
			<lne id="92" begin="18" end="18"/>
			<lne id="93" begin="19" end="19"/>
			<lne id="94" begin="20" end="20"/>
			<lne id="95" begin="21" end="21"/>
			<lne id="96" begin="18" end="22"/>
			<lne id="97" begin="6" end="22"/>
			<lne id="98" begin="0" end="23"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="99" begin="12" end="15"/>
			<lve slot="1" name="100" begin="17" end="22"/>
			<lve slot="0" name="33" begin="0" end="23"/>
		</localvariabletable>
	</operation>
	<operation name="75">
		<context type="101"/>
		<parameters>
		</parameters>
		<code>
			<push arg="102"/>
			<load arg="56"/>
			<get arg="12"/>
			<call arg="13"/>
			<push arg="103"/>
			<call arg="13"/>
			<load arg="56"/>
			<get arg="104"/>
			<call arg="60"/>
			<call arg="13"/>
			<push arg="105"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="106" begin="0" end="0"/>
			<lne id="107" begin="1" end="1"/>
			<lne id="108" begin="1" end="2"/>
			<lne id="109" begin="0" end="3"/>
			<lne id="110" begin="4" end="4"/>
			<lne id="111" begin="0" end="5"/>
			<lne id="112" begin="6" end="6"/>
			<lne id="113" begin="6" end="7"/>
			<lne id="114" begin="6" end="8"/>
			<lne id="115" begin="0" end="9"/>
			<lne id="116" begin="10" end="10"/>
			<lne id="117" begin="0" end="11"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="11"/>
		</localvariabletable>
	</operation>
	<operation name="75">
		<context type="118"/>
		<parameters>
		</parameters>
		<code>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<load arg="56"/>
			<get arg="119"/>
			<iterate/>
			<store arg="4"/>
			<load arg="4"/>
			<pusht/>
			<call arg="120"/>
			<call arg="16"/>
			<enditerate/>
			<store arg="4"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<load arg="56"/>
			<get arg="121"/>
			<iterate/>
			<store arg="10"/>
			<load arg="10"/>
			<pushf/>
			<call arg="120"/>
			<call arg="16"/>
			<enditerate/>
			<store arg="10"/>
			<push arg="122"/>
			<load arg="56"/>
			<get arg="12"/>
			<call arg="13"/>
			<push arg="123"/>
			<call arg="13"/>
			<push arg="124"/>
			<call arg="13"/>
			<load arg="56"/>
			<get arg="125"/>
			<call arg="13"/>
			<push arg="126"/>
			<call arg="13"/>
			<getasm/>
			<load arg="56"/>
			<get arg="127"/>
			<call arg="128"/>
			<call arg="13"/>
			<getasm/>
			<load arg="4"/>
			<push arg="123"/>
			<push arg="37"/>
			<call arg="80"/>
			<call arg="13"/>
			<push arg="129"/>
			<call arg="13"/>
			<getasm/>
			<load arg="10"/>
			<push arg="123"/>
			<push arg="37"/>
			<call arg="80"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="130" begin="3" end="3"/>
			<lne id="131" begin="3" end="4"/>
			<lne id="132" begin="7" end="7"/>
			<lne id="133" begin="8" end="8"/>
			<lne id="134" begin="7" end="9"/>
			<lne id="135" begin="0" end="11"/>
			<lne id="136" begin="16" end="16"/>
			<lne id="137" begin="16" end="17"/>
			<lne id="138" begin="20" end="20"/>
			<lne id="139" begin="21" end="21"/>
			<lne id="140" begin="20" end="22"/>
			<lne id="141" begin="13" end="24"/>
			<lne id="142" begin="26" end="26"/>
			<lne id="143" begin="27" end="27"/>
			<lne id="144" begin="27" end="28"/>
			<lne id="145" begin="26" end="29"/>
			<lne id="146" begin="30" end="30"/>
			<lne id="147" begin="26" end="31"/>
			<lne id="148" begin="32" end="32"/>
			<lne id="149" begin="26" end="33"/>
			<lne id="150" begin="34" end="34"/>
			<lne id="151" begin="34" end="35"/>
			<lne id="152" begin="26" end="36"/>
			<lne id="153" begin="37" end="37"/>
			<lne id="154" begin="26" end="38"/>
			<lne id="155" begin="39" end="39"/>
			<lne id="156" begin="40" end="40"/>
			<lne id="157" begin="40" end="41"/>
			<lne id="158" begin="39" end="42"/>
			<lne id="159" begin="26" end="43"/>
			<lne id="160" begin="44" end="44"/>
			<lne id="161" begin="45" end="45"/>
			<lne id="162" begin="46" end="46"/>
			<lne id="163" begin="47" end="47"/>
			<lne id="164" begin="44" end="48"/>
			<lne id="165" begin="26" end="49"/>
			<lne id="166" begin="50" end="50"/>
			<lne id="167" begin="26" end="51"/>
			<lne id="168" begin="52" end="52"/>
			<lne id="169" begin="53" end="53"/>
			<lne id="170" begin="54" end="54"/>
			<lne id="171" begin="55" end="55"/>
			<lne id="172" begin="52" end="56"/>
			<lne id="173" begin="26" end="57"/>
			<lne id="174" begin="13" end="57"/>
			<lne id="175" begin="0" end="57"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="176" begin="6" end="10"/>
			<lve slot="2" name="176" begin="19" end="23"/>
			<lve slot="2" name="177" begin="25" end="57"/>
			<lve slot="1" name="178" begin="12" end="57"/>
			<lve slot="0" name="33" begin="0" end="57"/>
		</localvariabletable>
	</operation>
	<operation name="179">
		<context type="180"/>
		<parameters>
			<parameter name="4" type="35"/>
		</parameters>
		<code>
			<load arg="4"/>
			<pusht/>
			<call arg="58"/>
			<if arg="181"/>
			<load arg="56"/>
			<get arg="182"/>
			<get arg="12"/>
			<goto arg="59"/>
			<load arg="56"/>
			<get arg="183"/>
			<get arg="12"/>
			<load arg="56"/>
			<get arg="184"/>
			<push arg="185"/>
			<push arg="6"/>
			<new/>
			<dup/>
			<push arg="186"/>
			<set arg="12"/>
			<call arg="58"/>
			<if arg="187"/>
			<push arg="188"/>
			<goto arg="189"/>
			<push arg="190"/>
			<call arg="13"/>
			<load arg="56"/>
			<get arg="191"/>
			<call arg="60"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="192" begin="0" end="0"/>
			<lne id="193" begin="1" end="1"/>
			<lne id="194" begin="0" end="2"/>
			<lne id="195" begin="4" end="4"/>
			<lne id="196" begin="4" end="5"/>
			<lne id="197" begin="4" end="6"/>
			<lne id="198" begin="8" end="8"/>
			<lne id="199" begin="8" end="9"/>
			<lne id="200" begin="8" end="10"/>
			<lne id="201" begin="0" end="10"/>
			<lne id="202" begin="11" end="11"/>
			<lne id="203" begin="11" end="12"/>
			<lne id="204" begin="13" end="18"/>
			<lne id="205" begin="11" end="19"/>
			<lne id="206" begin="21" end="21"/>
			<lne id="207" begin="23" end="23"/>
			<lne id="208" begin="11" end="23"/>
			<lne id="209" begin="0" end="24"/>
			<lne id="210" begin="25" end="25"/>
			<lne id="211" begin="25" end="26"/>
			<lne id="212" begin="25" end="27"/>
			<lne id="213" begin="0" end="28"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="33" begin="0" end="28"/>
			<lve slot="1" name="214" begin="0" end="28"/>
		</localvariabletable>
	</operation>
</asm>
