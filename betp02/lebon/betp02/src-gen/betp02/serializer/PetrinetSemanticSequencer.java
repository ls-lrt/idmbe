package betp02.serializer;

import betp02.services.PetrinetGrammarAccess;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import petrinet.Arc;
import petrinet.PetriNet;
import petrinet.PetrinetPackage;
import petrinet.Place;
import petrinet.Transition;

@SuppressWarnings("all")
public class PetrinetSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private PetrinetGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == PetrinetPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case PetrinetPackage.ARC:
				if(context == grammarAccess.getPnArcRule()) {
					sequence_PnArc(context, (Arc) semanticObject); 
					return; 
				}
				else break;
			case PetrinetPackage.PETRI_NET:
				if(context == grammarAccess.getPetrinetRule()) {
					sequence_Petrinet(context, (PetriNet) semanticObject); 
					return; 
				}
				else break;
			case PetrinetPackage.PLACE:
				if(context == grammarAccess.getPlaceRule() ||
				   context == grammarAccess.getPnNodeRule()) {
					sequence_Place(context, (Place) semanticObject); 
					return; 
				}
				else break;
			case PetrinetPackage.TRANSITION:
				if(context == grammarAccess.getPnNodeRule() ||
				   context == grammarAccess.getTransitionRule()) {
					sequence_Transition(context, (Transition) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID nodes+=PnNode* arcs+=PnArc*)
	 */
	protected void sequence_Petrinet(EObject context, PetriNet semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID marking=INT)
	 */
	protected void sequence_Place(EObject context, Place semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (source=[Node|ID] target=[Node|ID] weight=INT kind=ArcKind)
	 */
	protected void sequence_PnArc(EObject context, Arc semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID min_time=INT max_time=INT)
	 */
	protected void sequence_Transition(EObject context, Transition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
