
package fr.xtext.improvedsimplepdl;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class ImprovedSimplePdlStandaloneSetup extends ImprovedSimplePdlStandaloneSetupGenerated{

	public static void doSetup() {
		new ImprovedSimplePdlStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

