module SimplePDL2PetriNet;
create OUT: petrinet from  IN: simplepdl;

-- Obtenir le processus qui contient ce process element.
-- Remarque: Ce helper ne serait pas utile si une référence opposite
-- avait été placée entre Process et ProcessElement
helper context simplepdl!ProcessElement
def: getProcess(): simplepdl!Process =
	simplepdl!Process.allInstances()
		->select(p | p.processElements->includes(self))
		->asSequence()->first();

-- Traduire un Process en un PetriNet de même nom
rule Process2PetriNet {
	from p: simplepdl!Process
	to pn: petrinet!PetriNet (name <- p.name),

	--PLACES associées à un processus
	--place avant que le processus commence
	p_proc_idle: petrinet!Place(
		name <- p.name + '_proc_idle',		
		marking <- 1,
		net <- pn),
	--place running
	p_proc_run: petrinet!Place(
		name <- p.name + '_proc_run',		
		marking <- 0,
		net <- pn),
	--place finished
	p_proc_finished: petrinet!Place(
		name <- p.name + '_proc_finished',		
		marking <- 0,
		net <- pn),
	--TRANSITION associées à un processus
	--transition start	
	t_proc_start: petrinet!Transition(
			name <- p.name + '_proc_start',
			net <- pn,
			min_time <- 0,
			max_time <- 0),
	--transition finish	
	t_proc_finish: petrinet!Transition(
			name <- p.name + '_proc_finish',
			net <- pn,
			min_time <- 0,
			max_time <- -1),
			
	--ARCS associés à un processus
	a_idle2start: petrinet!Arc(
			source <- p_proc_idle,
			target <- t_proc_start,
			weight <- 1,
			kind <- #normal,
			net <- pn),
	a_start2run: petrinet!Arc(
			source <- t_proc_start,
			target <- p_proc_run,
			weight <- 1,
			kind <- #normal,
			net <- pn),
	a_run2finish: petrinet!Arc(
			source <- p_proc_run,
			target <- t_proc_finish,
			weight <- 1,
			kind <- #normal,
			net <- pn),
	a_finish2finished: petrinet!Arc(
			source <- t_proc_finish,
			target <- p_proc_finished,
			weight <- 1,
			kind <- #normal,
			net <- pn),
			
	--OBSERVER
	--PLACES de l'observer
		--on ajoute les états early ontime et toolate
		--on a fini avant t_min
		p_obs_proc_early : petrinet!Place(
				name <- p.name + '_early',		
				marking <- 0,
				net <- pn),
		--on termine entre t_min et t_max
		p_obs_proc_ontime: petrinet!Place(
				name <- p.name + '_on_time',		
				marking <- 0,
				net <- pn),
		--on termine apres t_max
		p_obs_proc_toolate: petrinet!Place(
				name <- p.name + '_too_late',		
				marking <- 0,
				net <- pn),				
		--TRANSITIONS de l'observer
		--sera franchie si la tache finie en moins de t_min 
		t_obs_proc_finished_on_time: petrinet!Transition(
			   name <- p.name + '_t_on_time',
			   net <- pn,
			   min_time <- p.min_time,
			   max_time <- p.min_time),
		--on franchira cette transition au bout de max_time - min_time
		t_obs_proc_finished_too_late: petrinet!Transition(
			   name <- p.name + '_t_time_over',
			   net <- pn,
			   min_time <- p.max_time - p.min_time,
			   max_time <- p.max_time - p.min_time),
		--ARCS de l'observateur
		--sur le start, on ajoute un jeton dans cette place
		a_start2early: petrinet!Arc(
				source <- t_proc_start,
				target <- p_obs_proc_early,
				weight <-1,
				kind <- #normal,
				net <- pn),
		--processus fini apres mintime
		a_run2ontime: petrinet!Arc(
				source <- p_proc_run,
				target <- t_obs_proc_finished_on_time,
				weight <-1,
				kind <- #read_arc,
				net <- pn),
		--processus fini apres mintime (suite)
		a_pobsearly2tobsontime:petrinet!Arc(
				source <- p_obs_proc_early,
				target <- t_obs_proc_finished_on_time,
				weight <-1,
				kind <- #normal,
				net <- pn),
		--processus fini apres mintime (fin)
		a_tobsontime2pobsontime:petrinet!Arc(
				source <- t_obs_proc_finished_on_time,
				target <- p_obs_proc_ontime,
				weight <-1,
				kind <- #normal,
				net <- pn),
		--processus fini trop tard
		a_obsprocontime2ttoolate: petrinet!Arc(
				source <- p_obs_proc_ontime,
				target <- t_obs_proc_finished_too_late,
				weight <-1,
				kind <- #normal,
				net <- pn),
		--processus fini trop tard (suite)
		a_procrun2toolate: petrinet!Arc(
				source <- p_proc_run,
				target <- t_obs_proc_finished_too_late,
				weight <-1,
				kind <- #read_arc,
				net <- pn),
		--processus fini trop tard (fin)
		a_ttoolate2ptoolate: petrinet!Arc(
				source <- t_obs_proc_finished_too_late,
				target <- p_obs_proc_toolate,
				weight <-1,
				kind <- #normal,
				net <- pn)
}

-- Traduire une WorkDefinition en un motif sur le réseau de Petri
rule WorkDefinition2PetriNet {
	from wd: simplepdl!WorkDefinition
	to
		-- PLACES d'une WorkDefinition
		p_ready: petrinet!Place(
				name <- wd.name + '_ready',
				marking <- 0,
				net <- wd.getProcess()),
		p_started: petrinet!Place(
				name <- wd.name + '_started',
				marking <- 0,
				net <- wd.getProcess()),
		-- place permettant de mémoriser qu'une tâche a été commencée		
		p_wasstarted: petrinet!Place(
				name <- wd.name + '_wasstarted',
				marking <- 0,
				net <- wd.getProcess()),
		p_finished: petrinet!Place(
				name <- wd.name + '_finished',
				marking <- 0,
				net <- wd.getProcess()),
		p_wasfinished: petrinet!Place(
				name <- wd.name + '_wasfinished',
				marking <- 0,
				net <- wd.getProcess()),
		--nouvelle place pour la mise en pause de la tache
		p_paused: petrinet!Place(
				name <- wd.name + '_paused',
				marking <- 0,
				net <- wd.getProcess()),		
	    -- TRANSITIONS d'une WorkDefinition
		t_start: petrinet!Transition(
				name <- wd.name + '_start',
				net <- wd.getProcess(),
				min_time <- 0,
			    max_time <- -1),
		t_finish: petrinet!Transition(
			    name <- wd.name + '_finish',
				net <- wd.getProcess(),
				min_time <- 0,
			    max_time <- -1),
		--deux nouvelles transitions: pause et recover
		t_pause: petrinet!Transition(
				name <- wd.name + '_pause',
				net <- wd.getProcess(),
				min_time <- 0,
			    max_time <- -1),
		t_recover: petrinet!Transition(
			    name <- wd.name + '_recover',
				net <- wd.getProcess(),
				min_time <- 0,
			    max_time <- -1),
		-- ARCS d'une WorkDefinition
		-- quand le processus commence, il distribue les jetons
		a_tprocstart2pready: petrinet!Arc(
				source <- thisModule.resolveTemp(wd.getProcess(), 't_proc_start'),
				target <- p_ready,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		-- les jetons sont concervés dans les places wasfinished pour que les observateurs
		-- puissent finir
		a_finish2wasfinished: petrinet!Arc(
				source <- t_finish,
				target <- p_wasfinished,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		a_finished2tprocfinshed: petrinet!Arc(
				source <- p_finished,
				target <- thisModule.resolveTemp(wd.getProcess(), 't_proc_finish'),
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),		
		a_ready2start: petrinet!Arc(
				source <- p_ready,
				target <- t_start,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		--on rajoute deux jetons pour modifier le moins possible la structure
		a_start2started: petrinet!Arc(
				source <- t_start,
				target <- p_started,
				weight <- 2,
				kind <- #normal,
				net <- wd.getProcess()),
		a_start2wasstarted: petrinet!Arc(
				source <- t_start,
				target <- p_wasstarted,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),				
		--il faut deux jetons pour pouvoir passer cette transition
		--ce qui est impossible quand la tache est mise en pause
		a_started2finish: petrinet!Arc(
				source <- p_started,
				target <- t_finish,
				weight <- 2,
				kind <- #normal,
				net <- wd.getProcess()),	
		a_finish2finished: petrinet!Arc(
				source <- t_finish,
				target <- p_finished,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		--on lie le nouvel état avec les transitions pause et recover
		a_run2pause: petrinet!Arc(
				source <- p_started,
				target <- t_pause,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		a_pause2paused: petrinet!Arc(
				source <- t_pause,
				target <- p_paused,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		a_paused2recover: petrinet!Arc(
				source <- p_paused,
				target <- t_recover,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		a_recover2run: petrinet!Arc(
				source <- t_recover,
				target <- p_started,
				weight <- 1,
				kind <- #normal,
				net <- wd.getProcess()),
		--
		--OBSERVER
		--		
		--PLACES de l'observer
		--on ajoute les états early ontime et toolate
		--on a fini avant t_min
		p_obs_early : petrinet!Place(
				name <- wd.name + '_early',		
				marking <- 0,
				net <- wd.getProcess()),
		--on termine entre t_min et t_max
		p_obs_ontime: petrinet!Place(
				name <- wd.name + '_on_time',		
				marking <- 0,
				net <- wd.getProcess()),
		--on termine apres t_max
		p_obs_toolate: petrinet!Place(
				name <- wd.name + '_too_late',		
				marking <- 0,
				net <- wd.getProcess()),				
		--TRANSITIONS de l'observer
		--on franchira cette transition au bout de min_time
		t_obs_on_time: petrinet!Transition(
			   name <- wd.name + '_t_check_on_time',
			   net <- wd.getProcess(),
			   min_time <- wd.min_time,
			   max_time <- wd.min_time),
		--on franchira cette transition au bout de max_time - min_time
		t_obs_time_over: petrinet!Transition(
			   name <- wd.name + '_t_time_over',
			   net <- wd.getProcess(),
			   min_time <- wd.max_time - wd.min_time,
			   max_time <- wd.max_time - wd.min_time),
		--ARCS de l'observateur
		--sur le start, on ajoute un jeton dans cette place
		a_start2pobsearly: petrinet!Arc(
				source <- t_start,
				target <- p_obs_early,
				weight <-1,
				kind <- #normal,
				net <- wd.getProcess()),	
		--tache finie dans les temps
		a_obsearly2tobsontime: petrinet!Arc(
				source <- p_obs_early,
				target <- t_obs_on_time,
				weight <-1,
				kind <- #normal,
				net <- wd.getProcess()),
		--tache finie dans les temps (suite)
		a_finished2tobsontime: petrinet!Arc(
				source <- p_started,
				target <- t_obs_on_time,
				weight <-1,
				kind <- #read_arc,
				net <- wd.getProcess()),
		--tache finie dans les temps (fin)
		a_tobsontime2pontime: petrinet!Arc(
				source <- t_obs_on_time,
				target <- p_obs_ontime,
				weight <-1,
				kind <- #normal,
				net <- wd.getProcess()),
		--tache dépasse le max time
		a_pobsontime2timeover: petrinet!Arc(
				source <- p_obs_ontime,
				target <- t_obs_time_over,
				weight <-1,
				kind <- #normal,
				net <- wd.getProcess()),
		--tache dépasse le max time (suite), la tâche doit toujours etre en cours
		a_started2timeover: petrinet!Arc(
				source <- p_started,
				target <- t_obs_time_over,
				weight <-1,
				kind <- #read_arc,
				net <- wd.getProcess()),
		--tache dépasse le max time (fin)
		a_timeover2toolate: petrinet!Arc(
				source <- t_obs_time_over,
				target <- p_obs_toolate,
				weight <-1,
				kind <- #normal,
				net <- wd.getProcess())
}

-- Traduire une WorkSequence en un motif sur le réseau de Petri
rule WorkSequence2PetriNet {
	from ws: simplepdl!WorkSequence
	to
		-- ARCS d'une WorkSequence
		a: petrinet!Arc(
			-- tout ce qui commence par finish
			source <- if (ws.linkType = #finishToStart or ws.linkType = #finishToFinish)
					then thisModule.resolveTemp(ws.predecessor, 'p_wasfinished')
					else thisModule.resolveTemp(ws.predecessor, 'p_wasstarted')
					endif,
			-- tout ce qui termine par finish
			target <- if (ws.linkType = #startToFinish or ws.linkType = #finishToFinish)
					then thisModule.resolveTemp(ws.successor, 't_finish')
					else thisModule.resolveTemp(ws.successor, 't_start')
					endif,
			weight <-1,
			--on concerve les jetons dans les places
			kind <- #read_arc,
			net <- ws.successor.getProcess())
}


-- traduire une Ressource en un motif sur le réseau de Petri 
rule Ressource2Petrinet {
	from res: simplepdl!Ressource
	to
		--place correspondant à une ressource
		p_ressource: petrinet!Place(
				--le nom sera préfixé par res_
				name <- 'res_' + res.name,
				--il y aura autant de jetons que de ressources disponibles
				marking <- res.quantity,
				net <- res.getProcess())
}

-- traduire une réservation de ressource en un motif sur le réseau de Petri
rule RessourceLink2PetriNet {
	from rl: simplepdl!RessourceLink
	to
	
		--ARC correspondant à la réservation de ressource
		a_use: petrinet!Arc(
			source <- rl.ressource,
			target <- thisModule.resolveTemp(rl.workDef, 't_start'),
			weight <-rl.weight,
			--il faut préempter les ressources
			kind <- #normal,
			net <- rl.getProcess()),
		--ARC correspondant à la libération de ressource	
		a_release: petrinet!Arc(
			source <- thisModule.resolveTemp(rl.workDef, 't_finish'),
			target <- rl.ressource,
			weight <-rl.weight,
			--il faut préempter les ressources
			kind <- #normal,
			net <- rl.getProcess()),
		--ARC correspondant au relachement des ressources suite a une pause	
		a_release_pause: petrinet!Arc(
			source <- thisModule.resolveTemp(rl.workDef, 't_pause'),
			target <- rl.ressource,
			weight <-rl.weight,
			kind <- #normal,
			net <- rl.getProcess()),
		--ARC correspondant à la reréservation de ressources suite à une reprise
		a_reuse: petrinet!Arc(
			source <- rl.ressource,
			target <- thisModule.resolveTemp(rl.workDef, 't_recover'),
			weight <-rl.weight,
			--il faut  préempter les ressources
			kind <- #normal,
			net <- rl.getProcess())
}
