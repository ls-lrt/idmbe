package betp02.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import betp02.services.PetrinetGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPetrinetParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'read_arc'", "'normal'", "'pn'", "'{'", "'}'", "'a'", "','", "'\\n'", "'p'", "'['", "']'", "'t'", "']\\n'"
    };
    public static final int RULE_ID=4;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalPetrinetParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPetrinetParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPetrinetParser.tokenNames; }
    public String getGrammarFileName() { return "../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g"; }


     
     	private PetrinetGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(PetrinetGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRulePetrinet"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:60:1: entryRulePetrinet : rulePetrinet EOF ;
    public final void entryRulePetrinet() throws RecognitionException {
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:61:1: ( rulePetrinet EOF )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:62:1: rulePetrinet EOF
            {
             before(grammarAccess.getPetrinetRule()); 
            pushFollow(FOLLOW_rulePetrinet_in_entryRulePetrinet61);
            rulePetrinet();

            state._fsp--;

             after(grammarAccess.getPetrinetRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePetrinet68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePetrinet"


    // $ANTLR start "rulePetrinet"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:69:1: rulePetrinet : ( ( rule__Petrinet__Group__0 ) ) ;
    public final void rulePetrinet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:73:2: ( ( ( rule__Petrinet__Group__0 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:74:1: ( ( rule__Petrinet__Group__0 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:74:1: ( ( rule__Petrinet__Group__0 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:75:1: ( rule__Petrinet__Group__0 )
            {
             before(grammarAccess.getPetrinetAccess().getGroup()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:76:1: ( rule__Petrinet__Group__0 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:76:2: rule__Petrinet__Group__0
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__0_in_rulePetrinet94);
            rule__Petrinet__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPetrinetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePetrinet"


    // $ANTLR start "entryRulePnNode"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:88:1: entryRulePnNode : rulePnNode EOF ;
    public final void entryRulePnNode() throws RecognitionException {
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:89:1: ( rulePnNode EOF )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:90:1: rulePnNode EOF
            {
             before(grammarAccess.getPnNodeRule()); 
            pushFollow(FOLLOW_rulePnNode_in_entryRulePnNode121);
            rulePnNode();

            state._fsp--;

             after(grammarAccess.getPnNodeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePnNode128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePnNode"


    // $ANTLR start "rulePnNode"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:97:1: rulePnNode : ( ( rule__PnNode__Alternatives ) ) ;
    public final void rulePnNode() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:101:2: ( ( ( rule__PnNode__Alternatives ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:102:1: ( ( rule__PnNode__Alternatives ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:102:1: ( ( rule__PnNode__Alternatives ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:103:1: ( rule__PnNode__Alternatives )
            {
             before(grammarAccess.getPnNodeAccess().getAlternatives()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:104:1: ( rule__PnNode__Alternatives )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:104:2: rule__PnNode__Alternatives
            {
            pushFollow(FOLLOW_rule__PnNode__Alternatives_in_rulePnNode154);
            rule__PnNode__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPnNodeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePnNode"


    // $ANTLR start "entryRulePnArc"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:116:1: entryRulePnArc : rulePnArc EOF ;
    public final void entryRulePnArc() throws RecognitionException {
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:117:1: ( rulePnArc EOF )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:118:1: rulePnArc EOF
            {
             before(grammarAccess.getPnArcRule()); 
            pushFollow(FOLLOW_rulePnArc_in_entryRulePnArc181);
            rulePnArc();

            state._fsp--;

             after(grammarAccess.getPnArcRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePnArc188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePnArc"


    // $ANTLR start "rulePnArc"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:125:1: rulePnArc : ( ( rule__PnArc__Group__0 ) ) ;
    public final void rulePnArc() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:129:2: ( ( ( rule__PnArc__Group__0 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:130:1: ( ( rule__PnArc__Group__0 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:130:1: ( ( rule__PnArc__Group__0 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:131:1: ( rule__PnArc__Group__0 )
            {
             before(grammarAccess.getPnArcAccess().getGroup()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:132:1: ( rule__PnArc__Group__0 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:132:2: rule__PnArc__Group__0
            {
            pushFollow(FOLLOW_rule__PnArc__Group__0_in_rulePnArc214);
            rule__PnArc__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPnArcAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePnArc"


    // $ANTLR start "entryRulePlace"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:144:1: entryRulePlace : rulePlace EOF ;
    public final void entryRulePlace() throws RecognitionException {
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:145:1: ( rulePlace EOF )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:146:1: rulePlace EOF
            {
             before(grammarAccess.getPlaceRule()); 
            pushFollow(FOLLOW_rulePlace_in_entryRulePlace241);
            rulePlace();

            state._fsp--;

             after(grammarAccess.getPlaceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePlace248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:153:1: rulePlace : ( ( rule__Place__Group__0 ) ) ;
    public final void rulePlace() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:157:2: ( ( ( rule__Place__Group__0 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:158:1: ( ( rule__Place__Group__0 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:158:1: ( ( rule__Place__Group__0 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:159:1: ( rule__Place__Group__0 )
            {
             before(grammarAccess.getPlaceAccess().getGroup()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:160:1: ( rule__Place__Group__0 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:160:2: rule__Place__Group__0
            {
            pushFollow(FOLLOW_rule__Place__Group__0_in_rulePlace274);
            rule__Place__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "entryRuleTransition"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:172:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:173:1: ( ruleTransition EOF )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:174:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_ruleTransition_in_entryRuleTransition301);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTransition308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:181:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:185:2: ( ( ( rule__Transition__Group__0 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:186:1: ( ( rule__Transition__Group__0 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:186:1: ( ( rule__Transition__Group__0 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:187:1: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:188:1: ( rule__Transition__Group__0 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:188:2: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_rule__Transition__Group__0_in_ruleTransition334);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "ruleArcKind"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:201:1: ruleArcKind : ( ( rule__ArcKind__Alternatives ) ) ;
    public final void ruleArcKind() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:205:1: ( ( ( rule__ArcKind__Alternatives ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:206:1: ( ( rule__ArcKind__Alternatives ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:206:1: ( ( rule__ArcKind__Alternatives ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:207:1: ( rule__ArcKind__Alternatives )
            {
             before(grammarAccess.getArcKindAccess().getAlternatives()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:208:1: ( rule__ArcKind__Alternatives )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:208:2: rule__ArcKind__Alternatives
            {
            pushFollow(FOLLOW_rule__ArcKind__Alternatives_in_ruleArcKind371);
            rule__ArcKind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getArcKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArcKind"


    // $ANTLR start "rule__PnNode__Alternatives"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:219:1: rule__PnNode__Alternatives : ( ( ruleTransition ) | ( rulePlace ) );
    public final void rule__PnNode__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:223:1: ( ( ruleTransition ) | ( rulePlace ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==22) ) {
                alt1=1;
            }
            else if ( (LA1_0==19) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:224:1: ( ruleTransition )
                    {
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:224:1: ( ruleTransition )
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:225:1: ruleTransition
                    {
                     before(grammarAccess.getPnNodeAccess().getTransitionParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleTransition_in_rule__PnNode__Alternatives406);
                    ruleTransition();

                    state._fsp--;

                     after(grammarAccess.getPnNodeAccess().getTransitionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:230:6: ( rulePlace )
                    {
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:230:6: ( rulePlace )
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:231:1: rulePlace
                    {
                     before(grammarAccess.getPnNodeAccess().getPlaceParserRuleCall_1()); 
                    pushFollow(FOLLOW_rulePlace_in_rule__PnNode__Alternatives423);
                    rulePlace();

                    state._fsp--;

                     after(grammarAccess.getPnNodeAccess().getPlaceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnNode__Alternatives"


    // $ANTLR start "rule__ArcKind__Alternatives"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:241:1: rule__ArcKind__Alternatives : ( ( ( 'read_arc' ) ) | ( ( 'normal' ) ) );
    public final void rule__ArcKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:245:1: ( ( ( 'read_arc' ) ) | ( ( 'normal' ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:246:1: ( ( 'read_arc' ) )
                    {
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:246:1: ( ( 'read_arc' ) )
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:247:1: ( 'read_arc' )
                    {
                     before(grammarAccess.getArcKindAccess().getRead_arcEnumLiteralDeclaration_0()); 
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:248:1: ( 'read_arc' )
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:248:3: 'read_arc'
                    {
                    match(input,11,FOLLOW_11_in_rule__ArcKind__Alternatives456); 

                    }

                     after(grammarAccess.getArcKindAccess().getRead_arcEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:253:6: ( ( 'normal' ) )
                    {
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:253:6: ( ( 'normal' ) )
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:254:1: ( 'normal' )
                    {
                     before(grammarAccess.getArcKindAccess().getNormalEnumLiteralDeclaration_1()); 
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:255:1: ( 'normal' )
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:255:3: 'normal'
                    {
                    match(input,12,FOLLOW_12_in_rule__ArcKind__Alternatives477); 

                    }

                     after(grammarAccess.getArcKindAccess().getNormalEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcKind__Alternatives"


    // $ANTLR start "rule__Petrinet__Group__0"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:267:1: rule__Petrinet__Group__0 : rule__Petrinet__Group__0__Impl rule__Petrinet__Group__1 ;
    public final void rule__Petrinet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:271:1: ( rule__Petrinet__Group__0__Impl rule__Petrinet__Group__1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:272:2: rule__Petrinet__Group__0__Impl rule__Petrinet__Group__1
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__0__Impl_in_rule__Petrinet__Group__0510);
            rule__Petrinet__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Petrinet__Group__1_in_rule__Petrinet__Group__0513);
            rule__Petrinet__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__0"


    // $ANTLR start "rule__Petrinet__Group__0__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:279:1: rule__Petrinet__Group__0__Impl : ( 'pn' ) ;
    public final void rule__Petrinet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:283:1: ( ( 'pn' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:284:1: ( 'pn' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:284:1: ( 'pn' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:285:1: 'pn'
            {
             before(grammarAccess.getPetrinetAccess().getPnKeyword_0()); 
            match(input,13,FOLLOW_13_in_rule__Petrinet__Group__0__Impl541); 
             after(grammarAccess.getPetrinetAccess().getPnKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__0__Impl"


    // $ANTLR start "rule__Petrinet__Group__1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:298:1: rule__Petrinet__Group__1 : rule__Petrinet__Group__1__Impl rule__Petrinet__Group__2 ;
    public final void rule__Petrinet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:302:1: ( rule__Petrinet__Group__1__Impl rule__Petrinet__Group__2 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:303:2: rule__Petrinet__Group__1__Impl rule__Petrinet__Group__2
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__1__Impl_in_rule__Petrinet__Group__1572);
            rule__Petrinet__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Petrinet__Group__2_in_rule__Petrinet__Group__1575);
            rule__Petrinet__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__1"


    // $ANTLR start "rule__Petrinet__Group__1__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:310:1: rule__Petrinet__Group__1__Impl : ( ( rule__Petrinet__NameAssignment_1 ) ) ;
    public final void rule__Petrinet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:314:1: ( ( ( rule__Petrinet__NameAssignment_1 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:315:1: ( ( rule__Petrinet__NameAssignment_1 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:315:1: ( ( rule__Petrinet__NameAssignment_1 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:316:1: ( rule__Petrinet__NameAssignment_1 )
            {
             before(grammarAccess.getPetrinetAccess().getNameAssignment_1()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:317:1: ( rule__Petrinet__NameAssignment_1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:317:2: rule__Petrinet__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Petrinet__NameAssignment_1_in_rule__Petrinet__Group__1__Impl602);
            rule__Petrinet__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPetrinetAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__1__Impl"


    // $ANTLR start "rule__Petrinet__Group__2"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:327:1: rule__Petrinet__Group__2 : rule__Petrinet__Group__2__Impl rule__Petrinet__Group__3 ;
    public final void rule__Petrinet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:331:1: ( rule__Petrinet__Group__2__Impl rule__Petrinet__Group__3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:332:2: rule__Petrinet__Group__2__Impl rule__Petrinet__Group__3
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__2__Impl_in_rule__Petrinet__Group__2632);
            rule__Petrinet__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Petrinet__Group__3_in_rule__Petrinet__Group__2635);
            rule__Petrinet__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__2"


    // $ANTLR start "rule__Petrinet__Group__2__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:339:1: rule__Petrinet__Group__2__Impl : ( '{' ) ;
    public final void rule__Petrinet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:343:1: ( ( '{' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:344:1: ( '{' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:344:1: ( '{' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:345:1: '{'
            {
             before(grammarAccess.getPetrinetAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,14,FOLLOW_14_in_rule__Petrinet__Group__2__Impl663); 
             after(grammarAccess.getPetrinetAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__2__Impl"


    // $ANTLR start "rule__Petrinet__Group__3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:358:1: rule__Petrinet__Group__3 : rule__Petrinet__Group__3__Impl rule__Petrinet__Group__4 ;
    public final void rule__Petrinet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:362:1: ( rule__Petrinet__Group__3__Impl rule__Petrinet__Group__4 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:363:2: rule__Petrinet__Group__3__Impl rule__Petrinet__Group__4
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__3__Impl_in_rule__Petrinet__Group__3694);
            rule__Petrinet__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Petrinet__Group__4_in_rule__Petrinet__Group__3697);
            rule__Petrinet__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__3"


    // $ANTLR start "rule__Petrinet__Group__3__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:370:1: rule__Petrinet__Group__3__Impl : ( ( rule__Petrinet__NodesAssignment_3 )* ) ;
    public final void rule__Petrinet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:374:1: ( ( ( rule__Petrinet__NodesAssignment_3 )* ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:375:1: ( ( rule__Petrinet__NodesAssignment_3 )* )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:375:1: ( ( rule__Petrinet__NodesAssignment_3 )* )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:376:1: ( rule__Petrinet__NodesAssignment_3 )*
            {
             before(grammarAccess.getPetrinetAccess().getNodesAssignment_3()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:377:1: ( rule__Petrinet__NodesAssignment_3 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==19||LA3_0==22) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:377:2: rule__Petrinet__NodesAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Petrinet__NodesAssignment_3_in_rule__Petrinet__Group__3__Impl724);
            	    rule__Petrinet__NodesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getPetrinetAccess().getNodesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__3__Impl"


    // $ANTLR start "rule__Petrinet__Group__4"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:387:1: rule__Petrinet__Group__4 : rule__Petrinet__Group__4__Impl rule__Petrinet__Group__5 ;
    public final void rule__Petrinet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:391:1: ( rule__Petrinet__Group__4__Impl rule__Petrinet__Group__5 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:392:2: rule__Petrinet__Group__4__Impl rule__Petrinet__Group__5
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__4__Impl_in_rule__Petrinet__Group__4755);
            rule__Petrinet__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Petrinet__Group__5_in_rule__Petrinet__Group__4758);
            rule__Petrinet__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__4"


    // $ANTLR start "rule__Petrinet__Group__4__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:399:1: rule__Petrinet__Group__4__Impl : ( ( rule__Petrinet__ArcsAssignment_4 )* ) ;
    public final void rule__Petrinet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:403:1: ( ( ( rule__Petrinet__ArcsAssignment_4 )* ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:404:1: ( ( rule__Petrinet__ArcsAssignment_4 )* )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:404:1: ( ( rule__Petrinet__ArcsAssignment_4 )* )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:405:1: ( rule__Petrinet__ArcsAssignment_4 )*
            {
             before(grammarAccess.getPetrinetAccess().getArcsAssignment_4()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:406:1: ( rule__Petrinet__ArcsAssignment_4 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==16) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:406:2: rule__Petrinet__ArcsAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__Petrinet__ArcsAssignment_4_in_rule__Petrinet__Group__4__Impl785);
            	    rule__Petrinet__ArcsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getPetrinetAccess().getArcsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__4__Impl"


    // $ANTLR start "rule__Petrinet__Group__5"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:416:1: rule__Petrinet__Group__5 : rule__Petrinet__Group__5__Impl ;
    public final void rule__Petrinet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:420:1: ( rule__Petrinet__Group__5__Impl )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:421:2: rule__Petrinet__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Petrinet__Group__5__Impl_in_rule__Petrinet__Group__5816);
            rule__Petrinet__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__5"


    // $ANTLR start "rule__Petrinet__Group__5__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:427:1: rule__Petrinet__Group__5__Impl : ( '}' ) ;
    public final void rule__Petrinet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:431:1: ( ( '}' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:432:1: ( '}' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:432:1: ( '}' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:433:1: '}'
            {
             before(grammarAccess.getPetrinetAccess().getRightCurlyBracketKeyword_5()); 
            match(input,15,FOLLOW_15_in_rule__Petrinet__Group__5__Impl844); 
             after(grammarAccess.getPetrinetAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__Group__5__Impl"


    // $ANTLR start "rule__PnArc__Group__0"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:458:1: rule__PnArc__Group__0 : rule__PnArc__Group__0__Impl rule__PnArc__Group__1 ;
    public final void rule__PnArc__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:462:1: ( rule__PnArc__Group__0__Impl rule__PnArc__Group__1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:463:2: rule__PnArc__Group__0__Impl rule__PnArc__Group__1
            {
            pushFollow(FOLLOW_rule__PnArc__Group__0__Impl_in_rule__PnArc__Group__0887);
            rule__PnArc__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__1_in_rule__PnArc__Group__0890);
            rule__PnArc__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__0"


    // $ANTLR start "rule__PnArc__Group__0__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:470:1: rule__PnArc__Group__0__Impl : ( 'a' ) ;
    public final void rule__PnArc__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:474:1: ( ( 'a' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:475:1: ( 'a' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:475:1: ( 'a' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:476:1: 'a'
            {
             before(grammarAccess.getPnArcAccess().getAKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__PnArc__Group__0__Impl918); 
             after(grammarAccess.getPnArcAccess().getAKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__0__Impl"


    // $ANTLR start "rule__PnArc__Group__1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:489:1: rule__PnArc__Group__1 : rule__PnArc__Group__1__Impl rule__PnArc__Group__2 ;
    public final void rule__PnArc__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:493:1: ( rule__PnArc__Group__1__Impl rule__PnArc__Group__2 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:494:2: rule__PnArc__Group__1__Impl rule__PnArc__Group__2
            {
            pushFollow(FOLLOW_rule__PnArc__Group__1__Impl_in_rule__PnArc__Group__1949);
            rule__PnArc__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__2_in_rule__PnArc__Group__1952);
            rule__PnArc__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__1"


    // $ANTLR start "rule__PnArc__Group__1__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:501:1: rule__PnArc__Group__1__Impl : ( ( rule__PnArc__SourceAssignment_1 ) ) ;
    public final void rule__PnArc__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:505:1: ( ( ( rule__PnArc__SourceAssignment_1 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:506:1: ( ( rule__PnArc__SourceAssignment_1 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:506:1: ( ( rule__PnArc__SourceAssignment_1 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:507:1: ( rule__PnArc__SourceAssignment_1 )
            {
             before(grammarAccess.getPnArcAccess().getSourceAssignment_1()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:508:1: ( rule__PnArc__SourceAssignment_1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:508:2: rule__PnArc__SourceAssignment_1
            {
            pushFollow(FOLLOW_rule__PnArc__SourceAssignment_1_in_rule__PnArc__Group__1__Impl979);
            rule__PnArc__SourceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPnArcAccess().getSourceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__1__Impl"


    // $ANTLR start "rule__PnArc__Group__2"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:518:1: rule__PnArc__Group__2 : rule__PnArc__Group__2__Impl rule__PnArc__Group__3 ;
    public final void rule__PnArc__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:522:1: ( rule__PnArc__Group__2__Impl rule__PnArc__Group__3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:523:2: rule__PnArc__Group__2__Impl rule__PnArc__Group__3
            {
            pushFollow(FOLLOW_rule__PnArc__Group__2__Impl_in_rule__PnArc__Group__21009);
            rule__PnArc__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__3_in_rule__PnArc__Group__21012);
            rule__PnArc__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__2"


    // $ANTLR start "rule__PnArc__Group__2__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:530:1: rule__PnArc__Group__2__Impl : ( ',' ) ;
    public final void rule__PnArc__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:534:1: ( ( ',' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:535:1: ( ',' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:535:1: ( ',' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:536:1: ','
            {
             before(grammarAccess.getPnArcAccess().getCommaKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__PnArc__Group__2__Impl1040); 
             after(grammarAccess.getPnArcAccess().getCommaKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__2__Impl"


    // $ANTLR start "rule__PnArc__Group__3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:549:1: rule__PnArc__Group__3 : rule__PnArc__Group__3__Impl rule__PnArc__Group__4 ;
    public final void rule__PnArc__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:553:1: ( rule__PnArc__Group__3__Impl rule__PnArc__Group__4 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:554:2: rule__PnArc__Group__3__Impl rule__PnArc__Group__4
            {
            pushFollow(FOLLOW_rule__PnArc__Group__3__Impl_in_rule__PnArc__Group__31071);
            rule__PnArc__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__4_in_rule__PnArc__Group__31074);
            rule__PnArc__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__3"


    // $ANTLR start "rule__PnArc__Group__3__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:561:1: rule__PnArc__Group__3__Impl : ( ( rule__PnArc__TargetAssignment_3 ) ) ;
    public final void rule__PnArc__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:565:1: ( ( ( rule__PnArc__TargetAssignment_3 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:566:1: ( ( rule__PnArc__TargetAssignment_3 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:566:1: ( ( rule__PnArc__TargetAssignment_3 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:567:1: ( rule__PnArc__TargetAssignment_3 )
            {
             before(grammarAccess.getPnArcAccess().getTargetAssignment_3()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:568:1: ( rule__PnArc__TargetAssignment_3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:568:2: rule__PnArc__TargetAssignment_3
            {
            pushFollow(FOLLOW_rule__PnArc__TargetAssignment_3_in_rule__PnArc__Group__3__Impl1101);
            rule__PnArc__TargetAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPnArcAccess().getTargetAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__3__Impl"


    // $ANTLR start "rule__PnArc__Group__4"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:578:1: rule__PnArc__Group__4 : rule__PnArc__Group__4__Impl rule__PnArc__Group__5 ;
    public final void rule__PnArc__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:582:1: ( rule__PnArc__Group__4__Impl rule__PnArc__Group__5 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:583:2: rule__PnArc__Group__4__Impl rule__PnArc__Group__5
            {
            pushFollow(FOLLOW_rule__PnArc__Group__4__Impl_in_rule__PnArc__Group__41131);
            rule__PnArc__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__5_in_rule__PnArc__Group__41134);
            rule__PnArc__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__4"


    // $ANTLR start "rule__PnArc__Group__4__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:590:1: rule__PnArc__Group__4__Impl : ( ',' ) ;
    public final void rule__PnArc__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:594:1: ( ( ',' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:595:1: ( ',' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:595:1: ( ',' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:596:1: ','
            {
             before(grammarAccess.getPnArcAccess().getCommaKeyword_4()); 
            match(input,17,FOLLOW_17_in_rule__PnArc__Group__4__Impl1162); 
             after(grammarAccess.getPnArcAccess().getCommaKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__4__Impl"


    // $ANTLR start "rule__PnArc__Group__5"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:609:1: rule__PnArc__Group__5 : rule__PnArc__Group__5__Impl rule__PnArc__Group__6 ;
    public final void rule__PnArc__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:613:1: ( rule__PnArc__Group__5__Impl rule__PnArc__Group__6 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:614:2: rule__PnArc__Group__5__Impl rule__PnArc__Group__6
            {
            pushFollow(FOLLOW_rule__PnArc__Group__5__Impl_in_rule__PnArc__Group__51193);
            rule__PnArc__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__6_in_rule__PnArc__Group__51196);
            rule__PnArc__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__5"


    // $ANTLR start "rule__PnArc__Group__5__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:621:1: rule__PnArc__Group__5__Impl : ( ( rule__PnArc__WeightAssignment_5 ) ) ;
    public final void rule__PnArc__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:625:1: ( ( ( rule__PnArc__WeightAssignment_5 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:626:1: ( ( rule__PnArc__WeightAssignment_5 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:626:1: ( ( rule__PnArc__WeightAssignment_5 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:627:1: ( rule__PnArc__WeightAssignment_5 )
            {
             before(grammarAccess.getPnArcAccess().getWeightAssignment_5()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:628:1: ( rule__PnArc__WeightAssignment_5 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:628:2: rule__PnArc__WeightAssignment_5
            {
            pushFollow(FOLLOW_rule__PnArc__WeightAssignment_5_in_rule__PnArc__Group__5__Impl1223);
            rule__PnArc__WeightAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getPnArcAccess().getWeightAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__5__Impl"


    // $ANTLR start "rule__PnArc__Group__6"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:638:1: rule__PnArc__Group__6 : rule__PnArc__Group__6__Impl rule__PnArc__Group__7 ;
    public final void rule__PnArc__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:642:1: ( rule__PnArc__Group__6__Impl rule__PnArc__Group__7 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:643:2: rule__PnArc__Group__6__Impl rule__PnArc__Group__7
            {
            pushFollow(FOLLOW_rule__PnArc__Group__6__Impl_in_rule__PnArc__Group__61253);
            rule__PnArc__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__7_in_rule__PnArc__Group__61256);
            rule__PnArc__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__6"


    // $ANTLR start "rule__PnArc__Group__6__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:650:1: rule__PnArc__Group__6__Impl : ( ',' ) ;
    public final void rule__PnArc__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:654:1: ( ( ',' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:655:1: ( ',' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:655:1: ( ',' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:656:1: ','
            {
             before(grammarAccess.getPnArcAccess().getCommaKeyword_6()); 
            match(input,17,FOLLOW_17_in_rule__PnArc__Group__6__Impl1284); 
             after(grammarAccess.getPnArcAccess().getCommaKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__6__Impl"


    // $ANTLR start "rule__PnArc__Group__7"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:669:1: rule__PnArc__Group__7 : rule__PnArc__Group__7__Impl rule__PnArc__Group__8 ;
    public final void rule__PnArc__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:673:1: ( rule__PnArc__Group__7__Impl rule__PnArc__Group__8 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:674:2: rule__PnArc__Group__7__Impl rule__PnArc__Group__8
            {
            pushFollow(FOLLOW_rule__PnArc__Group__7__Impl_in_rule__PnArc__Group__71315);
            rule__PnArc__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PnArc__Group__8_in_rule__PnArc__Group__71318);
            rule__PnArc__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__7"


    // $ANTLR start "rule__PnArc__Group__7__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:681:1: rule__PnArc__Group__7__Impl : ( ( rule__PnArc__KindAssignment_7 ) ) ;
    public final void rule__PnArc__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:685:1: ( ( ( rule__PnArc__KindAssignment_7 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:686:1: ( ( rule__PnArc__KindAssignment_7 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:686:1: ( ( rule__PnArc__KindAssignment_7 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:687:1: ( rule__PnArc__KindAssignment_7 )
            {
             before(grammarAccess.getPnArcAccess().getKindAssignment_7()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:688:1: ( rule__PnArc__KindAssignment_7 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:688:2: rule__PnArc__KindAssignment_7
            {
            pushFollow(FOLLOW_rule__PnArc__KindAssignment_7_in_rule__PnArc__Group__7__Impl1345);
            rule__PnArc__KindAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getPnArcAccess().getKindAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__7__Impl"


    // $ANTLR start "rule__PnArc__Group__8"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:698:1: rule__PnArc__Group__8 : rule__PnArc__Group__8__Impl ;
    public final void rule__PnArc__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:702:1: ( rule__PnArc__Group__8__Impl )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:703:2: rule__PnArc__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__PnArc__Group__8__Impl_in_rule__PnArc__Group__81375);
            rule__PnArc__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__8"


    // $ANTLR start "rule__PnArc__Group__8__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:709:1: rule__PnArc__Group__8__Impl : ( '\\n' ) ;
    public final void rule__PnArc__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:713:1: ( ( '\\n' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:714:1: ( '\\n' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:714:1: ( '\\n' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:715:1: '\\n'
            {
             before(grammarAccess.getPnArcAccess().getLineFeedLfKeyword_8()); 
            match(input,18,FOLLOW_18_in_rule__PnArc__Group__8__Impl1403); 
             after(grammarAccess.getPnArcAccess().getLineFeedLfKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__Group__8__Impl"


    // $ANTLR start "rule__Place__Group__0"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:746:1: rule__Place__Group__0 : rule__Place__Group__0__Impl rule__Place__Group__1 ;
    public final void rule__Place__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:750:1: ( rule__Place__Group__0__Impl rule__Place__Group__1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:751:2: rule__Place__Group__0__Impl rule__Place__Group__1
            {
            pushFollow(FOLLOW_rule__Place__Group__0__Impl_in_rule__Place__Group__01452);
            rule__Place__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Place__Group__1_in_rule__Place__Group__01455);
            rule__Place__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__0"


    // $ANTLR start "rule__Place__Group__0__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:758:1: rule__Place__Group__0__Impl : ( 'p' ) ;
    public final void rule__Place__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:762:1: ( ( 'p' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:763:1: ( 'p' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:763:1: ( 'p' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:764:1: 'p'
            {
             before(grammarAccess.getPlaceAccess().getPKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__Place__Group__0__Impl1483); 
             after(grammarAccess.getPlaceAccess().getPKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__0__Impl"


    // $ANTLR start "rule__Place__Group__1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:777:1: rule__Place__Group__1 : rule__Place__Group__1__Impl rule__Place__Group__2 ;
    public final void rule__Place__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:781:1: ( rule__Place__Group__1__Impl rule__Place__Group__2 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:782:2: rule__Place__Group__1__Impl rule__Place__Group__2
            {
            pushFollow(FOLLOW_rule__Place__Group__1__Impl_in_rule__Place__Group__11514);
            rule__Place__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Place__Group__2_in_rule__Place__Group__11517);
            rule__Place__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__1"


    // $ANTLR start "rule__Place__Group__1__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:789:1: rule__Place__Group__1__Impl : ( ( rule__Place__NameAssignment_1 ) ) ;
    public final void rule__Place__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:793:1: ( ( ( rule__Place__NameAssignment_1 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:794:1: ( ( rule__Place__NameAssignment_1 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:794:1: ( ( rule__Place__NameAssignment_1 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:795:1: ( rule__Place__NameAssignment_1 )
            {
             before(grammarAccess.getPlaceAccess().getNameAssignment_1()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:796:1: ( rule__Place__NameAssignment_1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:796:2: rule__Place__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Place__NameAssignment_1_in_rule__Place__Group__1__Impl1544);
            rule__Place__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__1__Impl"


    // $ANTLR start "rule__Place__Group__2"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:806:1: rule__Place__Group__2 : rule__Place__Group__2__Impl rule__Place__Group__3 ;
    public final void rule__Place__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:810:1: ( rule__Place__Group__2__Impl rule__Place__Group__3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:811:2: rule__Place__Group__2__Impl rule__Place__Group__3
            {
            pushFollow(FOLLOW_rule__Place__Group__2__Impl_in_rule__Place__Group__21574);
            rule__Place__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Place__Group__3_in_rule__Place__Group__21577);
            rule__Place__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__2"


    // $ANTLR start "rule__Place__Group__2__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:818:1: rule__Place__Group__2__Impl : ( ( '[' )? ) ;
    public final void rule__Place__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:822:1: ( ( ( '[' )? ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:823:1: ( ( '[' )? )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:823:1: ( ( '[' )? )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:824:1: ( '[' )?
            {
             before(grammarAccess.getPlaceAccess().getLeftSquareBracketKeyword_2()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:825:1: ( '[' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:826:2: '['
                    {
                    match(input,20,FOLLOW_20_in_rule__Place__Group__2__Impl1606); 

                    }
                    break;

            }

             after(grammarAccess.getPlaceAccess().getLeftSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__2__Impl"


    // $ANTLR start "rule__Place__Group__3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:837:1: rule__Place__Group__3 : rule__Place__Group__3__Impl rule__Place__Group__4 ;
    public final void rule__Place__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:841:1: ( rule__Place__Group__3__Impl rule__Place__Group__4 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:842:2: rule__Place__Group__3__Impl rule__Place__Group__4
            {
            pushFollow(FOLLOW_rule__Place__Group__3__Impl_in_rule__Place__Group__31639);
            rule__Place__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Place__Group__4_in_rule__Place__Group__31642);
            rule__Place__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__3"


    // $ANTLR start "rule__Place__Group__3__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:849:1: rule__Place__Group__3__Impl : ( ( rule__Place__MarkingAssignment_3 ) ) ;
    public final void rule__Place__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:853:1: ( ( ( rule__Place__MarkingAssignment_3 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:854:1: ( ( rule__Place__MarkingAssignment_3 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:854:1: ( ( rule__Place__MarkingAssignment_3 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:855:1: ( rule__Place__MarkingAssignment_3 )
            {
             before(grammarAccess.getPlaceAccess().getMarkingAssignment_3()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:856:1: ( rule__Place__MarkingAssignment_3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:856:2: rule__Place__MarkingAssignment_3
            {
            pushFollow(FOLLOW_rule__Place__MarkingAssignment_3_in_rule__Place__Group__3__Impl1669);
            rule__Place__MarkingAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getMarkingAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__3__Impl"


    // $ANTLR start "rule__Place__Group__4"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:866:1: rule__Place__Group__4 : rule__Place__Group__4__Impl ;
    public final void rule__Place__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:870:1: ( rule__Place__Group__4__Impl )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:871:2: rule__Place__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Place__Group__4__Impl_in_rule__Place__Group__41699);
            rule__Place__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__4"


    // $ANTLR start "rule__Place__Group__4__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:877:1: rule__Place__Group__4__Impl : ( ']' ) ;
    public final void rule__Place__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:881:1: ( ( ']' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:882:1: ( ']' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:882:1: ( ']' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:883:1: ']'
            {
             before(grammarAccess.getPlaceAccess().getRightSquareBracketKeyword_4()); 
            match(input,21,FOLLOW_21_in_rule__Place__Group__4__Impl1727); 
             after(grammarAccess.getPlaceAccess().getRightSquareBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__4__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:906:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:910:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:911:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_rule__Transition__Group__0__Impl_in_rule__Transition__Group__01768);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Transition__Group__1_in_rule__Transition__Group__01771);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:918:1: rule__Transition__Group__0__Impl : ( 't' ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:922:1: ( ( 't' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:923:1: ( 't' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:923:1: ( 't' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:924:1: 't'
            {
             before(grammarAccess.getTransitionAccess().getTKeyword_0()); 
            match(input,22,FOLLOW_22_in_rule__Transition__Group__0__Impl1799); 
             after(grammarAccess.getTransitionAccess().getTKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:937:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:941:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:942:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_rule__Transition__Group__1__Impl_in_rule__Transition__Group__11830);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Transition__Group__2_in_rule__Transition__Group__11833);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:949:1: rule__Transition__Group__1__Impl : ( ( rule__Transition__NameAssignment_1 ) ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:953:1: ( ( ( rule__Transition__NameAssignment_1 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:954:1: ( ( rule__Transition__NameAssignment_1 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:954:1: ( ( rule__Transition__NameAssignment_1 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:955:1: ( rule__Transition__NameAssignment_1 )
            {
             before(grammarAccess.getTransitionAccess().getNameAssignment_1()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:956:1: ( rule__Transition__NameAssignment_1 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:956:2: rule__Transition__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Transition__NameAssignment_1_in_rule__Transition__Group__1__Impl1860);
            rule__Transition__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:966:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:970:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:971:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_rule__Transition__Group__2__Impl_in_rule__Transition__Group__21890);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Transition__Group__3_in_rule__Transition__Group__21893);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:978:1: rule__Transition__Group__2__Impl : ( ( '[' )? ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:982:1: ( ( ( '[' )? ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:983:1: ( ( '[' )? )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:983:1: ( ( '[' )? )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:984:1: ( '[' )?
            {
             before(grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_2()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:985:1: ( '[' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==20) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:986:2: '['
                    {
                    match(input,20,FOLLOW_20_in_rule__Transition__Group__2__Impl1922); 

                    }
                    break;

            }

             after(grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:997:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl rule__Transition__Group__4 ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1001:1: ( rule__Transition__Group__3__Impl rule__Transition__Group__4 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1002:2: rule__Transition__Group__3__Impl rule__Transition__Group__4
            {
            pushFollow(FOLLOW_rule__Transition__Group__3__Impl_in_rule__Transition__Group__31955);
            rule__Transition__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Transition__Group__4_in_rule__Transition__Group__31958);
            rule__Transition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1009:1: rule__Transition__Group__3__Impl : ( ( rule__Transition__Min_timeAssignment_3 ) ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1013:1: ( ( ( rule__Transition__Min_timeAssignment_3 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1014:1: ( ( rule__Transition__Min_timeAssignment_3 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1014:1: ( ( rule__Transition__Min_timeAssignment_3 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1015:1: ( rule__Transition__Min_timeAssignment_3 )
            {
             before(grammarAccess.getTransitionAccess().getMin_timeAssignment_3()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1016:1: ( rule__Transition__Min_timeAssignment_3 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1016:2: rule__Transition__Min_timeAssignment_3
            {
            pushFollow(FOLLOW_rule__Transition__Min_timeAssignment_3_in_rule__Transition__Group__3__Impl1985);
            rule__Transition__Min_timeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getMin_timeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__4"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1026:1: rule__Transition__Group__4 : rule__Transition__Group__4__Impl rule__Transition__Group__5 ;
    public final void rule__Transition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1030:1: ( rule__Transition__Group__4__Impl rule__Transition__Group__5 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1031:2: rule__Transition__Group__4__Impl rule__Transition__Group__5
            {
            pushFollow(FOLLOW_rule__Transition__Group__4__Impl_in_rule__Transition__Group__42015);
            rule__Transition__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Transition__Group__5_in_rule__Transition__Group__42018);
            rule__Transition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4"


    // $ANTLR start "rule__Transition__Group__4__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1038:1: rule__Transition__Group__4__Impl : ( ',' ) ;
    public final void rule__Transition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1042:1: ( ( ',' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1043:1: ( ',' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1043:1: ( ',' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1044:1: ','
            {
             before(grammarAccess.getTransitionAccess().getCommaKeyword_4()); 
            match(input,17,FOLLOW_17_in_rule__Transition__Group__4__Impl2046); 
             after(grammarAccess.getTransitionAccess().getCommaKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4__Impl"


    // $ANTLR start "rule__Transition__Group__5"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1057:1: rule__Transition__Group__5 : rule__Transition__Group__5__Impl rule__Transition__Group__6 ;
    public final void rule__Transition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1061:1: ( rule__Transition__Group__5__Impl rule__Transition__Group__6 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1062:2: rule__Transition__Group__5__Impl rule__Transition__Group__6
            {
            pushFollow(FOLLOW_rule__Transition__Group__5__Impl_in_rule__Transition__Group__52077);
            rule__Transition__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Transition__Group__6_in_rule__Transition__Group__52080);
            rule__Transition__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5"


    // $ANTLR start "rule__Transition__Group__5__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1069:1: rule__Transition__Group__5__Impl : ( ( rule__Transition__Max_timeAssignment_5 ) ) ;
    public final void rule__Transition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1073:1: ( ( ( rule__Transition__Max_timeAssignment_5 ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1074:1: ( ( rule__Transition__Max_timeAssignment_5 ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1074:1: ( ( rule__Transition__Max_timeAssignment_5 ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1075:1: ( rule__Transition__Max_timeAssignment_5 )
            {
             before(grammarAccess.getTransitionAccess().getMax_timeAssignment_5()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1076:1: ( rule__Transition__Max_timeAssignment_5 )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1076:2: rule__Transition__Max_timeAssignment_5
            {
            pushFollow(FOLLOW_rule__Transition__Max_timeAssignment_5_in_rule__Transition__Group__5__Impl2107);
            rule__Transition__Max_timeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getMax_timeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5__Impl"


    // $ANTLR start "rule__Transition__Group__6"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1086:1: rule__Transition__Group__6 : rule__Transition__Group__6__Impl ;
    public final void rule__Transition__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1090:1: ( rule__Transition__Group__6__Impl )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1091:2: rule__Transition__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Transition__Group__6__Impl_in_rule__Transition__Group__62137);
            rule__Transition__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__6"


    // $ANTLR start "rule__Transition__Group__6__Impl"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1097:1: rule__Transition__Group__6__Impl : ( ']\\n' ) ;
    public final void rule__Transition__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1101:1: ( ( ']\\n' ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1102:1: ( ']\\n' )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1102:1: ( ']\\n' )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1103:1: ']\\n'
            {
             before(grammarAccess.getTransitionAccess().getRightSquareBracketLineFeedLfKeyword_6()); 
            match(input,23,FOLLOW_23_in_rule__Transition__Group__6__Impl2165); 
             after(grammarAccess.getTransitionAccess().getRightSquareBracketLineFeedLfKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__6__Impl"


    // $ANTLR start "rule__Petrinet__NameAssignment_1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1131:1: rule__Petrinet__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Petrinet__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1135:1: ( ( RULE_ID ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1136:1: ( RULE_ID )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1136:1: ( RULE_ID )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1137:1: RULE_ID
            {
             before(grammarAccess.getPetrinetAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Petrinet__NameAssignment_12215); 
             after(grammarAccess.getPetrinetAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__NameAssignment_1"


    // $ANTLR start "rule__Petrinet__NodesAssignment_3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1146:1: rule__Petrinet__NodesAssignment_3 : ( rulePnNode ) ;
    public final void rule__Petrinet__NodesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1150:1: ( ( rulePnNode ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1151:1: ( rulePnNode )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1151:1: ( rulePnNode )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1152:1: rulePnNode
            {
             before(grammarAccess.getPetrinetAccess().getNodesPnNodeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_rulePnNode_in_rule__Petrinet__NodesAssignment_32246);
            rulePnNode();

            state._fsp--;

             after(grammarAccess.getPetrinetAccess().getNodesPnNodeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__NodesAssignment_3"


    // $ANTLR start "rule__Petrinet__ArcsAssignment_4"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1161:1: rule__Petrinet__ArcsAssignment_4 : ( rulePnArc ) ;
    public final void rule__Petrinet__ArcsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1165:1: ( ( rulePnArc ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1166:1: ( rulePnArc )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1166:1: ( rulePnArc )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1167:1: rulePnArc
            {
             before(grammarAccess.getPetrinetAccess().getArcsPnArcParserRuleCall_4_0()); 
            pushFollow(FOLLOW_rulePnArc_in_rule__Petrinet__ArcsAssignment_42277);
            rulePnArc();

            state._fsp--;

             after(grammarAccess.getPetrinetAccess().getArcsPnArcParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Petrinet__ArcsAssignment_4"


    // $ANTLR start "rule__PnArc__SourceAssignment_1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1176:1: rule__PnArc__SourceAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__PnArc__SourceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1180:1: ( ( ( RULE_ID ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1181:1: ( ( RULE_ID ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1181:1: ( ( RULE_ID ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1182:1: ( RULE_ID )
            {
             before(grammarAccess.getPnArcAccess().getSourceNodeCrossReference_1_0()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1183:1: ( RULE_ID )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1184:1: RULE_ID
            {
             before(grammarAccess.getPnArcAccess().getSourceNodeIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__PnArc__SourceAssignment_12312); 
             after(grammarAccess.getPnArcAccess().getSourceNodeIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getPnArcAccess().getSourceNodeCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__SourceAssignment_1"


    // $ANTLR start "rule__PnArc__TargetAssignment_3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1195:1: rule__PnArc__TargetAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__PnArc__TargetAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1199:1: ( ( ( RULE_ID ) ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1200:1: ( ( RULE_ID ) )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1200:1: ( ( RULE_ID ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1201:1: ( RULE_ID )
            {
             before(grammarAccess.getPnArcAccess().getTargetNodeCrossReference_3_0()); 
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1202:1: ( RULE_ID )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1203:1: RULE_ID
            {
             before(grammarAccess.getPnArcAccess().getTargetNodeIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__PnArc__TargetAssignment_32351); 
             after(grammarAccess.getPnArcAccess().getTargetNodeIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getPnArcAccess().getTargetNodeCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__TargetAssignment_3"


    // $ANTLR start "rule__PnArc__WeightAssignment_5"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1214:1: rule__PnArc__WeightAssignment_5 : ( RULE_INT ) ;
    public final void rule__PnArc__WeightAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1218:1: ( ( RULE_INT ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1219:1: ( RULE_INT )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1219:1: ( RULE_INT )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1220:1: RULE_INT
            {
             before(grammarAccess.getPnArcAccess().getWeightINTTerminalRuleCall_5_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__PnArc__WeightAssignment_52386); 
             after(grammarAccess.getPnArcAccess().getWeightINTTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__WeightAssignment_5"


    // $ANTLR start "rule__PnArc__KindAssignment_7"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1229:1: rule__PnArc__KindAssignment_7 : ( ruleArcKind ) ;
    public final void rule__PnArc__KindAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1233:1: ( ( ruleArcKind ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1234:1: ( ruleArcKind )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1234:1: ( ruleArcKind )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1235:1: ruleArcKind
            {
             before(grammarAccess.getPnArcAccess().getKindArcKindEnumRuleCall_7_0()); 
            pushFollow(FOLLOW_ruleArcKind_in_rule__PnArc__KindAssignment_72417);
            ruleArcKind();

            state._fsp--;

             after(grammarAccess.getPnArcAccess().getKindArcKindEnumRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PnArc__KindAssignment_7"


    // $ANTLR start "rule__Place__NameAssignment_1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1244:1: rule__Place__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Place__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1248:1: ( ( RULE_ID ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1249:1: ( RULE_ID )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1249:1: ( RULE_ID )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1250:1: RULE_ID
            {
             before(grammarAccess.getPlaceAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Place__NameAssignment_12448); 
             after(grammarAccess.getPlaceAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__NameAssignment_1"


    // $ANTLR start "rule__Place__MarkingAssignment_3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1259:1: rule__Place__MarkingAssignment_3 : ( RULE_INT ) ;
    public final void rule__Place__MarkingAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1263:1: ( ( RULE_INT ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1264:1: ( RULE_INT )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1264:1: ( RULE_INT )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1265:1: RULE_INT
            {
             before(grammarAccess.getPlaceAccess().getMarkingINTTerminalRuleCall_3_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Place__MarkingAssignment_32479); 
             after(grammarAccess.getPlaceAccess().getMarkingINTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__MarkingAssignment_3"


    // $ANTLR start "rule__Transition__NameAssignment_1"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1274:1: rule__Transition__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Transition__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1278:1: ( ( RULE_ID ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1279:1: ( RULE_ID )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1279:1: ( RULE_ID )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1280:1: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Transition__NameAssignment_12510); 
             after(grammarAccess.getTransitionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__NameAssignment_1"


    // $ANTLR start "rule__Transition__Min_timeAssignment_3"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1289:1: rule__Transition__Min_timeAssignment_3 : ( RULE_INT ) ;
    public final void rule__Transition__Min_timeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1293:1: ( ( RULE_INT ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1294:1: ( RULE_INT )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1294:1: ( RULE_INT )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1295:1: RULE_INT
            {
             before(grammarAccess.getTransitionAccess().getMin_timeINTTerminalRuleCall_3_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Transition__Min_timeAssignment_32541); 
             after(grammarAccess.getTransitionAccess().getMin_timeINTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Min_timeAssignment_3"


    // $ANTLR start "rule__Transition__Max_timeAssignment_5"
    // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1304:1: rule__Transition__Max_timeAssignment_5 : ( RULE_INT ) ;
    public final void rule__Transition__Max_timeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1308:1: ( ( RULE_INT ) )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1309:1: ( RULE_INT )
            {
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1309:1: ( RULE_INT )
            // ../betp02.ui/src-gen/betp02/ui/contentassist/antlr/internal/InternalPetrinet.g:1310:1: RULE_INT
            {
             before(grammarAccess.getTransitionAccess().getMax_timeINTTerminalRuleCall_5_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Transition__Max_timeAssignment_52572); 
             after(grammarAccess.getTransitionAccess().getMax_timeINTTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Max_timeAssignment_5"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePetrinet_in_entryRulePetrinet61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePetrinet68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__0_in_rulePetrinet94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnNode_in_entryRulePnNode121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePnNode128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnNode__Alternatives_in_rulePnNode154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnArc_in_entryRulePnArc181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePnArc188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__0_in_rulePnArc214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlace_in_entryRulePlace241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlace248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__Group__0_in_rulePlace274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTransition308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__0_in_ruleTransition334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArcKind__Alternatives_in_ruleArcKind371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_rule__PnNode__Alternatives406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlace_in_rule__PnNode__Alternatives423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__ArcKind__Alternatives456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ArcKind__Alternatives477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__0__Impl_in_rule__Petrinet__Group__0510 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__1_in_rule__Petrinet__Group__0513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Petrinet__Group__0__Impl541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__1__Impl_in_rule__Petrinet__Group__1572 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__2_in_rule__Petrinet__Group__1575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__NameAssignment_1_in_rule__Petrinet__Group__1__Impl602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__2__Impl_in_rule__Petrinet__Group__2632 = new BitSet(new long[]{0x0000000000498000L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__3_in_rule__Petrinet__Group__2635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Petrinet__Group__2__Impl663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__3__Impl_in_rule__Petrinet__Group__3694 = new BitSet(new long[]{0x0000000000498000L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__4_in_rule__Petrinet__Group__3697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__NodesAssignment_3_in_rule__Petrinet__Group__3__Impl724 = new BitSet(new long[]{0x0000000000480002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__4__Impl_in_rule__Petrinet__Group__4755 = new BitSet(new long[]{0x0000000000498000L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__5_in_rule__Petrinet__Group__4758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Petrinet__ArcsAssignment_4_in_rule__Petrinet__Group__4__Impl785 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Petrinet__Group__5__Impl_in_rule__Petrinet__Group__5816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Petrinet__Group__5__Impl844 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__0__Impl_in_rule__PnArc__Group__0887 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__PnArc__Group__1_in_rule__PnArc__Group__0890 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__PnArc__Group__0__Impl918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__1__Impl_in_rule__PnArc__Group__1949 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__PnArc__Group__2_in_rule__PnArc__Group__1952 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__SourceAssignment_1_in_rule__PnArc__Group__1__Impl979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__2__Impl_in_rule__PnArc__Group__21009 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__PnArc__Group__3_in_rule__PnArc__Group__21012 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__PnArc__Group__2__Impl1040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__3__Impl_in_rule__PnArc__Group__31071 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__PnArc__Group__4_in_rule__PnArc__Group__31074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__TargetAssignment_3_in_rule__PnArc__Group__3__Impl1101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__4__Impl_in_rule__PnArc__Group__41131 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__PnArc__Group__5_in_rule__PnArc__Group__41134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__PnArc__Group__4__Impl1162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__5__Impl_in_rule__PnArc__Group__51193 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__PnArc__Group__6_in_rule__PnArc__Group__51196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__WeightAssignment_5_in_rule__PnArc__Group__5__Impl1223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__6__Impl_in_rule__PnArc__Group__61253 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_rule__PnArc__Group__7_in_rule__PnArc__Group__61256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__PnArc__Group__6__Impl1284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__7__Impl_in_rule__PnArc__Group__71315 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__PnArc__Group__8_in_rule__PnArc__Group__71318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__KindAssignment_7_in_rule__PnArc__Group__7__Impl1345 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PnArc__Group__8__Impl_in_rule__PnArc__Group__81375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__PnArc__Group__8__Impl1403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__Group__0__Impl_in_rule__Place__Group__01452 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Place__Group__1_in_rule__Place__Group__01455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Place__Group__0__Impl1483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__Group__1__Impl_in_rule__Place__Group__11514 = new BitSet(new long[]{0x0000000000100020L});
    public static final BitSet FOLLOW_rule__Place__Group__2_in_rule__Place__Group__11517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__NameAssignment_1_in_rule__Place__Group__1__Impl1544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__Group__2__Impl_in_rule__Place__Group__21574 = new BitSet(new long[]{0x0000000000100020L});
    public static final BitSet FOLLOW_rule__Place__Group__3_in_rule__Place__Group__21577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Place__Group__2__Impl1606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__Group__3__Impl_in_rule__Place__Group__31639 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Place__Group__4_in_rule__Place__Group__31642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__MarkingAssignment_3_in_rule__Place__Group__3__Impl1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Place__Group__4__Impl_in_rule__Place__Group__41699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Place__Group__4__Impl1727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__0__Impl_in_rule__Transition__Group__01768 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Transition__Group__1_in_rule__Transition__Group__01771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Transition__Group__0__Impl1799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__1__Impl_in_rule__Transition__Group__11830 = new BitSet(new long[]{0x0000000000100020L});
    public static final BitSet FOLLOW_rule__Transition__Group__2_in_rule__Transition__Group__11833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__NameAssignment_1_in_rule__Transition__Group__1__Impl1860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__2__Impl_in_rule__Transition__Group__21890 = new BitSet(new long[]{0x0000000000100020L});
    public static final BitSet FOLLOW_rule__Transition__Group__3_in_rule__Transition__Group__21893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Transition__Group__2__Impl1922 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__3__Impl_in_rule__Transition__Group__31955 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Transition__Group__4_in_rule__Transition__Group__31958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Min_timeAssignment_3_in_rule__Transition__Group__3__Impl1985 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__4__Impl_in_rule__Transition__Group__42015 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Transition__Group__5_in_rule__Transition__Group__42018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Transition__Group__4__Impl2046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__5__Impl_in_rule__Transition__Group__52077 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Transition__Group__6_in_rule__Transition__Group__52080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Max_timeAssignment_5_in_rule__Transition__Group__5__Impl2107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__6__Impl_in_rule__Transition__Group__62137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Transition__Group__6__Impl2165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Petrinet__NameAssignment_12215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnNode_in_rule__Petrinet__NodesAssignment_32246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnArc_in_rule__Petrinet__ArcsAssignment_42277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__PnArc__SourceAssignment_12312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__PnArc__TargetAssignment_32351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__PnArc__WeightAssignment_52386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArcKind_in_rule__PnArc__KindAssignment_72417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Place__NameAssignment_12448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Place__MarkingAssignment_32479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Transition__NameAssignment_12510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Transition__Min_timeAssignment_32541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Transition__Max_timeAssignment_52572 = new BitSet(new long[]{0x0000000000000002L});

}