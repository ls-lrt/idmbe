package betp02.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import betp02.services.PetrinetGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPetrinetParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'pn'", "'{'", "'}'", "'a'", "','", "'\\n'", "'p'", "'['", "']'", "'t'", "']\\n'", "'read_arc'", "'normal'"
    };
    public static final int RULE_ID=4;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalPetrinetParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPetrinetParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPetrinetParser.tokenNames; }
    public String getGrammarFileName() { return "../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g"; }



     	private PetrinetGrammarAccess grammarAccess;
     	
        public InternalPetrinetParser(TokenStream input, PetrinetGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Petrinet";	
       	}
       	
       	@Override
       	protected PetrinetGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulePetrinet"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:68:1: entryRulePetrinet returns [EObject current=null] : iv_rulePetrinet= rulePetrinet EOF ;
    public final EObject entryRulePetrinet() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePetrinet = null;


        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:69:2: (iv_rulePetrinet= rulePetrinet EOF )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:70:2: iv_rulePetrinet= rulePetrinet EOF
            {
             newCompositeNode(grammarAccess.getPetrinetRule()); 
            pushFollow(FOLLOW_rulePetrinet_in_entryRulePetrinet75);
            iv_rulePetrinet=rulePetrinet();

            state._fsp--;

             current =iv_rulePetrinet; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePetrinet85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePetrinet"


    // $ANTLR start "rulePetrinet"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:77:1: rulePetrinet returns [EObject current=null] : (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )* ( (lv_arcs_4_0= rulePnArc ) )* otherlv_5= '}' ) ;
    public final EObject rulePetrinet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_nodes_3_0 = null;

        EObject lv_arcs_4_0 = null;


         enterRule(); 
            
        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:80:28: ( (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )* ( (lv_arcs_4_0= rulePnArc ) )* otherlv_5= '}' ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:81:1: (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )* ( (lv_arcs_4_0= rulePnArc ) )* otherlv_5= '}' )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:81:1: (otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )* ( (lv_arcs_4_0= rulePnArc ) )* otherlv_5= '}' )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:81:3: otherlv_0= 'pn' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_nodes_3_0= rulePnNode ) )* ( (lv_arcs_4_0= rulePnArc ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_rulePetrinet122); 

                	newLeafNode(otherlv_0, grammarAccess.getPetrinetAccess().getPnKeyword_0());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:85:1: ( (lv_name_1_0= RULE_ID ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:86:1: (lv_name_1_0= RULE_ID )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:86:1: (lv_name_1_0= RULE_ID )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:87:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePetrinet139); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPetrinetAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPetrinetRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_rulePetrinet156); 

                	newLeafNode(otherlv_2, grammarAccess.getPetrinetAccess().getLeftCurlyBracketKeyword_2());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:107:1: ( (lv_nodes_3_0= rulePnNode ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==17||LA1_0==20) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:108:1: (lv_nodes_3_0= rulePnNode )
            	    {
            	    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:108:1: (lv_nodes_3_0= rulePnNode )
            	    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:109:3: lv_nodes_3_0= rulePnNode
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPetrinetAccess().getNodesPnNodeParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePnNode_in_rulePetrinet177);
            	    lv_nodes_3_0=rulePnNode();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPetrinetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"nodes",
            	            		lv_nodes_3_0, 
            	            		"PnNode");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:125:3: ( (lv_arcs_4_0= rulePnArc ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:126:1: (lv_arcs_4_0= rulePnArc )
            	    {
            	    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:126:1: (lv_arcs_4_0= rulePnArc )
            	    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:127:3: lv_arcs_4_0= rulePnArc
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPetrinetAccess().getArcsPnArcParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePnArc_in_rulePetrinet199);
            	    lv_arcs_4_0=rulePnArc();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPetrinetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"arcs",
            	            		lv_arcs_4_0, 
            	            		"PnArc");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_13_in_rulePetrinet212); 

                	newLeafNode(otherlv_5, grammarAccess.getPetrinetAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePetrinet"


    // $ANTLR start "entryRulePnNode"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:155:1: entryRulePnNode returns [EObject current=null] : iv_rulePnNode= rulePnNode EOF ;
    public final EObject entryRulePnNode() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePnNode = null;


        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:156:2: (iv_rulePnNode= rulePnNode EOF )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:157:2: iv_rulePnNode= rulePnNode EOF
            {
             newCompositeNode(grammarAccess.getPnNodeRule()); 
            pushFollow(FOLLOW_rulePnNode_in_entryRulePnNode248);
            iv_rulePnNode=rulePnNode();

            state._fsp--;

             current =iv_rulePnNode; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePnNode258); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePnNode"


    // $ANTLR start "rulePnNode"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:164:1: rulePnNode returns [EObject current=null] : (this_Transition_0= ruleTransition | this_Place_1= rulePlace ) ;
    public final EObject rulePnNode() throws RecognitionException {
        EObject current = null;

        EObject this_Transition_0 = null;

        EObject this_Place_1 = null;


         enterRule(); 
            
        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:167:28: ( (this_Transition_0= ruleTransition | this_Place_1= rulePlace ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:168:1: (this_Transition_0= ruleTransition | this_Place_1= rulePlace )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:168:1: (this_Transition_0= ruleTransition | this_Place_1= rulePlace )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==20) ) {
                alt3=1;
            }
            else if ( (LA3_0==17) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:169:5: this_Transition_0= ruleTransition
                    {
                     
                            newCompositeNode(grammarAccess.getPnNodeAccess().getTransitionParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleTransition_in_rulePnNode305);
                    this_Transition_0=ruleTransition();

                    state._fsp--;

                     
                            current = this_Transition_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:179:5: this_Place_1= rulePlace
                    {
                     
                            newCompositeNode(grammarAccess.getPnNodeAccess().getPlaceParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_rulePlace_in_rulePnNode332);
                    this_Place_1=rulePlace();

                    state._fsp--;

                     
                            current = this_Place_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePnNode"


    // $ANTLR start "entryRulePnArc"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:195:1: entryRulePnArc returns [EObject current=null] : iv_rulePnArc= rulePnArc EOF ;
    public final EObject entryRulePnArc() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePnArc = null;


        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:196:2: (iv_rulePnArc= rulePnArc EOF )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:197:2: iv_rulePnArc= rulePnArc EOF
            {
             newCompositeNode(grammarAccess.getPnArcRule()); 
            pushFollow(FOLLOW_rulePnArc_in_entryRulePnArc367);
            iv_rulePnArc=rulePnArc();

            state._fsp--;

             current =iv_rulePnArc; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePnArc377); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePnArc"


    // $ANTLR start "rulePnArc"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:204:1: rulePnArc returns [EObject current=null] : (otherlv_0= 'a' ( (otherlv_1= RULE_ID ) ) otherlv_2= ',' ( (otherlv_3= RULE_ID ) ) otherlv_4= ',' ( (lv_weight_5_0= RULE_INT ) ) otherlv_6= ',' ( (lv_kind_7_0= ruleArcKind ) ) otherlv_8= '\\n' ) ;
    public final EObject rulePnArc() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_weight_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Enumerator lv_kind_7_0 = null;


         enterRule(); 
            
        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:207:28: ( (otherlv_0= 'a' ( (otherlv_1= RULE_ID ) ) otherlv_2= ',' ( (otherlv_3= RULE_ID ) ) otherlv_4= ',' ( (lv_weight_5_0= RULE_INT ) ) otherlv_6= ',' ( (lv_kind_7_0= ruleArcKind ) ) otherlv_8= '\\n' ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:208:1: (otherlv_0= 'a' ( (otherlv_1= RULE_ID ) ) otherlv_2= ',' ( (otherlv_3= RULE_ID ) ) otherlv_4= ',' ( (lv_weight_5_0= RULE_INT ) ) otherlv_6= ',' ( (lv_kind_7_0= ruleArcKind ) ) otherlv_8= '\\n' )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:208:1: (otherlv_0= 'a' ( (otherlv_1= RULE_ID ) ) otherlv_2= ',' ( (otherlv_3= RULE_ID ) ) otherlv_4= ',' ( (lv_weight_5_0= RULE_INT ) ) otherlv_6= ',' ( (lv_kind_7_0= ruleArcKind ) ) otherlv_8= '\\n' )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:208:3: otherlv_0= 'a' ( (otherlv_1= RULE_ID ) ) otherlv_2= ',' ( (otherlv_3= RULE_ID ) ) otherlv_4= ',' ( (lv_weight_5_0= RULE_INT ) ) otherlv_6= ',' ( (lv_kind_7_0= ruleArcKind ) ) otherlv_8= '\\n'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_rulePnArc414); 

                	newLeafNode(otherlv_0, grammarAccess.getPnArcAccess().getAKeyword_0());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:212:1: ( (otherlv_1= RULE_ID ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:213:1: (otherlv_1= RULE_ID )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:213:1: (otherlv_1= RULE_ID )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:214:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getPnArcRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePnArc434); 

            		newLeafNode(otherlv_1, grammarAccess.getPnArcAccess().getSourceNodeCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_rulePnArc446); 

                	newLeafNode(otherlv_2, grammarAccess.getPnArcAccess().getCommaKeyword_2());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:229:1: ( (otherlv_3= RULE_ID ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:230:1: (otherlv_3= RULE_ID )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:230:1: (otherlv_3= RULE_ID )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:231:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getPnArcRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePnArc466); 

            		newLeafNode(otherlv_3, grammarAccess.getPnArcAccess().getTargetNodeCrossReference_3_0()); 
            	

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_15_in_rulePnArc478); 

                	newLeafNode(otherlv_4, grammarAccess.getPnArcAccess().getCommaKeyword_4());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:246:1: ( (lv_weight_5_0= RULE_INT ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:247:1: (lv_weight_5_0= RULE_INT )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:247:1: (lv_weight_5_0= RULE_INT )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:248:3: lv_weight_5_0= RULE_INT
            {
            lv_weight_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_rulePnArc495); 

            			newLeafNode(lv_weight_5_0, grammarAccess.getPnArcAccess().getWeightINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPnArcRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"weight",
                    		lv_weight_5_0, 
                    		"INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,15,FOLLOW_15_in_rulePnArc512); 

                	newLeafNode(otherlv_6, grammarAccess.getPnArcAccess().getCommaKeyword_6());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:268:1: ( (lv_kind_7_0= ruleArcKind ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:269:1: (lv_kind_7_0= ruleArcKind )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:269:1: (lv_kind_7_0= ruleArcKind )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:270:3: lv_kind_7_0= ruleArcKind
            {
             
            	        newCompositeNode(grammarAccess.getPnArcAccess().getKindArcKindEnumRuleCall_7_0()); 
            	    
            pushFollow(FOLLOW_ruleArcKind_in_rulePnArc533);
            lv_kind_7_0=ruleArcKind();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPnArcRule());
            	        }
                   		set(
                   			current, 
                   			"kind",
                    		lv_kind_7_0, 
                    		"ArcKind");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,16,FOLLOW_16_in_rulePnArc545); 

                	newLeafNode(otherlv_8, grammarAccess.getPnArcAccess().getLineFeedLfKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePnArc"


    // $ANTLR start "entryRulePlace"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:298:1: entryRulePlace returns [EObject current=null] : iv_rulePlace= rulePlace EOF ;
    public final EObject entryRulePlace() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlace = null;


        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:299:2: (iv_rulePlace= rulePlace EOF )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:300:2: iv_rulePlace= rulePlace EOF
            {
             newCompositeNode(grammarAccess.getPlaceRule()); 
            pushFollow(FOLLOW_rulePlace_in_entryRulePlace581);
            iv_rulePlace=rulePlace();

            state._fsp--;

             current =iv_rulePlace; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePlace591); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:307:1: rulePlace returns [EObject current=null] : (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' ) ;
    public final EObject rulePlace() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_marking_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:310:28: ( (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:311:1: (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:311:1: (otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']' )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:311:3: otherlv_0= 'p' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_marking_3_0= RULE_INT ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_rulePlace628); 

                	newLeafNode(otherlv_0, grammarAccess.getPlaceAccess().getPKeyword_0());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:315:1: ( (lv_name_1_0= RULE_ID ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:316:1: (lv_name_1_0= RULE_ID )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:316:1: (lv_name_1_0= RULE_ID )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:317:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePlace645); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPlaceAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPlaceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:333:2: (otherlv_2= '[' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:333:4: otherlv_2= '['
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_18_in_rulePlace663); 

                        	newLeafNode(otherlv_2, grammarAccess.getPlaceAccess().getLeftSquareBracketKeyword_2());
                        

                    }
                    break;

            }

            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:337:3: ( (lv_marking_3_0= RULE_INT ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:338:1: (lv_marking_3_0= RULE_INT )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:338:1: (lv_marking_3_0= RULE_INT )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:339:3: lv_marking_3_0= RULE_INT
            {
            lv_marking_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_rulePlace682); 

            			newLeafNode(lv_marking_3_0, grammarAccess.getPlaceAccess().getMarkingINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPlaceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"marking",
                    		lv_marking_3_0, 
                    		"INT");
            	    

            }


            }

            otherlv_4=(Token)match(input,19,FOLLOW_19_in_rulePlace699); 

                	newLeafNode(otherlv_4, grammarAccess.getPlaceAccess().getRightSquareBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "entryRuleTransition"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:367:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:368:2: (iv_ruleTransition= ruleTransition EOF )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:369:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_ruleTransition_in_entryRuleTransition735);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTransition745); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:376:1: ruleTransition returns [EObject current=null] : (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_min_time_3_0=null;
        Token otherlv_4=null;
        Token lv_max_time_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:379:28: ( (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:380:1: (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:380:1: (otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n' )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:380:3: otherlv_0= 't' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' )? ( (lv_min_time_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_max_time_5_0= RULE_INT ) ) otherlv_6= ']\\n'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleTransition782); 

                	newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getTKeyword_0());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:384:1: ( (lv_name_1_0= RULE_ID ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:385:1: (lv_name_1_0= RULE_ID )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:385:1: (lv_name_1_0= RULE_ID )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:386:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTransition799); 

            			newLeafNode(lv_name_1_0, grammarAccess.getTransitionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:402:2: (otherlv_2= '[' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:402:4: otherlv_2= '['
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleTransition817); 

                        	newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_2());
                        

                    }
                    break;

            }

            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:406:3: ( (lv_min_time_3_0= RULE_INT ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:407:1: (lv_min_time_3_0= RULE_INT )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:407:1: (lv_min_time_3_0= RULE_INT )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:408:3: lv_min_time_3_0= RULE_INT
            {
            lv_min_time_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleTransition836); 

            			newLeafNode(lv_min_time_3_0, grammarAccess.getTransitionAccess().getMin_timeINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"min_time",
                    		lv_min_time_3_0, 
                    		"INT");
            	    

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_15_in_ruleTransition853); 

                	newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getCommaKeyword_4());
                
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:428:1: ( (lv_max_time_5_0= RULE_INT ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:429:1: (lv_max_time_5_0= RULE_INT )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:429:1: (lv_max_time_5_0= RULE_INT )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:430:3: lv_max_time_5_0= RULE_INT
            {
            lv_max_time_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleTransition870); 

            			newLeafNode(lv_max_time_5_0, grammarAccess.getTransitionAccess().getMax_timeINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"max_time",
                    		lv_max_time_5_0, 
                    		"INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,21,FOLLOW_21_in_ruleTransition887); 

                	newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getRightSquareBracketLineFeedLfKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "ruleArcKind"
    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:458:1: ruleArcKind returns [Enumerator current=null] : ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) ) ;
    public final Enumerator ruleArcKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:460:28: ( ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) ) )
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:461:1: ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) )
            {
            // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:461:1: ( (enumLiteral_0= 'read_arc' ) | (enumLiteral_1= 'normal' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==22) ) {
                alt6=1;
            }
            else if ( (LA6_0==23) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:461:2: (enumLiteral_0= 'read_arc' )
                    {
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:461:2: (enumLiteral_0= 'read_arc' )
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:461:4: enumLiteral_0= 'read_arc'
                    {
                    enumLiteral_0=(Token)match(input,22,FOLLOW_22_in_ruleArcKind937); 

                            current = grammarAccess.getArcKindAccess().getRead_arcEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getArcKindAccess().getRead_arcEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:467:6: (enumLiteral_1= 'normal' )
                    {
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:467:6: (enumLiteral_1= 'normal' )
                    // ../betp02/src-gen/betp02/parser/antlr/internal/InternalPetrinet.g:467:8: enumLiteral_1= 'normal'
                    {
                    enumLiteral_1=(Token)match(input,23,FOLLOW_23_in_ruleArcKind954); 

                            current = grammarAccess.getArcKindAccess().getNormalEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getArcKindAccess().getNormalEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArcKind"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePetrinet_in_entryRulePetrinet75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePetrinet85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rulePetrinet122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePetrinet139 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_rulePetrinet156 = new BitSet(new long[]{0x0000000000126000L});
    public static final BitSet FOLLOW_rulePnNode_in_rulePetrinet177 = new BitSet(new long[]{0x0000000000126000L});
    public static final BitSet FOLLOW_rulePnArc_in_rulePetrinet199 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_13_in_rulePetrinet212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnNode_in_entryRulePnNode248 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePnNode258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_rulePnNode305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlace_in_rulePnNode332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePnArc_in_entryRulePnArc367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePnArc377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rulePnArc414 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePnArc434 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulePnArc446 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePnArc466 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulePnArc478 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_rulePnArc495 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulePnArc512 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_ruleArcKind_in_rulePnArc533 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_rulePnArc545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlace_in_entryRulePlace581 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlace591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rulePlace628 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePlace645 = new BitSet(new long[]{0x0000000000040020L});
    public static final BitSet FOLLOW_18_in_rulePlace663 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_rulePlace682 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulePlace699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition735 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTransition745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleTransition782 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTransition799 = new BitSet(new long[]{0x0000000000040020L});
    public static final BitSet FOLLOW_18_in_ruleTransition817 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleTransition836 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleTransition853 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleTransition870 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleTransition887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleArcKind937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleArcKind954 = new BitSet(new long[]{0x0000000000000002L});

}