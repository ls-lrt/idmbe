---------------------------------------------------------------------------
                  BE IDM: Validation de modèles de procédé
---------------------------------------------------------------------------

url sujet: http://cregut.perso.enseeiht.fr/ENS/2012-insa-idm/BE/


rapport:
        le répertoire rapport contient le rapport final.
	Il est très important de bien concerver sa structure.
	
rapport/test continet un fichier : trame.tex
c'est cette trame qu'il faut compiler pour avoir un apperçu de ce
qu'on est entrain de faire. Le mieux c'est de copier le répertoire
test entier (et le coller dans le répertoire rapport), le renommer
(par exemple en kikoo) puis éditer le fichier .tex contenu dedans.
Le fichier trame.tex peut être compilé ce qui permet de voir le rendu.
Le fichier test.tex contient le format qu'il faut respecter pour
l'inclusion d'images. 


Pour l'ajout de l'aspect temporisé des réseaux de Petri, une source
intéressante:
https://labanquise.insa-rouen.fr/scm/viewvc.php/*checkout*/trunk/doc/litterature/RdPExtTemp.pdf



---------------------------------------------------------------------------
                               TP
---------------------------------------------------------------------------

<pre>
progress : [***************************** ****************** ]
           |  TP01   |  TP02   |  TP03   |  TP04   |  TP05   | 
</pre>


---------------------------------------------------------------------------
                  TP01: Transformation de modèle avec ATL
---------------------------------------------------------------------------

url sujet: http://cregut.perso.enseeiht.fr/ENS/2012-insa-idm/BETP01/
url tina : http://projects.laas.fr/tina//distribution.php


ouvrir 
dans 
eclipse  : Choisir son kikooworkspace, puis faire un nouveau projet ecore
	   et choisir comme répertoire source betp01 et nomer le projet 
	   betp01.

TODO     : Commenter et nettoyer le code.


---------------------------------------------------------------------------
                  TP02: Définition de syntaxes concrètes textuelles
---------------------------------------------------------------------------

url sujet: http://cregut.perso.enseeiht.fr/ENS/2012-insa-idm/BETP02/

Le répertoire est un peu en désordre, le répertoir contient les exos du tp
(tout ce qui est fr.enseeit...), la dernière tentative de faire marcher le
xtext pour le petrinet (pour faire un éditeur) est petrinetxtext.
Ça compile mais ne marche pas pour l'éditeur.

Le bon répertoire est le répertoire lebon/ qui contient tous les bons
répertoires

---------------------------------------------------------------------------
                  TP03: Transformation de modèle à texte avec Acceleo
---------------------------------------------------------------------------

url sujet: http://cregut.perso.enseeiht.fr/ENS/2012-insa-idm/BETP03/

Les deux répertoires importants sont betp03 dans lequel tous les
fichiers source seront chargés et tous les fichiers de sortie seront
produits et le répertoire betp03.textGeneration qui contient tous les
fichiers sources Acceleo.

TODO     : La génération de .dot pour la visualisation graphique du
	   réseau de Petri peut être amméliorer: actuellement, les
	   read_arcs ne sont pas différenciés, le marquage non plus... 

---------------------------------------------------------------------------
                  TP04: Définition de syntaxes concrètes graphiques
---------------------------------------------------------------------------

url sujet: http://cregut.perso.enseeiht.fr/ENS/2012-insa-idm/BETP04/

Le répertoire contient 2 tentatives betp041/ et be04petrinet/, le dernier
est le bon et celui qui contient l'éditeur graphique joli (avec cercles
pour les places, flèches pour les arcs...).

---------------------------------------------------------------------------
                  TP05: Sémantique statique d'un DSML avec OCL
---------------------------------------------------------------------------

url sujet: http://cregut.perso.enseeiht.fr/ENS/2012-insa-idm/BETP05/

TODO:  Commenter le code ocl.
