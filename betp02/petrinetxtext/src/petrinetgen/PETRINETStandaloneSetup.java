
package petrinetgen;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class PETRINETStandaloneSetup extends PETRINETStandaloneSetupGenerated{

	public static void doSetup() {
		new PETRINETStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

