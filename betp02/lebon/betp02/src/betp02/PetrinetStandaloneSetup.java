
package betp02;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class PetrinetStandaloneSetup extends PetrinetStandaloneSetupGenerated{

	public static void doSetup() {
		new PetrinetStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

